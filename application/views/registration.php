    <link rel="stylesheet" type="text/css" href="<?php echo base_url(''); ?>assets/js/bootstrap-datepicker/css/datepicker-custom.css" />

    <div class="wrapper"  style="background-image: linear-gradient(white,#424F63);">

        <body class="login-body">

            <div class="container">

                <form class="form-signin form-signin-custom" id="frm_signup" method="POST" action="<?php echo base_url().'users/register_user'; ?>">

                    <div class="form-signin-heading text-center">

                    </div>

                    <div class="login-wrap login-wrap-custom">
                        <h1 class="sign-title sign-title-custom" style="color:white !important; ">
                            Registration
                        </h1>
                        <p>Enter your personal details below</p>

                        <div class="row">

                            <div class="col-lg-6">
                                <input type="text" autofocus="" placeholder="First Name" class="form-control required lettersonly" id="firstname" name="firstname">
                            </div>
                            <div class="col-lg-6">
                                <input type="text" autofocus="" placeholder="Last Name" class="form-control required lettersonly" id="lastname" name="lastname">
                            </div>  

                        </div>

                        <input type="text" autofocus=""  placeholder="Email" class="form-control required email" id="email" name="email">


                        <input class="form-control form-control-inline input-medium default-date-picker" size="16" type="text" value="" placeholder="Date of Birth (dd-mm-yyyy)" id="birthdate" name="birthdate"> 

                        <div class="radios">
                            <label for="radio_male" class="label_radio col-lg-6 col-sm-6">
                                <input type="radio" checked="" value="1" id="radio_male" name="gender"> Male
                            </label>
                            <label for="radio_female" class="label_radio col-lg-6 col-sm-6">
                                <input type="radio" value="0" id="radio_female" name="gender"> Female
                            </label>
                        </div>



                        <p> Enter your account details below</p>

                        <input type="text" autofocus="" placeholder="Username" class="form-control required" id="username" name="username"> 

                        <input type="password" placeholder="Password" class="form-control required" id="password" name="password" minlength="8"> 

                        <input type="password" placeholder="Re-type Password" class="form-control" id="passconf" name="passconf" equalTo="#password">

                        <button type="submit" class="btn btn-lg btn-login btn-block" id="btn_signup" name="btn_signup">

                            Sign Up
                        </button>

                        <div class="registration">
                            Already Registered.
                            <a href="<?php echo base_url().'users/signin';?>" class="">
                                Sign In
                            </a>
                        </div>

                    </div>

                </form>


            </div>
        </div>

        <!--pickers plugins-->
        <script type="text/javascript" src="<?php echo base_url('');?>assets/js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script type="text/javascript" src="<?php echo base_url('');?>assets/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
        <script type="text/javascript" src="<?php echo base_url('');?>assets/js/bootstrap-daterangepicker/moment.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('');?>assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script type="text/javascript" src="<?php echo base_url('');?>assets/js/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
        <script type="text/javascript" src="<?php echo base_url('');?>assets/js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>

        <!--pickers initialization-->
        <!-- <script src="<?php echo base_url('');?>assets/js/pickers-init.js"></script> -->
        <script src="<?php echo base_url('');?>assets/js/custom.js"></script>


        <script>

            $(document).ready(function(){
                $("#frm_signup").validate({
                    rules: {
                        username: {
                            remote: {
                                url: "<?php echo base_url(''); ?>users/user_exists",
                                type: "POST",
                                data: {
                                    username: function(){ 
                                        return $("#username").val(); 
                                    }
                                }
                            }
                        },

                        email: {
                            remote: {
                                url: "<?php echo base_url(''); ?>users/email_exists",
                                type: "POST",
                                data: {
                                    email: function(){ 
                                        return $("#email").val(); 
                                    }
                                }
                            }
                        },        
                    },
                    messages: {
                        username: {
                            remote: "Username Already Exists"
                        },
                        email: {
                           remote: "Email Already Exists"
                       }
                   }

               });
                $('.default-date-picker').datepicker({
                    format: 'dd-mm-yyyy',
                    autoclose: true
                });
            });

            function getCities(state_id){
                if (state_id != "") {
                    $.ajax({
                        url: '<?php echo base_url(''); ?>users/get_cities/' + state_id,
                        type: 'GET',
                        data: {state_id: state_id},

                    })
                    .done(function(cities) {
                        console.log("success");
                        $('#dd_cities').html("");
                        $('#dd_cities').html(cities);

                    })

                    .fail(function() {
                        console.log("error");
                    })

                } else {


                }


            }




        </script>





