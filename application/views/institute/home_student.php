<!--body wrapper start-->
<!--dropzone css-->
<?php $ssn_inst_details = $this->session->userdata('ssn_inst_details'); ?>
<link href="<?php echo base_url('assets'); ?>/js/dropzone/css/dropzone.css" rel="stylesheet"/>
<div class="wrapper" style="background-image: linear-gradient(white,#424F63);height: 100vh;">
	
	<div class="row blog">
		<div class="col-md-3">
			<!-- left side start-->
			<div class="left-side-inner" style="background-color: #353f4f;">

				<!--sidebar nav start-->
				<ul class="nav nav-pills nav-stacked custom-nav">
					<li>
						<a>
							<div class="row">
								<div class="col-md-8" style="word-wrap: break-word;">
									<?php echo $ssn_inst_details['institute_name']; ?>
									<span>as	</span>
									<?php print_r($role_name); ?>
								</div>
								<div class="col-md-3">
									<form action="<?php echo base_url('institute/change_pos') ?>" method="post" accept-charset="utf-8">
										<button id="btn_change" class="btn btn-success" style="background-color: #50cea1;color: #454d64;" type="submit">Change</button>
									</form>

								</div>

								
							</div>
						</a>
					</li>
				</ul>
				<!--sidebar nav end-->

			</div>

			<div class="left-side-inner" style="background-color: #353f4f;">

				<!--sidebar nav start-->
				<ul class="nav nav-pills nav-stacked custom-nav">
					
					<li>
						<a href="<?php echo base_url('institute/student/')."?tab="."classes_joined&parent=0"; ?>"><i class="fa fa-home"></i> <span>Classes Joined</span></a>

					</li>

					<li><a href="#"><i class="fa fa-laptop"></i> <span>Students</span></a>

					</li>

				</ul>
				<!--sidebar nav end-->

			</div>

			<!-- left side end-->


		</div>
		<div class="col-md-9">

			<?php if ($this->input->get()) {
				if ($this->input->get('tab') == "classes_joined") {
					?>
					<div class="panel" id="div_row">
						<?php include('classes/student_classes.php'); ?>
					</div>
					<?php
				}
				elseif ($this->input->get('tab') == "current") {
					?>
					<div class="panel" style="height: 300px;">
						<div class="panel-body">
							<div class="row">
								<div class="col-md-9">
									<span>Insitute you've joined is :</span>
									<?php $ssn_inst_details = $this->session->userdata('ssn_inst_details'); ?>
									<?php echo $ssn_inst_details['institute_name']; ?>
									<span>as</span>									
									<?php print_r($role_name); ?>
								</div>

								<div class="col-md-2">

									<form action="<?php echo base_url('institute/change_pos') ?>" method="post" accept-charset="utf-8">
										<button id="btn_change" type="submit">Change</button>
									</form>

								</div>
								<div class="col-md-2">
									<!-- <a href="#" class="btn btn-join"> Join </a> -->
									<!-- <button type="submit" class="btn btn-join">Join</button> -->
								</div>

							</div>	

						</div>
					</div>
					<?php
				}
				elseif ($this->input->get('tab') == "list_teachers") {
					?>
					<div class="panel">
						<div class="panel-body">

						</div>
					</div>

					<?php
				}
				elseif ($this->input->get('tab') == "requests") {
					?>
					<div class="panel">
						<div class="panel-body">

						</div>
					</div>
					<?php
				}

			}
			else {
				?>
				<div class="panel">
					<div class="panel-body">


					</div>
				</div>
				<?php

			} ?>




		</div>
<!-- <div class="col-md-5">
			
</div> -->
</div>
</div>
<!--body wrapper end-->
<!-- Modal -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_add_class" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<form method="POST" action="<?php echo base_url('classes/new_class');?>" accept-charset="utf-8">
				<div class="modal-header">
					<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
					<h4 class="modal-title">Add New Class </h4>
				</div>
				<div class="modal-body">		
					<div class="row">
						<div class="col-md-12">
							<label for="class_name">Class Name </label>
							<input type="text" name="class_name" id="class_name"><br><br>		
						</div>
					</div>				

					<div class="row">
						<div class="col-md-12">
							<label for="class_desc">Description</label>
							<input type="text" name="class_desc" id="class_desc" placeholder=""><br>
						</div>
					</div>

					<br>	
					<div class="row">
						
						<div class="col-md-12">
							<label for="type_choice" class="">Which One?</label>	
							
							<select id="type_choice" name="type_choice">
								<option value="folder">Folder</option>
								<option value="class">Class</option>
							</select>
							
							
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" id="btn_add_class" class="btn btn-primary">Create Class</button>
				</div>
			</form>
		</div>

	</div>
</div>
<script>
	$(document).ready(function() {
		$('body').on('click', '.classes_joined', function(event) {
			event.preventDefault();
			alert($(this).data('class_id'));
		});
	});

</script>