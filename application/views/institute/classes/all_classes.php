
<?php if (!empty($classes)) :?>
	<?php foreach ($classes as $class) : ?>
		<?php if ($class['is_folder'] == "1"):?>
			<div class="col-md-3 col-sm-3" id="div_class">
				<div class="panel btn-<?php echo ($class['is_folder'] == "1") ? "primary" : "default" ?>">
					<form action="<?php echo base_url('classes/my_classes'); ?>" method="POST" accept-charset="utf-8">
						<a class="panel-body class-info" data-class_id="<?php echo $class['class_id']; ?>" data-is_folder = "<?php echo $class['is_folder']; ?>" style="text-decoration: none; align-self: center;" href="" id="class_selector">

							<input type="hidden" value="<?php echo $class['class_id']; ?>" name="parent_id">

							<h4 <?php  echo ($class['is_folder'] == "1") ? "style=color:white;" : "" ; ?>>
								<?php echo $class['class_name']; ?>

								<span class="text-muted small">
									<?php echo $class['class_desc']; ?>
								</span>
							</h4>

							<div class="media">

							</div>
							<button type="submit" id="btn_class_click" style="display: none;" value="hahahlol" class="btn_class_click"></button>
						</a>
					</form>
				</div>
			</div>		
		<?php endif; ?>
	<?php endforeach; ?>
<?php endif; ?>		

<?php if (!empty($classes)) :?>
	<?php foreach ($classes as $class) : ?>
		<?php if ($class['is_folder'] == "0"):?>
			<div class="col-md-3 col-sm-3" id="div_class">
				<div class="panel btn-<?php echo ($class['is_folder'] == "1") ? "primary" : "default" ?>">
					<form action="<?php echo base_url('classes'); ?>" method="POST" accept-charset="utf-8">
						<a class="panel-body class-info" data-class_id="<?php echo $class['class_id']; ?>" data-is_folder = "<?php echo $class['is_folder']; ?>" style="text-decoration: none; align-self: center;" href="" id="class_selector">

							<input type="hidden" value="<?php echo $class['class_id']; ?>" name="parent_id">

							<h4 <?php  echo ($class['is_folder'] == "1") ? "style=color:white;" : "" ; ?>>
								<?php echo $class['class_name']; ?>

								<span class="text-muted small">
									<?php echo $class['class_desc']; ?>
								</span>
							</h4>

							<div class="media">

							</div>
							<button type="submit" id="btn_class_click" style="display: none;" value="hahahlol" class="btn_class_click"></button>
						</a>
					</form>
				</div>

			</div>		
		<?php endif; ?>
	<?php endforeach; ?>	
<?php endif; ?>				

<div id="div_classes">
	<div class="directory-info-row">
		<div class="row" id="div_row">
			hello	
			<!-- Ajax Content -->

		</div>
	</div>
</div>

<!-- Modal -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<form method="POST" action="<?php echo base_url('classes/new_class');?>" accept-charset="utf-8">
				<div class="modal-header">
					<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
					<h4 class="modal-title">Add New Class 

					</h4>
				</div>
				<div class="modal-body">		
					<div class="row">
						<div class="col-md-12">
							<label for="class_name">Class Name </label>
							<input type="text" name="class_name" id="class_name"><br><br>		
						</div>
					</div>				
					
					<div class="row">
						<div class="col-md-12">
							<label for="class_desc">Description</label>
							<input type="text" name="class_desc" id="class_desc" placeholder=""><br>
						</div>
					</div>

					<br>	
					<div class="row">
						
						<div class="col-md-12">
							<label for="type_choice" class="">Which One?</label>	
							
							<select id="type_choice" name="type_choice">
								<option value="folder">Folder</option>
								<option value="class">Class</option>
							</select>
							
							
						</div>
					</div>



					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" id="btn_add_class" class="btn btn-primary">Create Class</button>
				</div>
			</form>
		</div>

	</div>
</div>


<!-- Script -->
<script>
	function loadClasses(p_id){

		$.ajax({
			url: '<?php echo base_url('classes/load_classes/'); ?>'+ p_id,
			type: 'POST',					
		})
		.done(function(response) {
			$('#div_row').html(response);
			// $('#div_row').getNiceScroll().resize();
			console.log("success");
		})
		.fail(function(jqXHR, textStatus, errorThrown) {
			console.log(errorThrown);
		})
		
	}

	$(document).ready(function(){
		<?php 
		$parent_id = $this->input->get('parent');
		 ?>
		loadClasses("<?php echo (!$parent_id)?"0":$parent_id; ?>"); 

		$("body").on('click', '#class_selector', function(event) {
			event.preventDefault();
			var is_folder = $(this).data('is_folder');
			if (is_folder == "0") {
				window.location.href = "<?php echo base_url('classes/stream/'); ?>" + $(this).data('class_id');
			} else if (is_folder == "1") {
				var parent_id = $(this).data('class_id');

				$.ajax({
					url: '<?php echo base_url('classes'); ?>',
					type: 'POST',
					data: {parent_id: parent_id},
				})
				.done(function(response) {
					window.location.href = "<?php echo base_url('classes/')."?parent_id="; ?>" + parent_id;
					// loadClasses();
				})
				.fail(function() {
					console.log("error");
				})
				.always(function() {
					console.log("complete");
				});
				
			}



		});

		$("body").on('click', '#btn_add_class', function(event) {
			event.preventDefault();
			$.ajax({
				url: '<?php echo base_url('classes/new_class'); ?>',
				type: 'POST',
				dataType: 'json',
				data: {
					class_name: function(){return $('#class_name').val();},
					class_desc: function(){return $('#class_desc').val();},
					type_choice: function(){return $('#type_choice').val();},
					parent_id: "<?php echo $parent_id; ?>",
				},
			})
			.done(function(response) {
				console.log(response);
				
				if (response.is_folder == 1) {
					loadClasses("<?php echo (!$parent_id)?"0":$parent_id; ?>");
					$('#myModal').modal('hide');
				}
				else if(response.is_folder == 0){
					window.location.href = "<?php echo base_url('classes/stream/'); ?>" + response.data;
				}
				
			})
			.fail(function(jqXHR, textStatus, errorThrown) {
				console.log(errorThrown);
			})

		});
	});
</script>