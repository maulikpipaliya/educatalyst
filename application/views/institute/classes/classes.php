
<div class="dropzone">

	<div class="panel-body">
			<!-- <div class="panel" style="border:1px solid #454d64;">
				<center>
					<a href="#modal_add_class" data-toggle="modal" class="panel-body" style="text-decoration: none;">
						<h4> Add Class </h4>
					</a>	
				</center>	
			</div> -->

			<div class="preview">
				<div class="details">
					<a href="#modal_add_class" data-toggle="modal" class="panel-body" style="text-decoration: none;">
						<center>
							<i class="fa fa-plus-square" style="font-size: 35px;color: #454d64;"></i>
						</center>
					</a>		
				</div>
			</div>

			<?php if (!empty($classes)) :?>
				<?php foreach ($classes as $class) : ?>
					<?php if ($class['is_folder'] == "1"):?>

						<div class="preview" style="border:1px solid #454d64;">

							<div class="details details-folders" data-class_id="<?php echo $class['class_id']; ?>" data-is_folder = "<?php echo $class['is_folder']; ?>" style="">
								<i class="fa fa-folder"></i>
								<div class=""><?php echo $class['class_name']; ?></div>


								<form action="<?php echo base_url('classes/my_classes'); ?>" method="POST" accept-charset="utf-8">
									<a class="panel-body class-info" data-class_id="<?php echo $class['class_id']; ?>" data-is_folder = "<?php echo $class['is_folder']; ?>" style="text-decoration: none; align-self: center;" href="" id="class_selector">
										<input type="hidden" value="<?php echo $class['class_id']; ?>" name="parent_id">

									<!-- <h4 <?php  echo ($class['is_folder'] == "1") ? "style=color:white;" : "" ; ?>>
										<?php echo $class['class_name']; ?>
	
										<span class="text-muted small">
											<?php echo $class['class_desc']; ?>
										</span>
									</h4> -->
									<!-- <div class="filename" style="color: #454d64;"><?php echo $class['class_name']; ?></div> -->
									

									<div class="size" style="color: #454d64;"><?php echo $class['class_desc']; ?></div>


									<button type="submit" id="btn_class_click" style="display: none;" value="hahahlol" class="btn_class_click"></button>
								</a>
							</form>
							
						</div>
					</div>
				<?php endif; ?>
			<?php endforeach; ?>

		<?php endif; ?>		

		<?php if (!empty($classes)) :?>
			<?php foreach ($classes as $class) : ?>
				<?php if ($class['is_folder'] == "0"):?>
				<!-- <div class="col-md-3 col-sm-3" id="div_class">
						<div class="panel btn-<?php echo ($class['is_folder'] == "1") ? "primary" : "default" ?>" style="border: 1px solid #454d64;"">
							<form action="<?php echo base_url('classes'); ?>" method="POST" accept-charset="utf-8">
								<a class="panel-body class-info" data-class_id="<?php echo $class['class_id']; ?>" data-is_folder = "<?php echo $class['is_folder']; ?>" style="text-decoration: none; align-self: center;" id="class_selector">

									<input type="hidden" value="<?php echo $class['class_id']; ?>" name="parent_id">

									<h4 <?php  echo ($class['is_folder'] == "1") ? "style=color:white;" : "" ; ?>>
										<?php echo $class['class_name']; ?>

										<span class="text-muted small">
											<?php echo $class['class_desc']; ?>
										</span>
									</h4>

									<div class="media">

									</div>
									<button type="submit" id="btn_class_click" style="display: none;" value="hahahlol" class="btn_class_click"></button>
								</a>
							</form>
						</div>
					</div>		 -->

					<div class="preview">
						<div class="details details-classes" data-class_id="<?php echo $class['class_id']; ?>" data-is_folder = "<?php echo $class['is_folder']; ?>">
							<i class="fa fa-users"></i>
							<div><?php echo $class['class_name']; ?></div>	
							
							<form action="<?php echo base_url('classes'); ?>" method="POST" accept-charset="utf-8">
								<a class="panel-body class-info" style="text-decoration: none; align-self: center;" id="class_selector">
									

									<input type="hidden" value="<?php echo $class['class_id']; ?>" name="parent_id">

									<!-- <h4 <?php  echo ($class['is_folder'] == "1") ? "style=color:white;" : "" ; ?>>
										<?php echo $class['class_name']; ?>

										<span class="text-muted small">
											<?php echo $class['class_desc']; ?>
										</span>
									</h4> -->
									<!-- <div class="filename"><?php echo $class['class_name']; ?>
									</div> -->
									<div class="size" style="color: #454d64;;"><?php echo $class['class_desc']; ?></div>
									
									<button type="submit" id="btn_class_click" style="display: none;" value="hahahlol" class="btn_class_click"></button>
								</a>
							</form>
							
						</div>
					</div>

				<?php endif; ?>
			<?php endforeach; ?>

		<?php endif; ?>				

	</div>	
</div>



<script>
	function loadClasses(p_id){

		$.ajax({
			url: '<?php echo base_url('classes/load_classes/'); ?>'+ p_id,
			type: 'POST',					
		})
		.done(function(response) {
			$('#div_row').html(response);
			// $('#div_row').getNiceScroll().resize();
			console.log("success");
		})
		.fail(function(jqXHR, textStatus, errorThrown) {
			console.log(errorThrown);
		})
		.always(function() {
			console.log("complete");
		});
	}


	$(document).ready(function() {
		<?php echo $parent_id = $this->input->get('parent'); ?>
		// loadClasses("<?php echo (!$parent_id)?"0":$parent_id; ?>"); 
	});
</script>