<div class="wrapper" style="background-image: linear-gradient(#424F63, white);height: 100vh;">
	<div class="row">
		<div class="col-lg-6">
			<div class="split_left">
				<h2>Pro Tip <i class="fa fa-lightbulb-o"></i></h2>
				<ul>
					<li>Create your real time institute account having given perfect details making it easier to find for people</li>
					<li>One who creates the institute will be Head of the same.</li>
					<li>Subsequently, he can invite Tutors to be a part of the institute who can create classes and invite Tutees and manage them on the go.</li>
				</ul>
			</div>	
		</div>
		<div class="col-lg-6">
			<div class="row">
				<div class="col-lg-2"></div>
				<div class="col-lg-8">
					<section class="panel" style="border-radius: 5px; font-size: 13px;margin-top: 50px;">
						<header class="panel-heading" style="">
							Create New Institute
						</header>

						<div class="panel-body" style="">

							<div class="form">
								<form class="cmxform form-horizontal adminex-form" id="frm_new_institute" name="frm_new_institute" action="<?php echo base_url('institute/insert_institute'); ?>" method="POST" style="">

									<div class="form-group">
										<label for="institute_name" class="control-label col-lg-3">Name</label>
										<div class="col-lg-8">
											<input class="form-control required" id="institute_name" name="institute_name" type="text">

										</div>
									</div>



									<div class="form-group ">
										<label for="institute_type_id" class="control-label col-lg-3">Type</label>
										<div class="col-lg-8">

											<select class="form-control m-bot15 required" id="institute_type_id" name="institute_type_id">
												<option value="">Select Type</option>
												<?php foreach ($institutes as $institute) :?>
													<option value="<?php echo $institute['institute_type_id']; ?>">
														<?php echo $institute['institute_type']; ?>
													</option>
												<?php endforeach; ?>
											</select>

										</div>
									</div>



									<div class="form-group ">
										<label for="location" class="control-label col-lg-3">Location</label>
										<div class="col-lg-4">
											<select class="form-control m-bot15 required" name="dd_states" id="dd_states" onchange="getCities(this.value);getStateCode(this.value);">
												<option value="">Select State</option>
												<?php foreach ($states as $key => $value):?>
													<option value="<?php echo $value['id'];?>">
														<?php echo $value['name'];?>
													</option>
												<?php endforeach; ?>
											</select>
										</div>
										<div class="col-lg-4">
											<select class="form-control m-bot15 required" name="dd_cities" id="dd_cities" onblur="getCityCode(this.value);" onchange="getCityCode(this.value)">
												<option value="">Select City</option>

											</select>
										</div>
									</div>

									<div class="form-group ">
										<label for="address" class="control-label col-lg-3">Address</label>
										<div class="col-lg-8">

											<textarea rows="3" class="form-control required" id="address" name="address"></textarea>
										</div>
									</div>
									<div class="form-group ">
										<label for="postal_code" class="control-label col-lg-3">Postal Code</label>
										<div class="col-lg-8">
											<input type="text" placeholder="" data-mask="999999" class="form-control required" id="postal_code" name="postal_code">
										</div>
									</div>

									<div class="col-lg-10">
										<input class="form-control" id="institute_code" name="institute_code" type="hidden" value="" readonly="true">
									</div>

									<div class="form-group">
										<!-- <label for="institute_name" class="control-label col-lg-3">Postal Code</label> -->

										<div class="col-lg-offset-3 col-lg-8">
											<button class="btn btn-primary" type="submit">Create</button>
											<button class="btn btn-default" type="button">Cancel</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</section></div>
					<div class="col-lg-2"></div>
				</div>

			</div>

		</div>

	</div>

	<script type="text/javascript" src="<?php echo base_url('assets') ?>/js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>

	<script>
		var num;
		$(document).ready(function() {

			$('#frm_new_institute').validate();
			$.ajax({
				url: '<?php echo base_url(''); ?>Institute/get_next_inst_code_trail',
				type: 'POST',
				data: {},
			})
			.done(function(r) {
			// console.log(r);
			num = Number(r)+1;
			Number.prototype.pad = function(size) {
				var s = String(this);
				while (s.length < (size || 2)) {
					s = "0" + s;
				}
				return s;
			}
			num = num.pad(4);
		})
			.fail(function(jqXHR, textStatus, errorThrown) {
				console.log(errorThrown);
			})

		});
	
	num = num.pad(4);

	
	var st = $('#institute_code').val();

	function getCities(state_id){
		if (state_id != "") {
			$.ajax({
				url: '<?php echo base_url(''); ?>users/get_cities/' + state_id,
				type: 'GET',
				data: {state_id: state_id},
			})
			.done(function(cities) {
				console.log("success");
				$('#dd_cities').html("");
				$('#dd_cities').html(cities);
			})
			.fail(function() {
				console.log("error");
			})
		} 
	}

	function getStateCode(state_id){
		if (state_id != "") {
			$.ajax({
				url: '<?php echo base_url(''); ?>common/get_state_code/' + state_id,
				type: 'GET',
				data: {state_id: state_id},
			})
			.done(function(code) {
				// console.log("success");
				// $('#institute_code').val(code);
				// st = $('#institute_code').val();
				st = code;
			})
			.fail(function() {
				console.log("error");
			})
		} 	
	}
	function getCityCode(city_id){
		if (city_id != "") {
			$.ajax({
				
			})
			.done(function() {
				$('#institute_code').val("");
				$('#institute_code').val(st + city_id + "_" + num);
			})
			.fail(function() {
				console.log("error");
			})
		} 	
	}

</script>