<!--body wrapper start-->
<div class="wrapper">
	<div class="row blog">
		<div class="col-md-4">
			<div class="panel">
				<div class="panel-body">

					<label for="institute_code">Institute you want to join in</label>
					<div class="row">
						
						<form method="POST" accept-charset="utf-8" id="frm_inst_code" name="frm_inst_code" action="<?php echo base_url('institute/on_submit'); ?>">

							<div class="col-md-5">

								<input type="text" placeholder="Institute Code" class="form-control blog-search" id="institute_code" name="institute_code" value="GJ1041_0009">

								<!-- static value AN1_0001 for not having to rewrite it everytime until testing, to be removed later -->

							</div>
							<div class="col-md-4">
								<select name="join_as" id="join_as" class="form-control input-md">
									<option value="">Role</option>
									<?php foreach ($roles as $role): ?>
										<option value="<?php echo $role['role_id']; ?>">
											<?php echo $role['role_name']; ?>
										</option>
									<?php endforeach; ?>									
								</select>
							</div>

							<div class="col-md-3">
								<!-- <a href="#" class="btn btn-join"> Join </a> -->
								<!-- <button type="submit" class="btn btn-join btn-success">Join</button> -->



								<button type="submit" id="btn_join_req" name="btn_join_req" class="btn btn-join">Request</button>

							</div>

						</form>
					</div>	
				</div>	
			</div>

			<div class="panel">
				<div class="panel-body">
					<div class="blog-post">
						<h3> Recent Classes </h3>
						<div class="media">
							<a href="javascript:;" class="pull-left">
								<img alt="" src="images/blog/blog-thumb-1.jpg" class=" ">
							</a>
							<div class="media-body">
								<h5 class="media-heading"><a href="javascript:;">02 May 2013 </a></h5>
								<p>
									A
								</p>
							</div>
						</div>
						<div class="media">
							<a href="javascript:;" class="pull-left">
								<img alt="" src="images/blog/blog-thumb-2.jpg" class=" ">
							</a>
							<div class="media-body">
								<h5 class="media-heading"><a href="javascript:;">02 May 2013 </a></h5>
								<p>
									B
								</p>
							</div>
						</div>
						<div class="media">
							<a href="javascript:;" class="pull-left">
								<img alt="" src="images/blog/blog-thumb-3.jpg" class=" ">
							</a>
							<div class="media-body">
								<h5 class="media-heading"><a href="javascript:;">02 May 2013 </a></h5>
								<p>
									C
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="panel">
				<div class="panel-body">
					<div class="blog-post">
						<h3>recent comments</h3>
						<ul>
							<li><a href="javascript:;"><i class="  fa fa-comments-o"></i> admin on Vestiblulum quis dolor </a></li>
							<li><a href="javascript:;"><i class="  fa fa-comments-o"></i> admin on Nam sed arcu tellus</a></li>
							<li><a href="javascript:;"><i class="  fa fa-comments-o"></i> monster002 on Fringilla ut vel ipsum </a></li>
							<li><a href="javascript:;"><i class="  fa fa-comments-o"></i> admin on Vestiblulum quis dolor sit</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="panel">
				<div class="panel-body">
					<div class="blog-post">
						<h3>category</h3>
						<ul>
							<li><a href="javascript:;"><i class="  fa fa-angle-right"></i> Animals</a></li>
							<li><a href="javascript:;"><i class="  fa fa-angle-right"></i> Landscape</a></li>
							<li><a href="javascript:;"><i class="  fa fa-angle-right"></i> Portait</a></li>
							<li><a href="javascript:;"><i class="  fa fa-angle-right"></i> Wild Life</a></li>
							<li><a href="javascript:;"><i class="  fa fa-angle-right"></i> Video</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="panel">
				<div class="panel-body">
					<div class="blog-post">
						<h3>blog archive</h3>
						<ul>
							<li><a href="javascript:;"><i class="  fa fa-angle-right"></i> May 2013</a></li>
							<li><a href="javascript:;"><i class="  fa fa-angle-right"></i> April 2013</a></li>
							<li><a href="javascript:;"><i class="  fa fa-angle-right"></i> March 2013</a></li>
							<li><a href="javascript:;"><i class="  fa fa-angle-right"></i> February 2013</a></li>
							<li><a href="javascript:;"><i class="  fa fa-angle-right"></i> January 2013</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-8">
			<div class="blog">
				<div class="single-blog">
					<div class="panel">
						<div class="panel-body">
							<h1 class="text-center mtop35"><a href="#">Neque porro quisquam est qui dolo rem ipsum quio</a></h1>
							<p class="text-center auth-row">
								By <a href="#">Anthony Jones</a>   |   27 December 2014   | <a href="#">5 Comments</a>
							</p>
							<div class="blog-img-wide">
								<img src="images/blog/blog-wide-img.jpg" alt="">
							</div>
							<p>
								Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?
							</p>
							<p>
								Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?
							</p>

							<blockquote>
								<p>
									Vestibulum id ligula porta felis euismod semper. Sed posuere consectetur est at lobortis. Aenean eu leo quam. 				Pellentesque ornare sem lacinia quam venenatis vestibulum. Duis mollis, est non commodo luctus, nisi erat port			titor ligula, eget lacinia odio sem nec elit.
								</p>
							</blockquote>

							<p>
								consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam.

							</p>

							<div class="blog-tags">
								TAGS <a href="#">photoshop</a> <a href="#">illustrator</a> <a href="#">adobe</a> <a href="#">theme</a>
								<div class="pull-right tag-social">
									<a href="#" class="btn btn-share pull-right">Share</a>

									<ul class="pull-right">
										<li>
											<a href="#">
												<i class="fa fa-facebook"></i>
											</a>
										</li>
										<li>
											<a href="#">
												<i class="fa fa-twitter"></i>
											</a>
										</li>
										<li>
											<a href="#">
												<i class="fa fa-google-plus"></i>
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="panel">
						<div class="panel-body ">
							<div class="media blog-cmnt">
								<a href="javascript:;" class="pull-left">
									<img alt="" src="images/blog/blog-avatar.jpg" class="media-object">
								</a>
								<div class="media-body">
									<h4 class="media-heading">
										<a href="#">jones</a>
									</h4>
									<p class="mp-less">
										Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae.
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="panel">
						<div class="panel-body">
							<h1 class="text-center cmnt-head">5 Comments</h1>
							<div class="media blog-cmnt">
								<a href="javascript:;" class="pull-left">
									<img alt="" src="images/blog/blog-avatar-2.jpg" class="media-object">
								</a>
								<div class="media-body">
									<h4 class="media-heading">
										<a href="#">jones</a>
									</h4>
									<div class="bl-status">
										<span class="pull-left">About 10 Min ago</span>
										<a href="#" class="pull-right reply">Reply</a>
									</div>
									<p class="mp-less">
										Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit.
									</p>
								</div>
							</div>
							<div class="media blog-cmnt">
								<a href="javascript:;" class="pull-left">
									<img alt="" src="images/blog/blog-avatar.jpg" class="media-object">
								</a>
								<div class="media-body">
									<h4 class="media-heading">
										<a href="#">jones</a>
									</h4>
									<div class="bl-status">
										<span class="pull-left">About 10 Min ago</span>
										<a href="#" class="pull-right reply">Reply</a>
									</div>
									<p class="mp-less">
										Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit.
									</p>
									<div class="media blog-cmnt">
										<a href="javascript:;" class="pull-left">
											<img alt="" src="images/blog/blog-avatar-2.jpg" class="media-object-child">
										</a>
										<div class="media-body">
											<h4 class="media-heading">
												<a href="#">jones</a>
											</h4>
											<div class="bl-status">
												<span class="pull-left">About 10 Min ago</span>
												<!--<a href="#" class="pull-right reply">Reply</a>-->
											</div>
											<p class="mp-less">
												Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit.
											</p>
											<div class="media blog-cmnt">
												<a href="javascript:;" class="pull-left">
													<img alt="" src="images/blog/blog-avatar.jpg" class="media-object-child">
												</a>
												<div class="media-body">
													<h4 class="media-heading">
														<a href="#">gomez</a>
													</h4>
													<div class="bl-status">
														<span class="pull-left">About 10 Min ago</span>
														<!--<a href="#" class="pull-right reply">Reply</a>-->
													</div>
													<p class="mp-less">
														Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit.
													</p>
													<div class="media blog-cmnt">
														<a href="javascript:;" class="pull-left">
															<img alt="" src="images/blog/blog-avatar-2.jpg" class="media-object-child">
														</a>
														<div class="media-body">
															<h4 class="media-heading">
																<a href="#">gomez</a>
															</h4>
															<div class="bl-status">
																<span class="pull-left">About 10 Min ago</span>
																<!--<a href="#" class="pull-right reply">Reply</a>-->
															</div>
															<p class="mp-less">
																Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit.
															</p>
															<div class="media blog-cmnt">
																<a href="javascript:;" class="pull-left">
																	<img alt="" src="images/blog/blog-avatar.jpg" class="media-object-child">
																</a>
																<div class="media-body">
																	<h4 class="media-heading">
																		<a href="#">gomez</a>
																	</h4>
																	<div class="bl-status">
																		<span class="pull-left">About 10 Min ago</span>
																		<!--<a href="#" class="pull-right reply">Reply</a>-->
																	</div>
																	<p class="mp-less">
																		Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam,
																	</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="panel">
						<div class="panel-body">
							<h1 class="text-center cmnt-head ">Leave a Comments</h1>
							<p class="text-center fade-txt">If you want you can <a href="#">Cancel Reply</a></p>

							<form role="form" class="form-horizontal leave-cmnt">
								<div class="form-group">
									<div class="col-lg-12">
										<input type="text" class="col-lg-12 form-control" placeholder="Name *">
									</div>
								</div>
								<div class="form-group">
									<div class="col-lg-12">
										<input type="text" class="col-lg-12 form-control" placeholder="Email *">
									</div>

								</div>
								<div class="form-group">
									<div class="col-lg-12">
										<textarea class=" form-control" rows="8" placeholder="Message"></textarea>
									</div>
								</div>
								<p>
									<button class="btn btn-post-cmnt pull-right" type="submit">Post Comment</button>
								</p>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
<!--body wrapper end-->

<script>

	$(document).ready(function(){
		$("#frm_inst_code").validate({
			rules: {
				institute_code: {
					remote: {
						url: "<?php echo base_url(''); ?>institute/institute_code_exists",
						type: "POST",
						data: {
							institute_code: function(){ 
								return $("#institute_code").val(); 
							}
						}
					}
				}
			},
			messages: {
				institute_code: {
					remote: "Sorry, that doesn't exist yet!"
				}
			}

		});
		
		$('#join_as').change(function(e) {
			$.ajax({
				url: '<?php echo base_url('institute/is_valid') ?>',
				type: 'POST',
				debug: true,
				data: {
					'institute_code': function(){return $("#institute_code").val();},
					'role_id' : function(){return $("#join_as").val();}
				}	
			})
			.done(function(response) {
				console.log(response);
				if (response == "true") {
					$('#btn_join_req').addClass('btn-success');
					$('#btn_join_req').html("Join");
				}
				else{
					$('#btn_join_req').html("Request");	
				}

			})
			.fail(function() {
				console.log("error01");
			})
			

			$.ajax({
				url: '<?php echo base_url('users/request_status') ?>',
				type: 'POST',
				debug: true,
				data: {
					'institute_code': function(){return $("#institute_code").val();},
					'role_id' : function(){return $("#join_as").val();}
				}	
			})
			.done(function(r) {
				console.log(r);
				if (r == "pending") {
					$('#btn_join_req').html("Requested");
					$('#btn_join_req').attr({
						disabled: true
					});
				}
				else if(r == "accepted"){
					$('#btn_join_req').html("Join");
					$('#btn_join_req').attr({
						disabled: false
					});	
				}
				else if(r == "blocked"){
					$('#btn_join_req').html("Cant Join");
					$('#btn_join_req').attr({
						disabled: false
					});	
				}
				else if(r == "declined"){
					$('#btn_join_req').html("Request");
					$('#btn_join_req').attr({
						disabled: false
					});	
				}
				else{
					// $('#btn_join_req').html("Request");
					$('#btn_join_req').attr({
						disabled: false
					});	

				}

			})
			.fail(function() {
				console.log("error02");
			})

			

		});
	});

</script>