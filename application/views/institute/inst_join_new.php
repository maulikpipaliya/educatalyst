<!--body wrapper start-->
<div class="wrapper" style="background-image: linear-gradient(#424F63, white);height: 100vh;">
	<div class="row blog" style="margin-top: 100px;">
		<div class="col-md-4" style="color: white;">

		</div>
		<div class="col-md-4">
			<div class="panel" style="background-color: transparent; border: 0px solid;">
				<center> 
					<div class="panel-heading" style="color: white; border-bottom: 0px;">
						Code you've been given to join in
					</div>
					<div class="panel-body" style="height: 150px;width: 300px;">

						<label for="institute_code"></label>
						<div class="row">

							<form method="POST" accept-charset="utf-8" id="frm_inst_code" name="frm_inst_code" action="<?php echo base_url('institute/on_join_submit'); ?>" style="width: 400px;">

								<div class="col-md-7">

									<input type="text" placeholder="Code" class="form-control input-lg m-bot15 blog-search required" id="join_code" name="join_code" value="">

									<label for="join_code" class="error" style="color: #454d64;"></label>

								</div>


								<div class="col-md-2">
									<button type="submit" id="btn_join_req" name="btn_join_req" class="btn btn-join" style="background-color: #454d64; border: 1px solid white">Join</button>

								</div>

							</form>
						</div>	
						<?php 
						if ($this->session->flashdata('request_status') == "pending") {
							?>
							<div class="alert alert-danger">
								Request isn't accepted yet!
							</div>

							<?php
						}
						elseif ($this->session->flashdata('request_status') == "sent") {
							?>
							<div class="alert alert-info">
								Request sent!
							</div>
							<?php
						}
						?>
						<div class="row">
							<div class="col-md-12">
								If you don't know what this code is, here's how it works!
								<a class="popovers" data-trigger="hover" data-placement="bottom" data-content="You'd be given code for joining institue depending upon what role you want to join as. You must have a code to join and continue. Happy and Secure Learning !" data-original-title="Flow of Code" style="color: #454d64">how it works</a>
							</div>
						</div>
					</div>	
				</center>
			</div>
		</div>

		<div class="col-md-4"></div>

	</div>
</div>
<!--body wrapper end-->

<script>

	$(document).ready(function(){
		$("#frm_inst_code").validate({
			rules: {
				join_code: {
					remote: {
						url: "<?php echo base_url(''); ?>institute/join_code_exists",
						type: "POST",
						data: {
							join_code: function(){ 
								return $("#join_code").val(); 
							}
						}
					}
				}
			},
			messages: {
				join_code: {
					remote: "Sorry, that doesn't exist yet!"
				}
			}

		});

		$('#join_code').change(function(e) {
			$.ajax({
				url: '<?php echo base_url('institute/is_valid') ?>',
				type: 'POST',
				debug: true,
				data: {
					'institute_code': function(){return $("#institute_code").val();},
					'role_id' : function(){return $("#join_as").val();}
				}	
			})
			.done(function(response) {
				console.log(response);
				if (response == "true") {
					$('#btn_join_req').addClass('btn-success');
					$('#btn_join_req').html("Join");
				}
				else{
					$('#btn_join_req').html("Join");	
				}

			})
			.fail(function() {
				console.log("error01");
			})


			$.ajax({
				url: '<?php echo base_url('users/request_status') ?>',
				type: 'POST',
				debug: true,
				data: {
					'institute_code': function(){return $("#institute_code").val();},
					'role_id' : function(){return $("#join_as").val();}
				}	
			})
			.done(function(r) {
				console.log(r);
				if (r == "pending") {
					$('#btn_join_req').html("Join");
					$('#btn_join_req').attr({
						disabled: true
					});
				}
				else if(r == "accepted"){
					$('#btn_join_req').html("Join");
					$('#btn_join_req').attr({
						disabled: false
					});	
				}
				else if(r == "blocked"){
					$('#btn_join_req').html("Cant Join");
					$('#btn_join_req').attr({
						disabled: false
					});	
				}
				else if(r == "declined"){
					$('#btn_join_req').html("Join");
					$('#btn_join_req').attr({
						disabled: false
					});	
				}
				else{
					// $('#btn_join_req').html("Request");
					$('#btn_join_req').attr({
						disabled: false
					});	

				}

			})
			.fail(function(jqXHR, textStatus, errorThrown) {
				console.log(errorThrown);
			})



		});
	});

</script>