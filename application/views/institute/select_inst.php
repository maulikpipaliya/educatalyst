<div class="wrapper" style="background-image: linear-gradient(#424F63, white);height: 100vh;">
	<div class="row">
		<div class="col-md-4"></div>
		
		<div class="col-md-4">
			<?php if(!empty($user_institutes)): ?>
				<div class="panel" style="background-color: transparent;">
					<div class="panel-heading" style="color: white; font-size: 15px; text-transform: none">
						Select an institute
					</div>
					<div class="panel-body" style="background-color: white;">
						<div class="dir-info">
							<?php foreach ($user_institutes as $user_institute) :?>
								<div class="row">

									<div class="col-xs-3">
										<div class="avatar">
											<img src="images/photos/user2.png" alt="">
										</div>
									</div>

									<div class="col-xs-6">
										<h5><?php echo $user_institute['institute_name']; ?></h5>
										<span class="small" style="color: #50cea1;"><?php echo $user_institute['role_name']; ?></span>
									</div>

									<div class="col-xs-3">
										<a class="dir-like">

											<form class="hidden_form" action="<?php echo base_url('institute/dive_in') ?>" style="display: none" method="POST" accept-charset="utf-8">

												<input type="hidden" name="institute_code" value="<?php echo $user_institute['institute_code']; ?>">

												<input type="hidden" name="role_id" value="<?php echo $user_institute['role_id']; ?>">

												<button type="submit" id="form_btn_join" class="form_btn_join"></button>
											</form>

											<a id="btn_join_institute" name="btn_join_institute" class="btn btn-primary btn_join_institute" data-institute_code="<?php echo $user_institute['institute_code']; ?>" data-role_id= "<?php echo $user_institute['role_id']; ?>">
												Join
											</a>
										</a>
									</div> 
								</div>
							<?php endforeach; ?>

						</div>

					</div>
				</div>
			<?php else: ?>
				<h3 style="color: white;">You must <span><a href="<?php echo base_url('institute/join'); ?>">join institute</a></span> first to continue...</h3>
			<?php endif;?>

		</div>
	</div>
	<div class="col-md-4"></div>


</div>

<script>



	$(document).ready(function() {
		$('.btn_join_institute').click(function(event) {
			var $this = $(this);
			var institute_code = $(this).data('institute_code');
			var role_id = $(this).data('role_id');
					
				$.ajax({
					url: '<?php echo base_url('institute/dive_in') ?>',
					type: 'POST',
					debug: true,
					data: {
						institute_code: institute_code,
						role_id: role_id,
					},
				})
				.done(function(response) {
					console.log("success");
					
				})
				.fail(function() {
					console.log("error");
				})
				.always(function() {
					console.log("complete");
				});

				$(this).parent().find('#form_btn_join').click();

				
			});


	});


</script>