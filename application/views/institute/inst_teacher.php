<?php
$ssn_inst_join =  $this->session->userdata('ssn_inst_join'); 
$ssn_inst_details =  $this->session->userdata('ssn_inst_details'); 
?>

<!--nestable css-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets') ?>/js/nestable/jquery.nestable.css" />

<div class="wrapper">
	<div class="row blog">
		<div class="col-md-4">
			<div class="panel">
				<div class="panel-body">
					<!-- <label for="institute_code">Institute you've joined</label> -->

					<div class="row">
						<div class="col-md-9">

							<b><?php echo $institute_name; ?></b>

							<span>as</span>									
							<?php 
							echo $role_name;
							
							?>	
						</div>

						<div class="col-md-2">


							<form action="<?php echo base_url('institute/change_pos') ?>" method="post" accept-charset="utf-8">
								<button id="btn_change" type="submit">Change</button>						
							</form>
						</div>
						<div class="col-md-2">
							
						</div>
					</div>	


				</div>
			</div>
			<div class="panel">
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12">
							
							<a id="s_code_generator">Code</a>
							<span>for Students to join in</span>
						</div>
						
					</div>
					<div class="row">
						<div class="col-md-12">
							<h3>
								<span id="t_code">
									
								</span>
								<button id="btn_copy" style="display: none" class="btn btn-primary pull-right">Copy</button>

							</h3>
						</div>
					</div>
				</div>
			</div>
			<section class="panel">
				<header class="panel-heading">
					Nestable List 1
					<span class="tools pull-right">
						<a class="fa fa-chevron-down" href="javascript:;"></a>
						<a class="fa fa-times" href="javascript:;"></a>
					</span>
				</header>
				<div class="panel-body">
					<div class="dd" id="nestable_list_1">
						<ol class="dd-list">
							<li class="dd-item" data-id="1">
								<div class="dd-handle">Item 1</div>
							</li>
							<li class="dd-item" data-id="2">
								<div class="dd-handle">Item 2</div>
								<ol class="dd-list">
									<li class="dd-item" data-id="3">
										<div class="dd-handle">Item 3</div>
									</li>
									<li class="dd-item" data-id="4">
										<div class="dd-handle">Item 4</div>
									</li>
									<li class="dd-item" data-id="5">
										<div class="dd-handle">Item 5</div>
										<ol class="dd-list">
											<li class="dd-item" data-id="6">
												<div class="dd-handle">Item 6</div>
											</li>
											<li class="dd-item" data-id="7">
												<div class="dd-handle">Item 7</div>
											</li>
											<li class="dd-item" data-id="8">
												<div class="dd-handle">Item 8</div>
											</li>
										</ol>
									</li>
									<li class="dd-item" data-id="9">
										<div class="dd-handle">Item 9</div>
									</li>
									<li class="dd-item" data-id="10">
										<div class="dd-handle">Item 10</div>
									</li>
								</ol>
							</li>
							<li class="dd-item" data-id="11">
								<div class="dd-handle">Item 11</div>
							</li>
							<li class="dd-item" data-id="12">
								<div class="dd-handle">Item 12</div>
							</li>
						</ol>
					</div>
				</div>
			</section>
			<div class="panel">
				<div class="panel-body">
					<div class="blog-post">
						<h3> Recent Classes </h3>
						<div class="media">
							<a href="javascript:;" class="pull-left">
								<img alt="" src="images/blog/blog-thumb-1.jpg" class=" ">
							</a>
							<div class="media-body">
								<h5 class="media-heading"><a href="javascript:;">02 May 2013 </a></h5>
								<p>
									A
								</p>
							</div>
						</div>
						<div class="media">
							<a href="javascript:;" class="pull-left">
								<img alt="" src="images/blog/blog-thumb-2.jpg" class=" ">
							</a>
							<div class="media-body">
								<h5 class="media-heading"><a href="javascript:;">02 May 2013 </a></h5>
								<p>
									B
								</p>
							</div>
						</div>
						<div class="media">
							<a href="javascript:;" class="pull-left">
								<img alt="" src="images/blog/blog-thumb-3.jpg" class=" ">
							</a>
							<div class="media-body">
								<h5 class="media-heading"><a href="javascript:;">02 May 2013 </a></h5>
								<p>
									C
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="panel">
				<div class="panel-body">
					<div class="blog-post">
						<h3>recent comments</h3>
						<ul>
							<li><a href="javascript:;"><i class="  fa fa-comments-o"></i> admin on Vestiblulum quis dolor </a></li>
							<li><a href="javascript:;"><i class="  fa fa-comments-o"></i> admin on Nam sed arcu tellus</a></li>
							<li><a href="javascript:;"><i class="  fa fa-comments-o"></i> monster002 on Fringilla ut vel ipsum </a></li>
							<li><a href="javascript:;"><i class="  fa fa-comments-o"></i> admin on Vestiblulum quis dolor sit</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="panel">
				<div class="panel-body">
					<div class="blog-post">
						<h3>category</h3>
						<ul>
							<li><a href="javascript:;"><i class="  fa fa-angle-right"></i> Animals</a></li>
							<li><a href="javascript:;"><i class="  fa fa-angle-right"></i> Landscape</a></li>
							<li><a href="javascript:;"><i class="  fa fa-angle-right"></i> Portait</a></li>
							<li><a href="javascript:;"><i class="  fa fa-angle-right"></i> Wild Life</a></li>
							<li><a href="javascript:;"><i class="  fa fa-angle-right"></i> Video</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="panel">
				<div class="panel-body">
					<div class="blog-post">
						<h3>blog archive</h3>
						<ul>
							<li><a href="javascript:;"><i class="  fa fa-angle-right"></i> May 2013</a></li>
							<li><a href="javascript:;"><i class="  fa fa-angle-right"></i> April 2013</a></li>
							<li><a href="javascript:;"><i class="  fa fa-angle-right"></i> March 2013</a></li>
							<li><a href="javascript:;"><i class="  fa fa-angle-right"></i> February 2013</a></li>
							<li><a href="javascript:;"><i class="  fa fa-angle-right"></i> January 2013</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-8">
			<div class="blog">
				<div class="single-blog">
					<div class="panel">
						<div class="panel-body">
							<h1 class="text-center mtop35"><a href="#">Neque porro quisquam est qui dolo rem ipsum quio</a></h1>
							<p class="text-center auth-row">
								By <a href="#">Anthony Jones</a>   |   27 December 2014   | <a href="#">5 Comments</a>
							</p>
							<div class="blog-img-wide">
								<img src="images/blog/blog-wide-img.jpg" alt="">
							</div>
							<p>
								Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?
							</p>
							<p>
								Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?
							</p>

							<blockquote>
								<p>
									Vestibulum id ligula porta felis euismod semper. Sed posuere consectetur est at lobortis. Aenean eu leo quam. 				Pellentesque ornare sem lacinia quam venenatis vestibulum. Duis mollis, est non commodo luctus, nisi erat port			titor ligula, eget lacinia odio sem nec elit.
								</p>
							</blockquote>

							<p>
								consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam.

							</p>

							<div class="blog-tags">
								TAGS <a href="#">photoshop</a> <a href="#">illustrator</a> <a href="#">adobe</a> <a href="#">theme</a>
								<div class="pull-right tag-social">
									<a href="#" class="btn btn-share pull-right">Share</a>

									<ul class="pull-right">
										<li>
											<a href="#">
												<i class="fa fa-facebook"></i>
											</a>
										</li>
										<li>
											<a href="#">
												<i class="fa fa-twitter"></i>
											</a>
										</li>
										<li>
											<a href="#">
												<i class="fa fa-google-plus"></i>
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="panel">
						<div class="panel-body ">
							<div class="media blog-cmnt">
								<a href="javascript:;" class="pull-left">
									<img alt="" src="images/blog/blog-avatar.jpg" class="media-object">
								</a>
								<div class="media-body">
									<h4 class="media-heading">
										<a href="#">jones</a>
									</h4>
									<p class="mp-less">
										Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae.
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="panel">
						<div class="panel-body">
							<h1 class="text-center cmnt-head">5 Comments</h1>
							<div class="media blog-cmnt">
								<a href="javascript:;" class="pull-left">
									<img alt="" src="images/blog/blog-avatar-2.jpg" class="media-object">
								</a>
								<div class="media-body">
									<h4 class="media-heading">
										<a href="#">jones</a>
									</h4>
									<div class="bl-status">
										<span class="pull-left">About 10 Min ago</span>
										<a href="#" class="pull-right reply">Reply</a>
									</div>
									<p class="mp-less">
										Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit.
									</p>
								</div>
							</div>
							<div class="media blog-cmnt">
								<a href="javascript:;" class="pull-left">
									<img alt="" src="images/blog/blog-avatar.jpg" class="media-object">
								</a>
								<div class="media-body">
									<h4 class="media-heading">
										<a href="#">jones</a>
									</h4>
									<div class="bl-status">
										<span class="pull-left">About 10 Min ago</span>
										<a href="#" class="pull-right reply">Reply</a>
									</div>
									<p class="mp-less">
										Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit.
									</p>
									<div class="media blog-cmnt">
										<a href="javascript:;" class="pull-left">
											<img alt="" src="images/blog/blog-avatar-2.jpg" class="media-object-child">
										</a>
										<div class="media-body">
											<h4 class="media-heading">
												<a href="#">jones</a>
											</h4>
											<div class="bl-status">
												<span class="pull-left">About 10 Min ago</span>
												<!--<a href="#" class="pull-right reply">Reply</a>-->
											</div>
											<p class="mp-less">
												Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit.
											</p>
											<div class="media blog-cmnt">
												<a href="javascript:;" class="pull-left">
													<img alt="" src="images/blog/blog-avatar.jpg" class="media-object-child">
												</a>
												<div class="media-body">
													<h4 class="media-heading">
														<a href="#">gomez</a>
													</h4>
													<div class="bl-status">
														<span class="pull-left">About 10 Min ago</span>
														<!--<a href="#" class="pull-right reply">Reply</a>-->
													</div>
													<p class="mp-less">
														Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit.
													</p>
													<div class="media blog-cmnt">
														<a href="javascript:;" class="pull-left">
															<img alt="" src="images/blog/blog-avatar-2.jpg" class="media-object-child">
														</a>
														<div class="media-body">
															<h4 class="media-heading">
																<a href="#">gomez</a>
															</h4>
															<div class="bl-status">
																<span class="pull-left">About 10 Min ago</span>
																<!--<a href="#" class="pull-right reply">Reply</a>-->
															</div>
															<p class="mp-less">
																Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit.
															</p>
															<div class="media blog-cmnt">
																<a href="javascript:;" class="pull-left">
																	<img alt="" src="images/blog/blog-avatar.jpg" class="media-object-child">
																</a>
																<div class="media-body">
																	<h4 class="media-heading">
																		<a href="#">gomez</a>
																	</h4>
																	<div class="bl-status">
																		<span class="pull-left">About 10 Min ago</span>
																		<!--<a href="#" class="pull-right reply">Reply</a>-->
																	</div>
																	<p class="mp-less">
																		Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam,
																	</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="panel">
						<div class="panel-body">
							<h1 class="text-center cmnt-head ">Leave a Comments</h1>
							<p class="text-center fade-txt">If you want you can <a href="#">Cancel Reply</a></p>

							<form role="form" class="form-horizontal leave-cmnt">
								<div class="form-group">
									<div class="col-lg-12">
										<input type="text" class="col-lg-12 form-control" placeholder="Name *">
									</div>
								</div>
								<div class="form-group">
									<div class="col-lg-12">
										<input type="text" class="col-lg-12 form-control" placeholder="Email *">
									</div>

								</div>
								<div class="form-group">
									<div class="col-lg-12">
										<textarea class=" form-control" rows="8" placeholder="Message"></textarea>
									</div>
								</div>
								<p>
									<button class="btn btn-post-cmnt pull-right" type="submit">Post Comment</button>
								</p>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
<!--body wrapper end-->


<script>
	
	$(document).ready(function(){
		var light = 0;
		$('#s_code_generator').click(function(event) {

			light = light + 1 ;

			if (light == 1) {
				$.ajax({
					url: '<?php echo base_url('common/generate_student_code') ?>',
					debug: true,
					type: 'POST',
				})
				.done(function(response) {
					$('#t_code').html(response);
					$('#btn_copy').css('display', '');


				})
				.fail(function() {
					console.log("error: common/generate_teacher_code");
				})
			}
			return false;
		});

		$('#btn_copy').click(function(event) {
			// var myText = $('#t_code').text();
			// myText.select();
			// document.execCommand("copy");
			// console.log(myText.value);

			var $temp = $("<input>");
			$("body").append($temp);
			$temp.val($('#t_code').text()).select();
			document.execCommand("copy");
			$('#btn_copy').html("Copied");
			var copiedText = $temp.val();
			$temp.remove();
			// alert(copiedText);

		});
	});

</script>

<script src="<?php echo base_url('assets'); ?>/js/nestable/jquery.nestable.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/nestable-init.js"></script>