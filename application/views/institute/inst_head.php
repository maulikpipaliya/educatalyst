    

<!--body wrapper start-->
<div class="wrapper" style="background-image: linear-gradient(white,#424F63);height: 100vh;">
	
	<div class="row blog">
		<div class="col-md-3">
			<!-- left side start-->

			<div class="left-side-inner" style="background-color: #353f4f;">

				<!-- visible to small devices only -->
				<div class="visible-xs hidden-sm hidden-md hidden-lg">
					<div class="media logged-user">
						<img alt="" src="images/photos/user-avatar.png" class="media-object">
						<div class="media-body">
							<h4><a href="#"></a></h4>
							<span></span>
						</div>
					</div>

					<h5 class="left-nav-title">Account Information</h5>
					<ul class="nav nav-pills nav-stacked custom-nav">
						<li><a href="#"><i class="fa fa-user"></i> <span>Profile</span></a></li>
						<li><a href="#"><i class="fa fa-cog"></i> <span>Settings</span></a></li>
						<li><a href="#"><i class="fa fa-sign-out"></i> <span>Sign Out</span></a></li>
					</ul>
				</div>

				<!--sidebar nav start-->
				<ul class="nav nav-pills nav-stacked custom-nav">

					<li>
						<a href="<?php echo base_url('institute/head/')."?"."tab=current_institute" ?>"><i class="fa fa-home"></i> <span>Current Institute</span></a>
						
					</li>

					<li><a href="<?php echo base_url('institute/head/')."?"."tab=invite"; ?>"><i class="fa fa-laptop"></i> <span>Invite Teachers</span></a>
						
					</li>

					<li><a href="<?php echo base_url('institute/head/')."?"."tab=requests"; ?>"><i class="fa fa-laptop"></i> <span>Requests</span></a>
						
					</li>

					<li class="menu-list"><a href=""><i class="fa fa-book"></i> <span>Teachers</span></a>
						<ul class="sub-menu-list">
							<li><a href="<?php echo base_url('institute/head/')."?"."tab=list_teachers" ?>"> List</a></li>
							<li><a href="buttons.html"> Block</a></li>
						</ul>
					</li>
					

					<li><a href="#"><i class="fa fa-map-marker"></i> <span>Circular</span></a>
						
					</li>
					

				</ul>
				<!--sidebar nav end-->

			</div>

			<!-- left side end-->
		</div>
		<div class="col-md-9">

			<?php if ($this->input->get()) {
				if ($this->input->get('tab') == "current_institute") {
					?>
					<div class="panel">
						<div class="panel-body">

							<div class="row">
								<div class="col-md-9">
									<h4>
										
										<div class="row">
											<div class="col-md-12">
												<span>Insitute you've joined is :</span>
												<span>as</span>									
												<?php print_r($role_name); ?>


											</div>
										</div>

										<div class="row">
											<div class="col-md-2 alert">Name</div>
											<div class="col-md-8">
												<div class="alert">
													<?php echo $ssn_inst_details['institute_name'];?>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-2 alert">Address</div>
											<div class="col-md-8">
												<div class="alert">
													<?php echo $ssn_inst_details['address'];?>
												</div>
											</div>
										</div>


										
									</h4>
								</div>

								<div class="col-md-2">

									<form action="<?php echo base_url('institute/change_pos') ?>" method="post" accept-charset="utf-8">
										<button id="btn_change" class="btn btn-primary pull-right" type="submit">Change</button>						
									</form>

								</div>
								<div class="col-md-2">
									<!-- <a href="#" class="btn btn-join"> Join </a> -->
									<!-- <button type="submit" class="btn btn-join">Join</button> -->
								</div>

							</div>	
						</div>
					</div>
					<?php
				}
				elseif ($this->input->get('tab') == "invite") {
					?>
					<div class="panel" style="height: 300px;">
						<div class="panel-body">
							<div class="row">
								<div class="col-md-12">
									<a id="t_code_generator">Code</a>
									<span>for Teachers to join in</span>
								</div>

							</div>
							<div class="row">
								<div class="col-md-9">
									<h1 style="margin-top: 50px;">
										<font size="20">
											<span id="t_code">

											</span>
										</font>
										<button id="btn_copy" style="display: none" class="btn btn-primary btn-lg pull-right">Copy</button>

									</h1>
								</div>
							</div>
						</div>
					</div>
					<?php
				}
				elseif ($this->input->get('tab') == "list_teachers") {
					?>
					<div class="panel">
						<div class="panel-body">
							<div class="blog-post">
								<h3> Teachers </h3>
								<table class="table">
									<thead>
										<tr>
											<th>Name</th>
											<th>Email</th>
											<th>Username</th>
											<th>Mobile</th>
											<th>Gender</th>
											<th>Birthdate</th>
											<th>Block</th>
										</tr>
									</thead>
									<tbody>
										<?php
										if(!empty($teachers))
										{
											foreach($teachers as $teacher)
											{
												?>
												<tr>
													<td><?php echo $teacher->firstname." ".$teacher->lastname; ?></td>
													<td><?php echo $teacher->email; ?></td>
													<td><?php echo $teacher->username; ?></td>
													<td><?php echo $teacher->mobile; ?></td>
													<td><?php echo ($teacher->gender)?"Male":"Female"; ?></td>
													<td><?php echo $teacher->birthdate; ?></td>
													<td><button class="btn btn-xs <?php echo ($teacher->is_blocked == 1)?"btn-success":"btn-danger"; ?> btn_block" data-teacher_id="<?php echo $teacher->user_id; ?>" data-is_blocked="<?php echo ($teacher->is_blocked == 1)?'0':'1'; ?>" data-institute_code="<?php echo $teacher->institute_code; ?>"><?php echo ($teacher->is_blocked == 1)?"Unblock":"Block"; ?></button></td>
												</tr>
												<?php
											}	
										}
										else
										{
											?>
											<tr>
												<td colspan="7">No teachers yet!</td>
											</tr>
											<?php
										}
										?>
										<tr>
										</tr>
									</tbody>
								</table>	
							</div>
						</div>
					</div>

					<?php
				}
				elseif ($this->input->get('tab') == "requests") {
					?>
					<div class="panel">
						<div class="panel-body">
							<div id="div_requests">
								<div class="directory-info-row">
									<?php if (!empty($requests)) :?>

										<?php foreach ($requests as $request) :?>
											<div class="row">
												<div class="col-md-1 col-sm-1">
												</div>   

												<div class="col-md-6 col-sm-6">
													<div class="panel">

														<div class="panel-body">

															<h4 class="sample" data-user_id="<?php echo $request['user_id'] ?>">
																<?php echo $request['FullName']; ?>

																<span class="text-muted small"> - Graphics Designer</span>

																<button id="btn_reject" class="btn btn-danger pull-right btn_reject" data-req_id="<?php echo $request['request_id']; ?>">Reject</button>

																<button id="btn_accept" class="btn btn-success pull-right btn_accept" data-req_id="<?php echo $request['request_id']; ?>" style="margin-right: 10px;">Accept</button>
															</h4>


															<div class="media">
																<a class="pull-left" href="#">
																	<img class="thumb media-object" src="images/photos/user2.png" alt="">
																</a>
																<div class="media-body">
																	<address>
																		<strong>ABCDE, Inc.</strong>

																	</address>

																</div>
															</div>
														</div>
													</div>

												</div>
												<div class="col-md-3 col-sm-3">
												</div>   
											</div>
										<?php endforeach; ?>
									<?php else: ?>
										<span>No requests</span>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
					<?php
				}

			}
			else {
				?>
				<div class="panel">
					<div class="panel-body">

						<div class="row">
							<div class="col-md-9">
								<span>Insitute you've joined is :</span>
								<?php echo $ssn_inst_details['institute_name']; ?>

								<span>as</span>									
								<?php print_r($role_name); ?>
							</div>

							<div class="col-md-2">

								<form action="<?php echo base_url('institute/change_pos') ?>" method="post" accept-charset="utf-8">
									<button id="btn_change" type="submit">Change</button>						
								</form>

							</div>
							<div class="col-md-2">
								<!-- <a href="#" class="btn btn-join"> Join </a> -->
								<!-- <button type="submit" class="btn btn-join">Join</button> -->
							</div>

						</div>	
					</div>
				</div>
				<?php

			} ?>




		</div>
<!-- <div class="col-md-5">
			<div class="blog">
				<div class="single-blog">
					<div class="panel">
						<div class="panel-body">
							<h1 class="text-center mtop35"><a href="#">Neque porro quisquam est qui dolo rem ipsum quio</a></h1>
							<p class="text-center auth-row">
								By <a href="#">Anthony Jones</a>   |   27 December 2014   | <a href="#">5 Comments</a>
							</p>
							<div class="blog-img-wide">
								<img src="images/blog/blog-wide-img.jpg" alt="">
							</div>
							<p>
								Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?
							</p>
							<p>
								Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?
							</p>

							<blockquote>
								<p>
									Vestibulum id ligula porta felis euismod semper. Sed posuere consectetur est at lobortis. Aenean eu leo quam. 				Pellentesque ornare sem lacinia quam venenatis vestibulum. Duis mollis, est non commodo luctus, nisi erat port			titor ligula, eget lacinia odio sem nec elit.
								</p>
							</blockquote>

							<p>
								consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam.

							</p>

							<div class="blog-tags">
								TAGS <a href="#">photoshop</a> <a href="#">illustrator</a> <a href="#">adobe</a> <a href="#">theme</a>
								<div class="pull-right tag-social">
									<a href="#" class="btn btn-share pull-right">Share</a>

									<ul class="pull-right">
										<li>
											<a href="#">
												<i class="fa fa-facebook"></i>
											</a>
										</li>
										<li>
											<a href="#">
												<i class="fa fa-twitter"></i>
											</a>
										</li>
										<li>
											<a href="#">
												<i class="fa fa-google-plus"></i>
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div> -->
	</div>
</div>
<!--body wrapper end-->

<script>
	
	$(document).ready(function(){
		var light = 0;
		$('#t_code_generator').click(function(event) {

			light = light + 1 ;

			if (light == 1) {
				$.ajax({
					url: '<?php echo base_url('common/generate_teacher_code') ?>',
					debug: true,
					type: 'POST',
				})
				.done(function(response) {
					$('#t_code').html(response);
					$('#btn_copy').css('display', '');


				})
				.fail(function() {
					console.log("error: common/generate_teacher_code");
				})
			}
			return false;
		});

		$('#btn_copy').click(function(event) {
			// var myText = $('#t_code').text();
			// myText.select();
			// document.execCommand("copy");
			// console.log(myText.value);

			var $temp = $("<input>");
			$("body").append($temp);
			$temp.val($('#t_code').text()).select();
			document.execCommand("copy");
			$('#btn_copy').html("Copied");
			var copiedText = $temp.val();
			$temp.remove();

			// alert(copiedText);


		});

		$('body').on('click', '.btn_accept', function(event) {
			event.preventDefault();
			var t = this;
			$.ajax({
				url: '<?php echo base_url('requests/accept_request') ?>',
				type: 'POST',
				data: {
					request_id: $(this).data('req_id')
				},
			})
			.done(function(response) {
                if (response == "true") {
                	$(t).html("Accepted");
                	$(t).parent().find('.btn_reject').hide();
                }
            })
			.fail(function(jqXHR, textStatus, errorThrown) {
				console.log(errorThrown);
			})
		});
		$('body').on('click', '.btn_block', function(event) {
			event.preventDefault();
			var t = this;
			$.ajax({
				url: '<?php echo base_url('institute/block_teacher') ?>',
				type: 'POST',
				dataType: "json",
				data: {
					teacher_id: $(this).attr('data-teacher_id'),
					institute_code: $(this).attr('data-institute_code'),
					is_blocked: $(this).attr('data-is_blocked')
				},
			})
			.done(function(response) {
               $(t).addClass(response.btn_add_class);
               $(t).removeClass(response.btn_remove_class);
               $(t).attr("data-is_blocked", response.is_blocked);
               $(t).text(response.btn_text);
            })
			.fail(function(jqXHR, textStatus, errorThrown) {
				console.log(errorThrown);
			})
		});
	});

</script>