    <!--footer section start-->
    <footer>
        <center>© EduCatalyst 2018</center>
        
    </footer>
    <!--footer section end-->

</section>
<script src="<?php echo base_url(); ?>assets/js/modernizr.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js"></script>
<script src="<?php echo base_url(); ?>assets/js/additional-methods.js"></script>
<!--common scripts for all pages-->
<script src="<?php echo base_url(); ?>assets/js/scripts.js"></script>

</body>
</html>
