<?php if (empty($classes_joined)) :?>
	<div class="wrapper" style="height: 100vh; background-image: linear-gradient(white,#424F63);">

		<div class="text-center">
			<h3 style="color: #454d64;">No classes found that you've ever joined in. <span><a href="<?php echo base_url('institute/join'); ?>">Join institute</a></span> here</h3>
		</div>

	</div>
<?php else: ?>
	<div class="wrapper" style="height: 100vh;">

	<?php foreach ($classes_joined as $class_joined) :?>
		<div class="directory-info-row">
			<div class="row">
				<div class="col-md-2 col-sm-2">
					<div class="panel">
						<a href="<?php echo base_url('classes/stream/').$class_joined['class_id']; ?>" class="panel-body" data-class_id="<?php echo $class_joined['class_id']; ?>" style="text-decoration: none;">
							<h4><?php echo $class_joined['class_name']; ?> <span class="text-muted small"> <?php echo $class_joined['class_desc']; ?></span></h4>
							<div class="media">
								<a class="pull-left" href="#">
									<img class="thumb media-object" src="images/photos/user2.png" alt="">
								</a>
							</div>
						</a>
					</div>
				</div>


			</div>
		</div>
	<?php endforeach; ?>
		

	</div>
<?php endif; ?>