<!--body wrapper start-->
<div class="wrapper">
	<div class="row">
		<div class="col-md-12">
			<?php
			if($this->session->flashdata("response") != "" && $this->session->flashdata("resonse_msg") != "")
			{				
				?>
				<p class="alert alert-<?php echo $this->session->flashdata("response"); ?>">
					<?php echo $this->session->flashdata("resonse_msg"); ?>
				</p>
				<?php
			}
			?>			
			<section class="panel">		
				<header class="panel-heading">
					Cities
					<a href="<?php echo base_url('admin/city/add'); ?>" class="btn btn-primary btn-xs pull-right"> + Add new city</a>
					<div class="clearfix"></div>
				</header>
				<div class="panel-body">
					<table class="table">
						<thead>
							<tr>
								<th>Name</th>
								<th>State</th>								
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach ($cities as $key => $city) 
							{
								?>
								<tr>									
									<td><?php echo $city->name; ?></td>
									<td><?php echo $city->state_name; ?></td>									
									<td>
										<a href="<?php echo base_url("admin/city/edit/").$city->id; ?>" class="btn btn-info btn-xs btn-edit">Edit</a>
										<button class="btn btn-danger btn-xs btn-delete" data-href="<?php echo base_url('admin/city/delete/').$city->id ?>">Delete</button>
									</td>									
								</tr>
								<?php
							}
							?>						
						</tbody>
					</table>
				</div>
			</section>
		</div>
	</div>           
</div>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal-add-edit-state" class="modal fade"></div>
<!--body wrapper end-->
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/datatable/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/datatable/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets'); ?>/datatable/dataTables.bootstrap.min.css">
<script type="text/javascript">
	$(document).ready(function(){
		$(".table").dataTable();

		$("body").on("click", ".btn-delete", function(){
			if(confirm("Are you sure you want to delete?"))
			{
				window.location.href = $(this).data("href");
			}
			return false;
		});
	});
</script>