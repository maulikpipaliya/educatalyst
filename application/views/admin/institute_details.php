<!--body wrapper start-->
<?php
/*echo "<pre>";
print_r($institute_users);*/
?>
<div class="wrapper">
	<div class="row">
		<div class="col-md-4">
			<div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="profile-desk">
                                <h1><?php echo $institute->institute_name; ?></h1>
                                <span class="designation"><?php echo $institute->institute_type; ?></span>
                                <ul class="p-info">
                                    <li>
                                        <div class="title">Institute Code</div>
                                        <div class="desk"><?php echo $institute->institute_code; ?></div>
                                    </li>
                                    <li>
                                        <div class="title">Address</div>
                                        <div class="desk"><?php echo $institute->address." - ".$institute->postal_code; ?></div>
                                    </li>
                                    <li>
                                        <div class="title">Created By</div>
                                        <div class="desk"><?php echo $institute->firstname." ".$institute->lastname; ?></div>
                                    </li>
                                    <li>
                                        <div class="title">Created On</div>
                                        <div class="desk"><?php echo $institute->created_at; ?></div>
                                    </li>                                  
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="panel">
                        <header class="panel-heading">
                            Classes                                
                        </header>
                        <div class="panel-body">
                            <?php echo $institute_classes; ?>
                        </div>
                    </div>
                </div>
            </div>
		</div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12">
                    <?php 
                    foreach ($institute_users as $key => $institute_user) 
                    {
                        ?>
                        <div class="panel">
                            <header class="panel-heading">
                                <?php echo $key; ?>
                                <span class="tools pull-right">
                                    <a class="fa fa-chevron-down" href="javascript:;"></a>
                                 </span>
                            </header>
                            <div class="panel-body">
                                <ul class="p-info">
                                    <?php 
                                    foreach($institute_user as $user)
                                    {
                                        ?>
                                        <li>
                                            <h5><a href="<?php echo base_url('admin/user/details/').$user->user_id; ?>"><?php echo $user->firstname." ".$user->lastname; ?></a></h5>
                                            <p class="text-muted"><?php echo $user->username." (".$user->email.")"; ?></p>
                                        </li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
	</div>           
</div>
