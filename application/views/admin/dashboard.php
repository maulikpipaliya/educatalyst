<!--body wrapper start-->
        <div class="wrapper">
            <div class="row">
                <div class="col-md-6">
                    <!--statistics start-->
                    <div class="row state-overview">
                        <div class="col-md-6 col-xs-12 col-sm-6">
                            <div class="panel purple">
                                <div class="symbol">
                                    <i class="fa fa-gavel"></i>
                                </div>
                                <div class="state-value">
                                    <div class="value"><?php echo $total_institutes; ?></div>
                                    <div class="title">Total Institutes</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12 col-sm-6">
                            <div class="panel red">
                                <div class="symbol">
                                    <i class="fa fa-tags"></i>
                                </div>
                                <div class="state-value">
                                    <div class="value"><?php echo $total_classes; ?></div>
                                    <div class="title">Total Classes</div>
                                </div>
                            </div>
                        </div>
                    </div>                    
                    <!--statistics end-->
                </div>      
            </div>          
        </div>
        <!--body wrapper end-->