<!--body wrapper start-->
<div class="wrapper">
  <div class="row">
    <div class="col-lg-6">
      <section class="panel">
        <header class="panel-heading">
          <?php echo @$institute_type->id?"Edit":"Add"; ?> Institute type
        </header>
        <div class="panel-body">
         <form role="form" id="form" action="" method="post">            
            <div class="form-group">
              <label for="name">Institute type</label>
              <input type="text" class="form-control required" id="institute_type" value="<?php echo @$institute_type->institute_type; ?>" name="institute_type" placeholder="Institute type">
            </div>                                
            <button type="submit" class="btn btn-primary">Submit</button>
            <a href="<?php echo base_url("admin/institute_type"); ?>" class="btn btn-default">Cancel</a>
          </form>
        </div>
      </section>
    </div>
  </div>           
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $("#form").validate();
  }); 
</script>