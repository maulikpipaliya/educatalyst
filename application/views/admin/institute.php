<!--body wrapper start-->
<div class="wrapper">
	<div class="row">
		<div class="col-md-12">
			<section class="panel">		
				<header class="panel-heading">
					Institutes              
				</header>
				<div class="panel-body">
					<table class="table">
						<thead>
							<tr>
								<th>Name</th>
								<th>Code</th>
								<th>Address</th>
								<th>Type</th>
								<th>Created By</th>
								<th>Created on</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach ($institutes as $key => $institute) 
							{
								?>
								<tr>
									<td><a href="<?php echo base_url('admin/institute/details/').$institute->institute_id; ?>"><?php echo $institute->institute_name; ?></a></td>
									<td><?php echo $institute->institute_code; ?></td>
									<td><?php echo $institute->address; ?></td>
									<td><?php echo $institute->institute_type; ?></td>
									<td><?php echo $institute->firstname." ".$institute->lastname; ?></td>
									<td><?php echo $institute->created_at; ?></td>
									<td><button class="btn <?php echo $institute->blocked==1?'btn-success':'btn-danger' ?> btn-xs btn-block-action" data-action="<?php echo $institute->blocked==1?0:1 ?>" data-id="<?php echo $institute->institute_id; ?>"><?php echo $institute->blocked==1?'Unblock':'Block' ?></button></td>
									<!-- You need to add condition user side to check institute blocked or not  -->
								</tr>
								<?php
							}
							?>						
						</tbody>
					</table>
				</div>
			</section>
		</div>
	</div>           
</div>
<!--body wrapper end-->
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/datatable/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/datatable/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets'); ?>/datatable/dataTables.bootstrap.min.css">
<script type="text/javascript">
	$(document).ready(function(){
		$(".table").dataTable();
		$("body").on("click", ".btn-block-action", function(){
			var th = this;
			var btn_text = $(th).text();
			$(th).text("Wait..");
			$.ajax({
				url: "<?php echo base_url('admin/institute/block'); ?>",
				data: {id: $(th).data('id'), action: $(th).attr('data-action')},
				type: "post",
				dataType: 'json'
			})
			.done(function(response) {
				$(th).addClass(response.add_class);
				$(th).removeClass(response.remove_class);
				$(th).text(response.btn_text);
				$(th).attr("data-action", response.action);
			})
			.fail(function(jqXHR, textStatus, errorThrown) {
				$(th).text(btn_text);
				console.log(errorThrown);
			})
			.always(function() {
				console.log("complete");
			});			
		});
	});
</script>