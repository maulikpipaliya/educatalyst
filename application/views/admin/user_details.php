<!--body wrapper start-->
<?php
/*echo "<pre>";
print_r($user_joinings);*/
?>
<div class="wrapper">
	<div class="row">
		<div class="col-md-4">
			<div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="profile-desk">
                                <h1><?php echo $user->firstname." ".$user->lastname; ?></h1>
                                <p></p>
                                <ul class="p-info">
                                    <li>
                                        <div class="title">Email</div>
                                        <div class="desk"><?php echo $user->email; ?></div>
                                    </li>
                                    <li>
                                        <div class="title">Username</div>
                                        <div class="desk"><?php echo $user->username; ?></div>
                                    </li>
                                    <li>
                                        <div class="title">Mobile</div>
                                        <div class="desk"><?php echo $user->mobile; ?></div>
                                    </li>
                                    <li>
                                        <div class="title">Gender</div>
                                        <div class="desk"><?php echo ($user->gender == 1)?"Male":"Female"; ?></div>
                                    </li>
                                    <li>
                                        <div class="title">Birthdate</div>
                                        <div class="desk"><?php echo $user->birthdate; ?></div>
                                    </li>                                    
                                    <li>
                                        <div class="title">Created On</div>
                                        <div class="desk"><?php echo $user->created_at; ?></div>
                                    </li>                                  
                                </ul>
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
		</div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12">
                    <?php 
                    foreach ($user_joinings as $key => $user_joining) 
                    {
                        ?>
                        <div class="panel">
                            <header class="panel-heading">
                                <?php echo $key; ?>
                                <span class="tools pull-right">
                                    <a class="fa fa-chevron-down" href="javascript:;"></a>
                                 </span>
                            </header>
                            <div class="panel-body">
                                <ul class="p-info">
                                    <?php 
                                    foreach($user_joining as $institute)
                                    {
                                        ?>
                                        <li>
                                            <h5><a href="<?php echo base_url('admin/institute/details/').$institute->institute_id; ?>"><?php echo $institute->institute_name; ?></a></h5>
                                            <p class="text-muted"><?php echo $institute->institute_type; ?></p>
                                        </li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
	</div>           
</div>
