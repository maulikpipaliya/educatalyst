<!--body wrapper start-->
<div class="wrapper">
  <div class="row">
    <div class="col-lg-6">
      <section class="panel">
        <header class="panel-heading">
          <?php echo @$state->id?"Edit":"Add"; ?> State
        </header>
        <div class="panel-body">
         <form role="form" id="form" action="" method="post">
            <div class="form-group">
              <label for="country_id">Country</label>
              <select class="form-control" id="country_id" name="country_id" readonly="readonly">
                <option value="101">India</option>
              </select>
            </div>
            <div class="form-group">
              <label for="name">State</label>
              <input type="text" class="form-control required" id="name" value="<?php echo @$state->name; ?>" name="name" placeholder="State name">
            </div>                        
            <div class="form-group">
              <label for="code">Code</label>
              <input type="text" class="form-control required" id="code" value="<?php echo @$state->code; ?>" name="code" placeholder="State code">
            </div>                        
            <button type="submit" class="btn btn-primary">Submit</button>
            <a href="<?php echo base_url("admin/state"); ?>" class="btn btn-default">Cancel</a>
          </form>
        </div>
      </section>
    </div>
  </div>           
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $("#form").validate();
  }); 
</script>
