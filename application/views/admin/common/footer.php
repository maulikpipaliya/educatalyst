<!--footer section start-->
        <footer>
            2014 &copy; AdminEx by ThemeBucket
        </footer>
        <!--footer section end-->


    </div>
    <!-- main content end-->
</section>

<!-- Placed js at the end of the document so the pages load faster -->
<script src="<?php echo base_url('assets/'); ?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/modernizr.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/jquery.nicescroll.js"></script>

<!--easy pie chart-->
<script src="<?php echo base_url('assets/'); ?>js/easypiechart/jquery.easypiechart.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/easypiechart/easypiechart-init.js"></script>

<!--Sparkline Chart-->
<script src="<?php echo base_url('assets/'); ?>js/sparkline/jquery.sparkline.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/sparkline/sparkline-init.js"></script>

<!--icheck -->
<script src="<?php echo base_url('assets/'); ?>js/iCheck/jquery.icheck.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/icheck-init.js"></script>

<!-- jQuery Flot Chart-->
<!-- <script src="<?php echo base_url('assets/'); ?>js/flot-chart/jquery.flot.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/flot-chart/jquery.flot.tooltip.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/flot-chart/jquery.flot.resize.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/flot-chart/jquery.flot.pie.resize.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/flot-chart/jquery.flot.selection.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/flot-chart/jquery.flot.stack.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/flot-chart/jquery.flot.time.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/main-chart.js"></script> -->

<!--common scripts for all pages-->
<script src="<?php echo base_url('assets/'); ?>js/scripts.js"></script>


</body>
</html>
