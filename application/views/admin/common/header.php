<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="keywords" content="admin, dashboard, bootstrap, template, flat, modern, theme, responsive, fluid, retina, backend, html5, css, css3">
  <meta name="description" content="">
  <meta name="author" content="ThemeBucket">
  <link rel="shortcut icon" href="#" type="image/png">

  <title>AdminX</title>

  <!--icheck-->
  <link href="<?php echo base_url('assets'); ?>/js/iCheck/skins/minimal/minimal.css" rel="stylesheet">
  <link href="<?php echo base_url('assets'); ?>/js/iCheck/skins/square/square.css" rel="stylesheet">
  <link href="<?php echo base_url('assets'); ?>/js/iCheck/skins/square/red.css" rel="stylesheet">
  <link href="<?php echo base_url('assets'); ?>/js/iCheck/skins/square/blue.css" rel="stylesheet">

  <!--dashboard calendar-->
  <link href="<?php echo base_url('assets'); ?>/css/clndr.css" rel="stylesheet">


  <!--common-->
  <link href="<?php echo base_url('assets'); ?>/css/style.css" rel="stylesheet">
  <link href="<?php echo base_url('assets'); ?>/css/style-responsive.css" rel="stylesheet">

  <script src="<?php echo base_url('assets/'); ?>js/jquery-1.10.2.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo base_url('assets/'); ?>js/jquery.validate.min.js"></script>

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
</head>

<body class="sticky-header">

<section>
    <!-- left side start-->
    <div class="left-side sticky-left-side">

        <!--logo and iconic logo start-->
        <div class="logo">
            <a href="index.html"><img src="<?php echo base_url('assets'); ?>/images/main_logo.png" alt="" width="80%"></a>
        </div>

        <div class="logo-icon text-center">
            <a href="index.html"><img src="images/logo_icon.png" alt=""></a>
        </div>
        <!--logo and iconic logo end-->

        <div class="left-side-inner">

            <!-- visible to small devices only -->
            <div class="visible-xs hidden-sm hidden-md hidden-lg">
                <div class="media logged-user">
                    <img alt="" src="images/photos/user-avatar.png" class="media-object">
                    <div class="media-body">
                        <h4><a href="#">John Doe</a></h4>
                        <span>"Hello There..."</span>
                    </div>
                </div>

                <h5 class="left-nav-title">Account Information</h5>
                <ul class="nav nav-pills nav-stacked custom-nav">
                  <li><a href="#"><i class="fa fa-user"></i> <span>Profile</span></a></li>
                  <li><a href="#"><i class="fa fa-cog"></i> <span>Settings</span></a></li>
                  <li><a href="#"><i class="fa fa-sign-out"></i> <span>Sign Out</span></a></li>
                </ul>
            </div>

            <!--sidebar nav start-->
            <ul class="nav nav-pills nav-stacked custom-nav">
                <li><a href="<?php echo base_url('admin/dashboard') ?>"><i class="fa fa-bullhorn"></i> <span>Dashboard</span></a></li>

                <li class="menu-list <?php echo (@$menu == "institute")?'active':''; ?>"><a href=""><i class="fa fa-building-o"></i> <span>Institutes</span></a>
                    <ul class="sub-menu-list">
                        <li class="<?php echo (@$submenu_level1 == "all_institutes")?'active':''; ?>"><a href="<?php echo base_url('admin/institute'); ?>"> All Institutes</a></li>
                        <li class="<?php echo (@$submenu_level1 == "institute_type")?'active':''; ?>"><a href="<?php echo base_url('admin/institute_type'); ?>"> Institute types</a></li>
                    </ul>
                </li>                
                <li class="<?php echo (@$menu == "user")?'active':''; ?>"><a href="<?php echo base_url('admin/user'); ?>"><i class="fa fa-users"></i> <span>Users</span></a></li>

                <li class="menu-list <?php echo (@$menu == "state_city")?'active':''; ?>"><a href=""><i class="fa fa-building-o"></i> <span>Locations</span></a>
                    <ul class="sub-menu-list">
                        <li class="<?php echo (@$submenu_level1 == "state")?'active':''; ?>"><a href="<?php echo base_url('admin/state'); ?>"> State</a></li>
                        <li class="<?php echo (@$submenu_level1 == "city")?'active':''; ?>"><a href="<?php echo base_url('admin/city'); ?>"> City</a></li>
                    </ul>
                </li>   
            </ul>
            <!--sidebar nav end-->

        </div>
    </div>
    <!-- left side end-->
    
    <!-- main content start-->
    <div class="main-content" >

        <!-- header section start-->
        <div class="header-section">

            <!--toggle button start-->
            <a class="toggle-btn"><i class="fa fa-bars"></i></a>
            <!--toggle button end-->

            <!--search start-->
            <form class="searchform" action="index.html" method="post">
                <input type="text" class="form-control" name="keyword" placeholder="Search here..." />
            </form>
            <!--search end-->

            <!--notification menu start -->
            <div class="menu-right">
                <ul class="notification-menu">                    
                    <li>
                        <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <img src="images/photos/user-avatar.png" alt="" />
                            <?php $admin_data = $this->session->userdata("admin_details");
                            echo $admin_data['firstname']." ".$admin_data['lastname']; ?>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-usermenu pull-right">                            
                            <li><a href="<?php echo base_url('admin/signout'); ?>"><i class="fa fa-sign-out"></i> Log Out</a></li>
                        </ul>
                    </li>

                </ul>
            </div>
            <!--notification menu end -->

        </div>
        <!-- header section end-->