<!--body wrapper start-->
<div class="wrapper">
  <div class="row">
    <div class="col-lg-6">
      <section class="panel">
        <header class="panel-heading">
          <?php echo @$city->id?"Edit":"Add"; ?> City
        </header>
        <div class="panel-body">
         <form role="form" id="form" action="" method="post">
            <div class="form-group">
              <label for="state_id">State</label>
              <select class="form-control required" id="state_id" name="state_id">
                <option value="">Select State</option>
                <?php 
                foreach($states as $state)
                {
                  ?>
                  <option value="<?php echo $state->id; ?>" <?php echo ($state->id == @$city->state_id)?"selected":""; ?>><?php echo $state->name; ?></option>
                  <?php
                }
                ?>                
              </select>
            </div>
            <div class="form-group">
              <label for="name">City</label>
              <input type="text" class="form-control required" id="name" value="<?php echo @$city->name; ?>" name="name" placeholder="City name">
            </div>                                 
            <button type="submit" class="btn btn-primary">Submit</button>            
            <a href="<?php echo base_url("admin/city"); ?>" class="btn btn-default">Cancel</a>
          </form>
        </div>
      </section>
    </div>
  </div>           
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $("#form").validate();
  }); 
</script>
