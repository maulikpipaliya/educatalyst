<!--body wrapper start-->
<div class="wrapper">
	<div class="row">
		<div class="col-md-12">
			<section class="panel">		
				<header class="panel-heading">
					Users              
				</header>
				<div class="panel-body">
					<table class="table">
						<thead>
							<tr>
								<th>Name</th>
								<th>Username</th>
								<th>Email</th>
								<th>Mobile</th>
								<th>Gender</th>
								<th>Birthdate</th>								
								<th>Created on</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach ($users as $key => $user) 
							{
								?>
								<tr>
									<td><a href="<?php echo base_url('admin/user/details/').$user->user_id; ?>"><?php echo $user->firstname." ".$user->lastname; ?></a></td>
									<td><?php echo $user->username; ?></td>
									<td><?php echo $user->email; ?></td>
									<td><?php echo $user->mobile; ?></td>
									<td><?php echo ($user->gender == 1)?"Male":"Female"; ?></td>
									<td><?php echo $user->birthdate; ?></td>
									<td><?php echo $user->created_at; ?></td>
									<td><button class="btn <?php echo $user->is_blocked==1?'btn-success':'btn-danger' ?> btn-xs btn-block-action" data-action="<?php echo $user->is_blocked==1?0:1 ?>" data-id="<?php echo $user->user_id; ?>"><?php echo $user->is_blocked==1?'Unblock':'Block' ?></button></td>
									<!-- You need to add condition user side to check institute blocked or not  -->
								</tr>
								<?php
							}
							?>						
						</tbody>
					</table>
				</div>
			</section>
		</div>
	</div>           
</div>
<!--body wrapper end-->
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/datatable/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/datatable/dataTables.bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets'); ?>/datatable/dataTables.bootstrap.min.css">
<script type="text/javascript">
	$(document).ready(function(){
		$(".table").dataTable();
		$("body").on("click", ".btn-block-action", function(){
			var th = this;
			var btn_text = $(th).text();
			$(th).text("Wait..");
			$.ajax({
				url: "<?php echo base_url('admin/user/block'); ?>",
				data: {id: $(th).data('id'), action: $(th).attr('data-action')},
				type: "post",
				dataType: 'json'
			})
			.done(function(response) {
				$(th).addClass(response.add_class);
				$(th).removeClass(response.remove_class);
				$(th).text(response.btn_text);
				$(th).attr("data-action", response.action);
			})
			.fail(function(jqXHR, textStatus, errorThrown) {
				$(th).text(btn_text);
				console.log(errorThrown);
			})
			.always(function() {
				console.log("complete");
			});			
		});
	});
</script>