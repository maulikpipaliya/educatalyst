 <?php 
 $ssn_inst_join  = $this->session->userdata('ssn_inst_join');
 ?>
 <!--body wrapper start-->
 <div class="wrapper">

    <div id="div_requests">
        <div class="directory-info-row">
            <?php if (!empty($requests)) :?>

                <?php foreach ($requests as $request) :?>
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                        </div>   

                        <div class="col-md-6 col-sm-6">
                            <div class="panel">

                                <div class="panel-body">

                                    <h4 class="sample" data-user_id="<?php echo $request['user_id'] ?>">
                                        <?php echo $request['FullName']; ?>

                                        <span class="text-muted small"> - Graphics Designer</span>

                                        <button id="btn_reject" class="btn btn-danger pull-right btn_reject" data-req_id="<?php echo $request['request_id']; ?>">Reject</button>

                                        <button id="btn_accept" class="btn btn-success pull-right btn_accept" data-req_id="<?php echo $request['request_id']; ?>" style="margin-right: 10px;">Accept</button>
                                    </h4>


                                    <div class="media">
                                        <a class="pull-left" href="#">
                                            <img class="thumb media-object" src="images/photos/user2.png" alt="">
                                        </a>
                                        <div class="media-body">
                                            <address>
                                                <strong>ABCDE, Inc.</strong>

                                            </address>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-3 col-sm-3">
                        </div>   
                    </div>
                <?php endforeach; ?>
            <?php else: ?>
                <span>No requests</span>
            <?php endif; ?>
        </div>
    </div>
</div>
<!--body wrapper end-->


<script>
    $(document).ready(function() {


        $('.btn_accept').click(function(event) {
            var t = this;
            $.ajax({
                url: '<?php echo base_url('requests/accept_request') ?>',
                type: 'POST',
                data: {
                    request_id: $(this).data('req_id')
                },
            })
            .done(function(response) {
                // console.log("success");
                // console.log(response);
                if (response == "true") {
                    $(t).html("Accepted");
                    $(t).parent().find('.btn_reject').hide();
                }

            })
            .fail(function() {
                console.log("error: accept_request");
            })
        });
        
    });

</script>