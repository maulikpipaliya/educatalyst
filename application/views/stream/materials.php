<!--dropzone css-->
<link href="<?php echo base_url('assets'); ?>/js/dropzone/css/dropzone.css" rel="stylesheet"/>
<div class="wrapper">

	<form action="<?php echo base_url('classes/add_material'); ?>" class="dropzone" id="my-awesome-dropzone">
		<div class="default message" style="background-image: url();"><span>Drop files here to upload</span></div>


	</form>
	
</div>

<!--dropzone-->
<script src="<?php echo base_url('assets'); ?>/js/dropzone/dropzone.js"></script>

<script>
	function load_files() {
		$.ajax({
			url: '<?php echo base_url('classes/load_files'); ?>',
			type: 'POST',
			data: {class_id: "<?php echo $class_id ?>"},
		})
		.done(function(response) {
			$('#my-awesome-dropzone').append(response);
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		
	}

	$(document).ready(function() {
		load_files();

		$('#my-awesome-dropzone').click(function(event) {
			load_files();
		});
		$('body').on('click', '#dl_link', function(event) {
			$.ajax({
				url: '<?php echo base_url('classes/do_download') ?>',
				type: 'POST)',
				data: {file_id: function(){return $('#dl_link').data(id);}},
			})
			.done(function() {
				console.log("success");

			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
			event.preventDefault();
			
		});
	});
</script>
