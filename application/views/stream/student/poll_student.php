
<!-- icheck -->
<link href="<?php echo base_url('assets'); ?>/js/iCheck/skins/minimal/minimal.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/js/iCheck/skins/minimal/red.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/js/iCheck/skins/minimal/green.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/js/iCheck/skins/minimal/blue.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/js/iCheck/skins/minimal/yellow.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/js/iCheck/skins/minimal/purple.css" rel="stylesheet">

<link href="<?php echo base_url('assets'); ?>/js/iCheck/skins/square/square.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/js/iCheck/skins/square/red.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/js/iCheck/skins/square/green.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/js/iCheck/skins/square/blue.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/js/iCheck/skins/square/yellow.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/js/iCheck/skins/square/purple.css" rel="stylesheet">

<link href="<?php echo base_url('assets'); ?>/js/iCheck/skins/flat/grey.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/js/iCheck/skins/flat/red.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/js/iCheck/skins/flat/green.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/js/iCheck/skins/flat/blue.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/js/iCheck/skins/flat/yellow.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/js/iCheck/skins/flat/purple.css" rel="stylesheet">


<div class="panel-body">
	
	<div class="activity-desk" style="margin-left: 0px;">
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<div class="pull-right">

				</div>

				<h5 style="margin-top: 0px;">
					<a style="color: #454d64; text-decoration: none;"><?php echo $polls['0']['text']; ?></a> 
					<span></span>
				</h5>

				<form class="form-horizontal bucket-form" method="post" id="form_poll">
					<div class="form-group">
						<?php foreach ($polls as $poll) :?>
							<?php $poll_id = $poll['poll_id']; ?>

							<div class="row">
								<div class="col-md-3" style="margin-top: 0px;margin-bottom: 0px;">
									<div class="icheck ">
										<div class="square single-row">
											<div class="radio ">
												<input tabindex="3" type="radio" value="<?php echo $poll['option_text']; ?>" name="radio_poll">
												<?php echo $poll['option_text']; ?>
											</div>
										</div>
									</div>		
								</div>
								<div class="col-md-5" style="margin-top: 20px;margin-bottom: 15px;">

								</div>

							</div>

						<?php endforeach;?>




					</div>
				</form>

				<p class="text-muted" style="font-size: 10px;"></p>
				<p></p>
			</div>
		</div>
		<div class="pull-left">

		</div>
	</div>

</div>

<div class="panel-footer">
	<button class="btn btn-post pull-right" id="btn_answer_poll" data-poll_id="<?php echo $poll_id; ?>" type="submit" name="btn_answer_poll">Answer Poll</button>
	<div class="clearfix"></div>
</div>


<!--icheck -->
<script src="<?php echo base_url('assets'); ?>/js/iCheck/jquery.icheck.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/icheck-init.js"></script>