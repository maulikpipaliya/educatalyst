
<?php foreach ($notices as $notice) :?>
<div class="row">
	<div class="col-md-8">
		<li data-notice_id= "<?php echo $notice['notice_id'] ?>">
			<?php echo $notice['notice']; ?>
		</li>
			
	</div>
	<div class="col-md-4">
		<div class="pull-right">
				
					
				<?php
				if (!empty($notice['created_at'])) {
				echo date("jS F Y", strtotime($notice['created_at'])); 	
				}
				 
				 ?>
			</div>
	</div>
</div>
<?php endforeach; ?>