
<?php $current_user_details = $this->session->userdata('user_details');  ?>
<!--pickers css-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets'); ?>/js/bootstrap-datepicker/css/datepicker-custom.css" />
<!--data table-->
<link rel="stylesheet" href="<?php echo base_url('assets'); ?>/js/data-tables/DT_bootstrap.css" />

<!--icheck-->


<!-- <link rel="stylesheet" type="text/css" href="https://uxsolutions.github.io/bootstrap-datepicker/bootstrap-datepicker/css/bootstrap-datepicker.min.css" /> -->

<!--body wrapper start-->
<div class="wrapper">
	
	<div class="row">
		<div class="col-md-4">
			<div class="row">
				<div class="col-md-12">
					<div class="panel">
						<div class="panel-body">
							<div class="row">
								<div class="col-md-9">
									<div class="text-center">
										<h2 id="class_code"><?php echo $this_class['class_code']; ?></h2>
									</div>		
								</div>
								<div class="col-md-3">
									<button id="btn_copy" style="margin-top: 20px;" class="btn btn-primary">Copy</button>
								</div>
							</div>
							
							
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="panel" id="calendar_panel">
						<div class="panel-heading">
							<div class="row">
								<div class="col-md-3">NOTICES</div>
								
							</div>
							
						</div>
						<div class="panel-body" id="panel_notices">
							
							<div class="col-md-12 col-xs-12">
								
							</div>



						</div>
					</div>
				</div>
				<div class="col-md-12">

				</div>
				<div class="col-md-12">

				</div>
				<div class="col-md-12">

				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="row">
				<div class="col-md-12">
					<div class="panel">
						<div class="panel-body">
							<div class="profile-desk">
								<form action="<?php echo base_url('classes/session'); ?>" method="POST">
									<h1><?php echo $this_class['class_name']; ?></h1>
									<span class="designation"><?php echo $this_class['class_desc'] ?></span>
									<p>
										Topic of this class will be here
									</p>
									
								</form>

							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel" id="panel_post">
						<form action="<?php echo base_url('classes/post_update'); ?>" method="POST" accept-charset="utf-8">
							<div id="textarea">
								<textarea class="form-control input-lg p-text-area" rows="2" id="post_text" placeholder="Have doubts? Write up here..." style="color: black"></textarea>
							</div>
							<div>
								<div class="row">
									<div class="col-md-4" style="margin-left: 25px;" id="poll_questions">

									</div>

								</div>



							</div>

							<footer class="panel-footer">
								<button class="btn btn-post pull-right" id="btn_post" type="submit" name="btn_post">Post</button>
								<button class="btn btn-post pull-right" id="btn_poll" type="button" name="btn_poll">Poll</button>
								<ul class="nav nav-pills p-option">
									<li>
										<a id="btn_post_post"><i class="fa fa-pencil"></i></a>

									</li>
									<li>
										<a id="btn_post_poll"><i class="fa fa-question"></i></a>
									</li>

								</ul>
							</footer>
						</form>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="panel" id="panel_poll">

						<div class="panel-footer">
							haah
						</div>
					</div>
					<div class="panel" id="panel_posts">

						<!-- Ajax Content -->

					</div>
				</div>
			</div>

		</div>
	</div>

</div>
<!--body wrapper end-->

<!-- MODALS -->

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_edit" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
				<h4 class="modal-title">Edit Post</h4>
			</div>
			<div class="modal-body">

				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label for="post_content" class="col-lg-2 col-sm-2 control-label">Post</label>
						<div class="col-lg-10">
							<input type="text1" class="form-control" id="edit_post_content">
						</div>
					</div>
					
					
					<div class="form-group">
						<div class="col-lg-offset-2 col-lg-10">
							<button type="submit" class="btn btn-primary" id="btn_update_post">Update Post</button>
						</div>
					</div>
				</form>

			</div>

		</div>
	</div>
</div>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_delete" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
				<h4 class="modal-title">Delete Post</h4>
			</div>
			<div class="modal-body">

				This will be removed from your timeline. You can edit this post if you want to change something.

			</div>
			<div class="modal-footer">
				<a href="#modal_edit" id="modal_delete_btn_edit" type="button" class="btn btn-default">Edit</a>
				<button type="button" id="btn_delete_post" class="btn btn-danger"> Delete</button>
			</div>
		</div>
	</div>
</div>



<!-- MODALS -->

<!--pickers plugins-->
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

<script type="text/javascript" src="<?php echo base_url('assets'); ?>/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/js/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/js/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<!--data table-->
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/js/data-tables/DT_bootstrap.js"></script>

<!--script for editable table-->
<script src="<?php echo base_url('assets'); ?>/js/editable-table.js"></script>

<!-- END JAVASCRIPTS -->
<script>
	jQuery(document).ready(function() {
		EditableTable.init();
	});
</script>


<!-- <script type="text/javascript" src="https://uxsolutions.github.io/bootstrap-datepicker/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script> -->
<script>
	var $selected_date;
	var $post_id;

	function load_notices(class_id){
		$.ajax({
			url: '<?php echo base_url('stream/load_notices_student/') ?>' + class_id,
			type: 'POST'
		})
		.done(function(response) {
			$('#panel_notices').html(response);
			console.log("load_notices:ajax called");
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});	
	}
	function load_posts(class_id) {
		$.ajax({
			url: '<?php echo base_url('stream/load_posts/'); ?>' + class_id
		})
		.done(function(response) {
			$('#panel_posts').html(response);
			console.log("success");

		})
		.fail(function() {
			console.log("error454");
		})
		.always(function() {
			console.log("complete");
		});
	}
	function load_poll(class_id) {
		$.ajax({
			url: '<?php echo base_url('stream/load_poll_student/'); ?>' + class_id
		})
		.done(function(response) {
			$('#panel_poll').html(response);
		})
		.fail(function( jqXHR, textStatus, errorThrown) {
			console.log(errorThrown);
		})
		.always(function() {
			console.log("complete");
		});
	}


	$(document).ready(function() {


		load_notices("<?php echo $this_class['class_id'] ?>");
		load_poll("<?php echo $this_class['class_id'] ?>");
		
		setInterval(function(){load_posts("<?php echo $this_class['class_id'] ?>");}, 1000);
		load_posts("<?php echo $this_class['class_id'] ?>");

		$datepicker = $('.default-date-picker');
		$datepicker.datepicker({
			format: 'dd-mm-yyyy',
			autoclose: true
		});
		$('#datepicker').val();

		$('#btn_copy').click(function(event) {
			// var myText = $('#t_code').text();
			// myText.select();
			// document.execCommand("copy");
			// console.log(myText.value);

			var $temp = $("<input>");
			$("body").append($temp);
			$temp.val($('#class_code').text()).select();
			document.execCommand("copy");
			$('#btn_copy').html("Copied");
			var copiedText = $temp.val();
			$temp.remove();

			// alert(copiedText);


		});

		$('body').on('click', '.btns-delete-notice', function(event) {
			event.preventDefault();
			if (confirm("Notice will be deleted, are you sure?")) {
				$notice_id = $(this).data('notice_id');

				$.ajax({
					url: '<?php echo base_url('stream/delete_notice/') ?>' + $notice_id,
					type: 'POST',
					dataType: 'json',
					data: {notice_id: $notice_id}
				})
				.done(function(response) {
					if (response.deleted === true) {
						// $('#modal_notice').modal('toggle');
						load_notices("<?php echo $this_class['class_id']; ?>");
					}
					else{
						alert('some error occured');
					}
				})
				.fail(function() {
					console.log("error");
				})
				.always(function() {
					console.log("complete");
				});
			}
			else{
				return false;
			}
			
			
		});
		$('body').on('click', '#btn_add_notice', function(event) {
			event.preventDefault();
			$.ajax({
				url: '<?php echo base_url('stream/add_notice') ?>',
				type: 'POST',
				data: {
					notice: function(){return $('#notice').val();},
					class_id: "<?php echo $this_class['class_id'] ?>"
				},
			})
			.done(function(response) {
				console.log(response);
				if (response.notice_added === true) {
					$('#modal_notice').modal('toggle');
					load_notices("<?php echo $this_class['class_id']; ?>");
				}

			})
			.fail(function(jqXHR, textStatus, errorThrown) {
				console.log(errorThrown);
			})
			.always(function() {
				console.log("complete");
			});
			
		});

		$('body').on('click', '#btn_post', function(event) {
			event.preventDefault();
			$.ajax({
				url: '<?php echo base_url('stream/post_update'); ?>',
				type: 'POST',
				data: {post_text: function(){return $('#post_text').val();}}
			})
			.done(function(response) {
				console.log('hgahalol');
				$('#post_text').val("");
				load_posts("<?php echo $this_class['class_id'] ?>");
				setInterval(load_posts("<?php echo $this_class['class_id'] ?>"), 3000);
			})
			.fail(function() {
				console.log("error01");
			})
			.always(function() {
				console.log("complete");
			});
		});

		$('body').on('click', '#icon_edit,#icon_delete', function(event) {
			$('#edit_post_content').val($(this).data('content'));
			$post_id = $(this).data('post_id');
		});
		

		$('body').on('click', '#btn_update_post', function(event) {
			$.ajax({
				url: '<?php echo base_url('stream/edit_post') ?>',
				type: 'POST',
				data: {
					post_id: $post_id,
					post_content: function(){return $('#edit_post_content').val();}
				},
			})
			.done(function(response) {
				console.log(response);
				if (response == "true") {
					$('#modal_edit').modal('toggle');
					load_posts("<?php echo $this_class['class_id'] ?>");
				}

			})
			.fail(function() {
				console.log("error");
			})
			
		});

		$('body').on('click', '#modal_delete_btn_edit', function(event) {
			event.preventDefault();
			console.log($(this).data('content'));
			$('#modal_delete').modal('toggle');
			$('#modal_edit').modal('toggle');
		});

		$('body').on('click', '#btn_delete_post', function(event) {
			event.preventDefault();
			$.ajax({
				url: '<?php echo base_url('stream/delete_post') ?>',
				type: 'POST',
				data: {post_id: $post_id}
			})
			.done(function(response) {
				console.log(response);
				if (response == "true") {
					$('#modal_delete').modal('toggle');
					load_posts("<?php echo $this_class['class_id'] ?>");
				}
				console.log("success");
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
			
		});

		$('body').on('click', '.btn_upvote', function(event) {
			event.preventDefault();
			$this = $(this);
			$post_id = $(this).data('post_id');
			$user_id = "<?php echo $current_user_details['user_id']; ?>";

			$.ajax({
				url: '<?php echo base_url('stream/do_upvote'); ?>',
				type: 'POST',
				dataType: 'json',
				data: {
					post_id: $post_id,
					user_id: $user_id,
				},
			})
			.done(function(response) {
				console.log(response);
				
				if (response.just_upvoted === true) {
					$this.css('background-color', '#50cea1');
				}
				if (response.undone_upvote === true) {
					$this.css('background-color', 'transparent');
				}

				load_posts("<?php echo $this_class['class_id'] ?>");





			})
			.fail(function( jqXHR, textStatus, errorThrown) {
				console.log(errorThrown);
			})
			.always(function() {
				console.log("complete");
			});
			
			$this.css('background-color', '#50cea1');
			
		});
		$('.btn_upvote').click(function(event) {
			
		});

		$('#btn_post').show();
		$('#btn_poll').hide();
		$('body').on('click', '#btn_post_poll', function(event) {
			event.preventDefault();
			$('#post_text').attr({
				placeholder: 'Ask question'
			});

			$(this).css({
				'background-color': '#65CEA7',
				'color': '#424d64'
			});
			$('#btn_post_post').css({
				'background-color': 'initial'
			});


			$('#btn_post').hide();
			$('#poll_questions').show();
			$('#btn_poll').show();



			$('#poll_questions').html(
				'<div class="row" style="margin-top: 10px;margin-bottom: 10px;">'+
				'<div class="col-md-10">'+	
				'<input type="text" name="" value="" placeholder="Option 1" class="form-control input-sm input_options">'+
				'</div>'+
				'</div>'+
				'<div class="row" style="margin-top: 10px;margin-bottom: 10px;">'+
				'<div class="col-md-10">'+	
				'<input type="text" name="" value="" placeholder="Option 2" class="form-control input-sm input_options">'+
				'</div>'+
				'</div>'+
				'<a id="btn_add_question" class="fa fa-plus-square fa-2x"></a>'
				);


			// $(this).parent().find('.panel').append('<div id="poll_question"></div>');
		});

		$('body').on('click', '#btn_post_post', function(event) {
			event.preventDefault();
			$('#post_text').attr({
				placeholder: 'Have doubts? Write up here...'
			});

			$(this).css({
				'background-color': '#65CEA7',
				'color': '#424d64'
			});
			$('#btn_post_poll').css({
				'background-color': 'initial'
			});

			$('#btn_post').show();
			$('#btn_poll').hide();
			$('#poll_questions').hide();


		});

		$btn_add_question = $('#btn_add_question').clone();
		
		$(function() {

			$('body').on('click', '#btn_poll', function(event) {
				event.preventDefault();
				$arr_poll_options = [];
				$poll_question = $('#post_text').val();

				$('.input_options').each(function(index, el) {
					$arr_poll_options.push($(this).val());
				});
				// alert($poll_question + $arr_poll_options);
				$.ajax({
					url: '<?php echo base_url('stream/push_poll') ?>',
					type: 'POST',
					data: {
						class_id: "<?php echo $this_class['class_id'] ?>",
						poll_question: $poll_question,
						arr_poll_options: $arr_poll_options
					},
				})
				.done(function(response) {
					if (response.poll_added === true) {
						$('#post_text').attr({
							placeholder: 'Ask question',
							value: ''
						});
						load_poll("<?php echo $this_class['class_id'] ?>");
						
						$('#btn_post_post').click();



					}
					console.log(response);
				})
				.fail(function(jqXHR, textStatus, errorThrown) {
					console.log(errorThrown);
				})
				.always(function() {
					console.log("complete");
				});
				

			});
			$('body').on('click','#btn_add_question', function( e ) {
				e.preventDefault();

				// $option_no = $(this).prev().find('input').attr('placeholder');
				// $option_no = parseInt($option_no.match(/\d+/)) + 1;
				// console.log($option_no);


				$('#btn_add_question').remove();
				$('#poll_questions').append('<div class="row"  style="margin-top: 10px;margin-bottom: 10px;"><div class="col-md-10"><input type="text" id="" name="option" value="" placeholder="Option" class="form-control input-sm input_options"></div><div class="col-md-2"><a class="fa fa-minus-square fa-2x btn_remove_question"></a></div></div><a id="btn_add_question" class="fa fa-plus-square fa-2x"></a>');

				$('#poll_questions').append($btn_add_question);

				$('.input_options').each(function(index, el) {
					$(this).attr('id',"option_"+(index+1));
					$(this).attr('placeholder',"Option " + (index + 1));

				});
				
			});


			$(document).on('click', '.btn_remove_question', function( e ) {
				e.preventDefault();

				$(this).closest( 'div.row' ).remove();
				$('.input_options').each(function(index, el) {
					$(this).attr('placeholder',"Option " + (index + 1));
				});

				$('.input_options').each(function(index, el) {
					$(this).attr('id',"option_"+(index+1));
				});
			});
		});

		$(document).on('click', '#btn_answer_poll', function(event) {
			event.preventDefault();
			$choice = $('input[name=radio_poll]:checked', '#form_poll').val();
			$poll_id = $(this).data('poll_id');
			console.log($poll_id + $choice);
			$this = $(this);
			if ($choice) {
				$.ajax({
					url: '<?php echo base_url('stream/add_poll_choice') ?>',
					type: 'POST',
					data: {
						poll_id: $poll_id,
						choice: $choice
					},
				})
				.done(function(response) {
					console.log(response);
					$this.attr({
						disabled: 'true'
					});	
					console.log("hahhas");
				})
				.fail(function(jqXHR, textStatus, errorThrown) {
					console.log(errorThrown);
				})
				.always(function() {
					console.log("complete");
				});

			}
			
			

		});
	});
</script>
<!--pickers initialization-->
<script src="<?php echo base_url('assets'); ?>/js/pickers-init.js"></script>
