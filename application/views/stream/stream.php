
<!--pickers css-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets'); ?>/js/bootstrap-datepicker/css/datepicker-custom.css" />

<!-- <link rel="stylesheet" type="text/css" href="https://uxsolutions.github.io/bootstrap-datepicker/bootstrap-datepicker/css/bootstrap-datepicker.min.css" /> -->


<!--body wrapper start-->
<div class="wrapper">


	<div class="row">
		<div class="col-md-2">
			
		</div>
		<div class="col-md-8">
			<div class="row">
				<div class="col-md-3">

				</div>
				<div class="col-md-7">
					
				</div>
				<div class="col-md-12">
				</div>
				<div class="col-md-12">

				</div>
				<div class="col-md-12">

				</div>
			</div>
		</div>

		<div class="col-md-8">
			

		</div>
	</div>

	<div class="row">
		<div class="col-md-4"></div>
		<div class="col-md-4">
			<div class="alert alert-success text-center">
				<h2 class="center"><?php echo $this_class['class_code']; ?></h2>
			</div>

		</div>
		<div class="col-md-4"></div>
	</div>
	<div class="row">
		<div class="col-md-4"></div>
		<div class="col-md-2">
			<a type="button" class="btn btn-primary btn-block">Continue Last Session</a>
		</div>
		<div class="col-md-2">
			<a href="#new_session_modal" data-toggle="modal" type="button" class="btn btn-primary btn-block">New Session</a>
		</div>
		<div class="col-md-4"></div>
	</div>
	
<!-- 	<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-5">
			<div class="input-group m-bot15">
				<input type="text" class="form-control input-lg datepicker">
				<span class="input-group-btn">
					<button class="btn btn-default" style="padding: 13%;" type="button">Start Session</button>
				</span>
			</div>
		</div>
		<div class="col-md-3"></div></div> -->

</div>
<!--body wrapper end-->

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="new_session_modal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<form method="POST" action="<?php echo base_url('classes/create_session');?>" accept-charset="utf-8">
				<div class="modal-header">
					<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
					<h4 class="modal-title">
						Add New Session 
					</h4>
				</div>
				<div class="modal-body">		
					<div class="row">
						<div class="col-md-3">
							<label for="session_name">Session Name </label>
						</div>
						<div class="col-md-9">
							<input type="text" name="session_name" id="session_name"><br><br>		
						</div>
					</div>				
					
					<div class="row">
						<div class="col-md-3">
							<label for="session_desc">Description</label>
						</div>
						<div class="col-md-9">
							<input type="text" name="session_desc" id="session_desc" placeholder=""><br>
						</div>
					</div>

					<br>	

					<input type="hidden" name="class_id" value="<?php echo $this_class['class_id']; ?>">
				</div>
				<div class="modal-footer">

					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" id="btn_add_session" class="btn btn-primary">Add Session</button>
				</div>
			</form>
		</div>

	</div>
</div>

<!--pickers plugins-->
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/js/bootstrap-datepicker/js/bootstrap-datepicker1.js"></script>

<script type="text/javascript" src="<?php echo base_url('assets'); ?>/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/js/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/js/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>

<!-- <script type="text/javascript" src="https://uxsolutions.github.io/bootstrap-datepicker/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script> -->

<!--pickers initialization-->
<script src="<?php echo base_url('assets'); ?>/js/pickers-init.js"></script>

<script>
	var $selected_date="ha";
	$(document).ready(function() {
		
/*		$('#datepicker').datepicker({
			format: 'dd-mm-yyyy',
			autoclose: true
		});
		$("#datepicker").datepicker("setDate", 'now');
		
		
		$selected_date = $('#datepicker').datepicker('getFormattedDate');			
		$('#datepicker').on('changeDate', function() {
			$('#dpfield').val($('#datepicker').datepicker('getFormattedDate'));
			$selected_date = $('#dpfield').val();
			console.log($selected_date);

		});
		*/
		
		
		$('.datepicker').datepicker({
			format: 'dd-mm-yyyy',
			autoclose: true
		});
		$(".datepicker").datepicker("setDate", 'now');
		
		
		


		// $('.default-date-picker').datepicker({
		// 	format: 'dd-mm-yyyy',
		// 	autoclose: true
		// });

	});
</script>