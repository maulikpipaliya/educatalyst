<?php $current_user_details = $this->session->userdata('user_details'); ?>


<?php if (!empty($all_posts)) : ?>
	<div class="panel-body" id="all_posts">
		<ul class="activity-list">
			<?php foreach ($all_posts as $post) :?>
				<li>
					<div class="avatar">

					</div>
					<div class="activity-desk" style="margin-left: 0px;">
						<div class="row">
							<div class="col-md-1"></div>
							<div class="col-md-10">
								<div class="pull-right">
									
								</div>

								<h5 style="margin-top: 0px;">
									<a href="#"><?php echo $post['firstname']." ".$post['lastname']; ?></a> 
									<span>posted an update.</span>
								</h5>
								<p class="text-muted" style="font-size: 10px;"><?php echo $post['created_at']; ?></p>
								<p><?php echo $post['post_content']; ?></p>
							</div>
						</div>
						<div class="pull-left">
							
						</div>
					</div>
				</li>
			<?php endforeach; ?>

		</ul>
	</div>
<?php endif; ?> 
