
<!-- icheck -->
<link href="<?php echo base_url('assets'); ?>/js/iCheck/skins/minimal/minimal.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/js/iCheck/skins/minimal/red.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/js/iCheck/skins/minimal/green.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/js/iCheck/skins/minimal/blue.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/js/iCheck/skins/minimal/yellow.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/js/iCheck/skins/minimal/purple.css" rel="stylesheet">

<link href="<?php echo base_url('assets'); ?>/js/iCheck/skins/square/square.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/js/iCheck/skins/square/red.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/js/iCheck/skins/square/green.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/js/iCheck/skins/square/blue.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/js/iCheck/skins/square/yellow.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/js/iCheck/skins/square/purple.css" rel="stylesheet">

<link href="<?php echo base_url('assets'); ?>/js/iCheck/skins/flat/grey.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/js/iCheck/skins/flat/red.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/js/iCheck/skins/flat/green.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/js/iCheck/skins/flat/blue.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/js/iCheck/skins/flat/yellow.css" rel="stylesheet">
<link href="<?php echo base_url('assets'); ?>/js/iCheck/skins/flat/purple.css" rel="stylesheet">


<div class="panel-body">
	<ul class="activity-list">

		<li>
			<div class="avatar">

			</div>
			<div class="activity-desk" style="margin-left: 0px;">
				<div class="row">
					<div class="col-md-1"></div>
					<div class="col-md-10">
						<div class="pull-right">

						</div>

						<h5 style="margin-top: 0px;">
							<a style="color: #454d64; text-decoration: none;"><?php echo $polls['0']['text']; ?></a> 
							<span></span>
						</h5>

						<form class="form-horizontal bucket-form" method="get">
							<div class="form-group">
								<?php foreach ($polls as $poll) :?>
									<div class="row">
										<div class="col-md-3" style="margin-top: 15px;margin-bottom: 15px;">
											<label>
												<?php echo $poll['option_text']; ?>
											</label>
										</div>
										<div class="col-md-5" style="margin-top: 20px;margin-bottom: 15px;">
											<div class="progress " style="min-width: 100px;">
												<div style="width: 50%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="50" role="progressbar" class="progress-bar">
													50
												</div>
											</div>		
										</div>

									</div>
									
								<?php endforeach;?>



								
							</div>
						</form>

						<p class="text-muted" style="font-size: 10px;"></p>
						<p></p>
					</div>
				</div>
				<div class="pull-left">

				</div>
			</div>
		</li>
	</ul>
</div>


<!--icheck -->
<script src="<?php echo base_url('assets'); ?>/js/iCheck/jquery.icheck.js"></script>
<script src="<?php echo base_url('assets'); ?>/js/icheck-init.js"></script>