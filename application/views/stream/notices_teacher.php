
<?php foreach ($notices as $notice) :?>
<div class="row">
	<div class="col-md-8">
		<li>
			<?php echo $notice['notice']; ?>
		</li>
			
	</div>
	<div class="col-md-4">
		<div class="pull-right">
				<a  class="btn notices">
					<i class="fa fa-minus-circle btns-delete-notice" data-notice_id= "<?php echo $notice['notice_id'] ?>" style="color: red"></i>
				</a>
			</div>
	</div>
</div>
<?php endforeach; ?>