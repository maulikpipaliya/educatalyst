<?php foreach ($files as $file) :?>
	<div class="preview file-preview processing success"> 
		<div class="details">   
			<a href="<?php echo base_url('classes/do_download/').$file['id']."/".$file['alias']; ?>" target="_blank" data-id="<?php echo $file['id']; ?>" id="dl_link">
			<div class="filename">
				<span>
					<?php echo $file['alias']; ?>
				</span>
			</div>
			</a>
			
		</div>
		<div class="progress">
			<span class="upload" style="width: 100%;"></span>
		</div>
		<div class="success-mark" style="">
			<span>✔</span>
		</div>  
		<div class="error-mark">
			<span>✘</span>
		</div> 
		<div class="error-message">
			<span></span>
		</div>
	</div>	
<?php endforeach; ?>

