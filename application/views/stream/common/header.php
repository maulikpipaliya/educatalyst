<?php  
$ssn_class_details = $this->session->userdata('ssn_class_details');

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="ThemeBucket">
  <link rel="shortcut icon" href="#" type="image/png">

  <title>Horizontal menu Page</title>

  <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/css/style-responsive.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!--[if lt IE 9]>
      <script src="<?php echo base_url(); ?>assets/js/html5shiv.js"></script>
      <script src="<?php echo base_url(); ?>assets/js/respond.min.js"></script>
    <![endif]-->

    <!-- Placed js at the end of the document so the pages load faster -->
    <script src="<?php echo base_url(); ?>assets/js/jquery-1.10.2.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
  </head>

  <body class="horizontal-menu-page">
    <section>
      <nav class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.html">
              <img src="<?php echo base_url(); ?>assets/images/main_logo.png" alt="Logo" height="50px">
            </a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

            <ul class="nav navbar-nav"  style="margin-top: 8px;">
              <li class="active">
                <a href="<?php echo base_url('classes/stream/').$ssn_class_details['class_id']; ?>">Stream</a>
              </li>
              <li class="">
                <a href="<?php echo base_url('classes/students'); ?>">Students</a>
              </li>
            </ul>

            <ul class="nav navbar-nav navbar-right"  style="margin-top: 8px;">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <img alt="" src="images/photos/user-avatar.png">
                  This Class
                  <b class="caret"></b>
                </a>

                <ul class="dropdown-menu">
                  <li><a href="#">Information</a></li>
                  <li><a href="#">Stuff</a></li>
                  <li><a href="<?php echo base_url('classes/change_class'); ?>">Change</a></li>
                </ul>
              </li>
              
              <?php 
              $user_details   = $this->session->userdata('user_details');
              if (!empty($user_details)): ?>

              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <img alt="" src="images/photos/user-avatar.png">

                  <?php echo $user_details['username']; ?>
                  <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo base_url('requests') ?>">Requests</a></li>
                    <li><a href="<?php echo base_url('classes') ?>">Classes</a></li>
                    <li><a href="<?php echo base_url('classes/materials/').$ssn_class_details['class_id']; ?>">Materials</a></li>
                    <li><a href="<?php echo base_url('institute/select_institute') ?>">Institutes</a></li>
                    <li><a href="<?php echo base_url('profile') ?>">Profile</a></li>
                    <li><a href="<?php echo base_url('setting') ?>">Settings</a></li>
                    <li><a href="<?php echo base_url('users/logout') ?>">Log Out</a></li>
                  </ul>
                </li>
              <?php endif; ?>
              </ul>
            </div>
            <!-- /.navbar-collapse -->
          </div>
          <!-- /.container-fluid -->
        </nav>
