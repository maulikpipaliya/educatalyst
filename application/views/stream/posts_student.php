<?php $current_user_details = $this->session->userdata('user_details'); ?>

<!-- <?php echo count($all_posts); echo "<pre>";	 print_r($all_posts); ?> -->
	
<?php if (!empty($all_posts)) : ?>
	<div class="panel-body" id="all_posts">
		<ul class="activity-list">
			<?php foreach ($all_posts as $post) :?>
				<li>
					<div class="avatar">

					</div>
					<div class="activity-desk" style="margin-left: 0px;">
						<div class="row">
							<div class="col-md-1">
								<a class="btn btn-success btn_upvote" data-user_id="<?php echo $post['user_id']; ?>" data-post_id="<?php echo $post['post_id']; ?>" style="border-color: 1px solid #50cea1;background-color: <?php echo  ($post['has_upvoted'] == "1") ? "#50cea1" : "transparent" ; ?>; color: #454d64">
									<?php echo $post['upvotes']; ?>+
								</a>
							</div>
							<div class="col-md-10">
								<div class="pull-right">
									<?php 

									
									if ($current_user_details['user_id'] == $post['user_id']) :
										?>
										<div class="row">
											<div class="col-md-4">
												<a href="#modal_edit" class="fa fa-edit" id="icon_edit" data-post_id="<?php echo $post['post_id']; ?>" data-toggle="modal" data-content="<?php echo $post['post_content']; ?>">
												</a>
											</div>     
											<div class="col-md-6">
												<a href="#modal_delete" id="icon_delete" class="fa fa-trash-o" data-toggle="modal" style="color:red;" data-post_id="<?php echo $post['post_id']; ?>" data-content="<?php echo $post['post_content']; ?>"></a>
											</div>
										</div>
									<?php endif;?>
								</div>

								<h5 style="margin-top: 0px;">
									<a href="#"><?php echo $post['firstname']." ".$post['lastname']; ?></a> 
									<span>posted an update.</span>
								</h5>
								<p class="text-muted"><?php echo $post['created_at']; ?></p>
								<p><?php echo $post['post_content']; ?></p>
							</div>
						</div>
						<div class="pull-left">
							
						</div>
					</div>
				</li>
			<?php endforeach; ?>

		</ul>
	</div>
<?php endif; ?> 


<script>
	$(document).ready(function() {
		
	});

</script>