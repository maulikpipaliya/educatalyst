<style>
.wrapper{
	background-image: linear-gradient(#424F63, white);
	height: 100vh;
}		

.tagline1,.tagline2{
	padding-left: 15px;
	color: #313d52;
	line-height: 50px;
	font-size: 50px;
	width: 400px;
	max-width: 900px;
	font-weight: 600;
	margin-top: 100px;
	margin-left: 100px;
	border-left: 10px solid #313d52;
}

.tagline1{
	font-size: 15px;
	border-left: 0px;
	margin-left: 115px;
}
.buttons{
	padding-left: 30px;
	color: white;
	margin-top: 50px;
	margin-left: 100px;
	max-width: 400px;
}
#btn_create_inst{
	padding: 15px;
	background-color: #454d64; border:3px solid white
	color: white;
	border-radius: 15px;
}
#btn_get_started{
	padding: 15px;
	width: 200px;
	background-color: #00d9b9; 
	border:5px solid #454d64;
	color: #454d64;
	border-radius: 15px;	
}
#btn_create_inst:hover{
	background-color: white;
	/*border:1px solid #50cea1;*/
	color: #454d64;
}
#btn_join_inst{
	padding: 15px;
	background-color: #50cea1;
	border:1px solid #454d64;
	color: #454d64;
	border-radius: 15px;
}
#btn_join_inst:hover{
	background-color: white;
	/*border:3px solid #50cea1;*/
	color: #454d64;
}


#image1{
	height: 400px;
	width: 450px;
	margin-right: 200px;
	margin-top: 30px;
}
#image2{
	/*height: 400px;
	width: 450px;*/
	margin-right: 100px;
	margin-top: 50px;
}



</style>
<div class="wrapper">
	<?php if ($is_logged_in) : ?>
		<img id="image1" src="<?php echo base_url('assets/images/')."stuffs2.png"; ?>" alt="aa" align="right">
	<?php else: ?>
		<img id="image2" src="<?php echo base_url('assets/images/')."stuffs.png"; ?>" alt="aa" align="right">
	<?php endif;  ?>



	<div class="row">
		

		<div class="col-md-4">
			<?php if ($is_logged_in) : ?>
				<div class="tagline1">Get your institute registered ,<a href="<?php echo base_url('institute/create'); ?>" style="color: #50cea1"> create new one</a>
					<p>And if you've been here more than once!</p>
				</div>
				<div class="buttons">
					<div class="row">
						<div class="col-md-5">
							<a href="<?php echo base_url('institute/join'); ?>" id="btn_create_inst" class="btn" style="margin-right: 15px;">Join with a code</a>						
						</div>
						<div class="col-md-1"></div>
						<div class="col-md-5">
							<a href="<?php echo base_url('institute/select_institute'); ?>" id="btn_join_inst" class="btn" style="">
							 Institutes I've joined
							
							</a>		
						</div>
					</div>
				</div>
			<?php else: ?>
				<div class="tagline2">Not all classrooms have four walls</div>
				<div class="buttons">
					<div class="row">
						<div class="col-md-5">
							<a href="<?php echo base_url('users/signin?continue_url=dashboard'); ?>" id="btn_get_started" class="btn" style="">Get Started</a>						
						</div>
						<div class="col-md-1"></div>	
					</div>
				</div>
				

			<?php endif; ?>
		</div>
		<div class="col-md-4"></div>
		<div class="col-md-4"></div> 
	</div>
</div>