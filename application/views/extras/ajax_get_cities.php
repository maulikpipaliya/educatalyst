<select name="dd_cities" id="dd_cities" class="form-control input-sm custom-state-dd">
	<option value="" selected>Select City</option>	
	<?php foreach ($cities as $key => $value): ?>
		<option value="<?php echo $value->id; ?>">
			<?php echo $value->name; ?>
		</option>
	</select>
	
<?php endforeach; ?>