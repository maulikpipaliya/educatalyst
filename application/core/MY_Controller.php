<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Base_controller extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		
	}
	public function index()
	{
		
	}
	
	
	
	public function console_log_table( $data, $comment = 'Output :' ) {
		ob_start();
		// $output  = 'console.info( \'' . $comment . ':\' );';

		$output  = 'console.group("'. $comment .'");';
		$output .= 'console.table(' . json_encode( $data ) . ');';
		$output .= 'console.groupEnd();';
		$output  = sprintf( '<script>%s</script>', $output );
		echo $output;
	}

	public function console_log( $data, $comment = 'Error :' ) {
		ob_start();
		// $output  = 'console.info( \'' . $comment . ':\' );';

		$output  = 'console.group("'. $comment .'");';
		$output .= 'console.log(' . json_encode( $data ) . ');';
		$output .= 'console.groupEnd();';
		$output  = sprintf( '<script>%s</script>', $output );
		echo $output;
	}

}

class Common_controller extends Base_controller {
	public function index(){}
	
	public function get_role_id_in_institute($user_id, $institute_code)
	{
		$this->db->select('role_id');
		$this->db->where('user_id', $user_id);
		$this->db->where('institute_code', $institute_code);
		return $this->db->get('tbl_user_inst')->result_array();
	}

	public function get_role_name_by_role_id($role_id)
	{
		$this->db->where('role_id', $role_id);
		$row = $this->db->get('tbl_roles',1)->row();
		return (!empty($row)) ? $row->role_name : null ;

	}


	public function get_institute_name_by_code($institute_code)
	{
		$this->db->select('institute_name');
		$this->db->where('institute_code', $institute_code);
		$row =  $this->db->get('tbl_institute')->row();
		return (!empty($row)) ? $row->institute_name : null ;
	}

	public function get_current_user_id()
	{
		$user_details = $this->session->userdata('user_details');

		$this->db->select("*");
		$this->db->from('tbl_user');
		$this->db->where('username', $user_details['username']);
        // return $this->db->get()->row()->user_id;
		$row = $this->db->get()->row();
		if (!empty($row)) {
			return $row->user_id;
		}
	}

	public function get_class_row($class_id = '')
	{
		$this->db->where('class_id', $class_id);
		return $this->db->get('tbl_class')->row_array();
	}

	public function get_session_row($session_id = '')
	{
		$this->db->where('session_id', $session_id);
		return $this->db->get('tbl_session')->row_array();	
	}

	public function generate_class_code()
	{
		return substr(str_shuffle(MD5(microtime())), 0, 5);
	}
}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */

/* End of file MY_controller.php */
/* Location: ./application/core/MY_controller.php */