<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
	}
	public function index()
	{
		$this->load->view('common/header');
		$this->load->view('setting');	
		$this->load->view('common/footer');
		
	}

}

/* End of file Setting.php */
/* Location: ./application/controllers/Setting.php */