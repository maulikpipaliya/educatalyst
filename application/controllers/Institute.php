<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Institute extends Common_controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Mdl_institute');
		$this->load->model('Mdl_users');
		$this->load->model('Mdl_request');
		$this->load->model('Mdl_essential');
		$this->load->model('Mdl_common');
		$this->load->model('Mdl_class');


	}

	public function index()
	{
		
	}
	
	public function select_institute()
	{
		$this->session->set_userdata("ssn_inst_join","");

		if(empty($this->session->userdata('is_logged_in'))){
			redirect('users/signin','refresh');
		}	

		$user_id = $this->Mdl_users->get_current_user_id();
		$data['connections'] = $connections  = $this->Mdl_institute->get_connected_institutes($user_id);
		
		// print_r($data['connections']);
		// SELECT institute_name FROM tbl_institute INNER JOIN tbl_user_inst ON tbl_institute.institute_code= tbl_user_inst.institute_code WHERE user_id="65" 
		$this->db->distinct();
		$this->db->select('I.institute_name,UI.role_id,UI.institute_code,R.role_name,');
		$this->db->from('tbl_institute I');
		$this->db->join('tbl_user_inst UI', 'I.institute_code = UI.institute_code', 'innner');
		$this->db->join('tbl_roles R', 'R.role_id = UI.role_id', 'inner');
		$this->db->where('user_id', $user_id);
		$data['user_institutes'] = $this->db->get()->result_array();
		
		// echo $this->db->last_query();
		// echo "<pre>";
		// print_r($data['user_institutes']);

		$this->load->view('common/header');
		$this->load->view('institute/select_inst', $data);
		$this->load->view('common/footer', $data, FALSE);
		

		// foreach ($connections as $connection) {
		// 	echo $arr_institutes[] = $connection['institute_code'];
		// }

		// $this->db->from('tbl_institute');
		// $this->db->where_in('institute_code', $arr_institutes);
		// $institutes = $this->db->get()->result_array();

		// foreach ($institutes as $institute) {
		// 	echo $institute['institute_name'];
		// }
	}


	public function dive_in()
	{
		$institute_code = $this->input->post('institute_code');
		
		$role_id = $this->input->post('role_id');

		$array_inst_join = array(
			'institute_code' => $institute_code,
			'role_id' => $role_id
		);

		$this->session->set_userdata("ssn_inst_join", $array_inst_join);
		$ssn_inst_join = $this->session->userdata('ssn_inst_join');
		$arr_inst_details = $this->Mdl_institute->get_institute_details($institute_code);	
		$this->session->set_userdata("ssn_inst_details", $arr_inst_details);
		// redirect('classes/home');

		if ($ssn_inst_join['role_id'] == "1") {
			redirect('institute/student');
		}
		elseif ($ssn_inst_join['role_id'] == "2") {
			redirect('institute/teacher');
		}
		elseif ($ssn_inst_join['role_id'] == "3") {
			redirect('institute/head');
		}
		else{
			$this->session->flashdata('message',"Nothing selected");
		}
	}

	public function student($param = '')
	{
		$data = null;
		$user_id = 	$user_id = $this->get_current_user_id();
		$data['ssn_inst_join'] = $this->session->userdata('ssn_inst_join');
		$data['classes_joined'] = $this->Mdl_class->list_classes_joined($user_id);
		// echo "<pre>";
		// echo count($data['classes_joined']);
		// echo "<br>";
		// print_r($data['classes_joined']);
		$data['role_name'] = $this->get_role_name_by_role_id($data['ssn_inst_join']['role_id']);

		$this->load->view('common/header', $data, FALSE);
		$this->load->view('institute/home_student', $data, FALSE);
		$this->load->view('common/footer', $data, FALSE);	
	}

	public function teacher($param = '')
	{
		$data = null;
		$user_id = $this->get_current_user_id();
		
		if($_GET){
			$parent_id = $this->input->get('parent');
		}
		else{
			$parent_id= "0";
		}

		$data['classes'] = $this->Mdl_class->list_classes($user_id,$parent_id);
		
		$data['parent_id'] = $parent_id;

		$this->load->view('common/header', $data, FALSE);
		$this->load->view('institute/home_teacher', $data, FALSE);
		$this->load->view('common/footer', $data, FALSE);	
		
	}
	public function head($param = '')
	{
		$user_id = $this->Mdl_users->get_current_user_id();

		$data['ssn_inst_join'] = $this->session->userdata('ssn_inst_join');
		$data['ssn_inst_details'] = $this->session->userdata('ssn_inst_details');
		$data['role_name'] = $this->get_role_name_by_role_id($data['ssn_inst_join']['role_id']);
		$data['requests'] = $this->Mdl_request->list_requests_of($user_id);
		$data['teachers'] = $this->Mdl_institute->teachers_list($data['ssn_inst_join']['institute_code']);
		
		/*echo "<pre>";
		print_r($data);die;*/
		$data['ssn_inst_join'] = $this->session->userdata('ssn_inst_join');
		$data['ssn_inst_details'] = $this->session->userdata('ssn_inst_details');
		$data['role_name'] = $this->get_role_name_by_role_id($data['ssn_inst_join']['role_id']);
		$this->load->view('common/header', $data, FALSE);
		$this->load->view('institute/inst_head', $data, FALSE);
		$this->load->view('common/footer', $data, FALSE);	
		
	}
	public function home()
	{
		if (!($this->session->userdata('is_logged_in'))) {
			$continue_url = urlencode("institute/home");
			redirect('users/signin?continue_url='.$continue_url);
		}

		$this->load->view('common/header');

		if (($this->session->userdata('ssn_inst_join'))) {
			$ssn_inst_join = $this->session->userdata('ssn_inst_join');
			$role_id = $ssn_inst_join['role_id'];
			$institute_code = $ssn_inst_join['institute_code'];
			
			$data['role_name'] = $this->Mdl_institute->get_role_name_by_id($role_id);
			$data['institute_name'] = $this->get_institute_name_by_code($institute_code);

			if ($role_id == "1") {
				$this->load->view('institute/inst_student', $data);				
			}	
			elseif ($role_id == "2") {
				$this->load->view('institute/home_teacher', $data);				
			}
			elseif ($role_id == "3") {
				$this->load->view('institute/inst_head', $data);				
			}
			elseif ($role_id == "4") {
				$this->load->view('institute/inst_asst', $data);					
			}
			else {
				$this->Mdl_essential->console_log("institute/home : role_id except 1/2/3/4");
			}
			
		}
		
		$this->load->view('common/footer');
		
	}

	public function get_requests($user_id)
	{
		$requests = $this->Mdl_request->list_requests_of($user_id);

	}

	public function get_next_inst_code_trail()
	{
		$trail = $this->Mdl_institute->get_inst_code_trailer();
		echo $trail['0']['Trail'];
		// print_r($trail);
	}

	public function code_exists()
	{
		$code = $this->input->post('join_code');
		$rs = $this->Mdl_institute->institute_code_exists($code);
		if (!empty($rs)) {
			echo json_encode(TRUE);
			return true;
		}
		else{
			
			echo json_encode(FALSE);
			return false;
		}
	}
	
	public function on_join_submit()
	{
		$user_id = $this->Mdl_users->get_current_user_id();
		$join_code = $this->input->post('join_code');		

		$arr_join = array(
			'user_id' => $user_id,
			'code' => $join_code );
		
		$arr_code =	$this->Mdl_common->get_code_details($join_code);

		if (!empty($arr_code)) {
			$institute_code = $arr_code['institute_code'];

			if ($arr_code['code_type'] == "0") { 
				//code is for teachers
				$request_to_id = $arr_code['user_id'];
				$arr_request = array(
					'request_from_id' => $user_id, 
					'request_to_id' => $request_to_id,
					'institute_code' => $institute_code,
					'as_role_id' => '2', // Teacher Role ID
			// 'status' => '2'  // 2: Pending
				);

				$this->console_log_table($arr_request);


				$this->db->where($arr_request);
				$row = $this->db->get('tbl_request')->row_array();
				$request_already_exists = (!empty($row)) ? true : false ;

				$this->console_log_table($row , "Request");

				if ($request_already_exists) {
					if ($row['status'] == "1") {
						$array_inst_join = array(
							'institute_code' => $institute_code,
							'role_id' => $row['as_role_id']
						);
						$this->session->set_userdata("ssn_inst_join", $array_inst_join);
						redirect('institute/home');

					}
					elseif ($row['status'] == "2") {
						$this->session->set_flashdata('request_status', 'pending');		 		
						redirect('institute/join');
					}
				}
				else{
					$arr_request['status'] = "2";
					$this->Mdl_request->send_request($arr_request);
					$this->session->set_flashdata('request_status', 'sent');		 		

					redirect('institute/join');
				}
			}
			else if ($arr_code['code_type'] == "1") { 
				$arr_user_inst = array(
					'user_id' => $user_id,
		 			'role_id' => "1", //student role id
		 			'class_code' => $join_code,
		 			'institute_code' => $institute_code
		 		);

				$row = $this->db->where($arr_user_inst)->get('tbl_user_inst')->row_array();

				if (empty($row)) {
					$this->db->insert('tbl_user_inst', $arr_user_inst); 	
				}

				// $arr_code_2 = $this->Mdl_common->get_code_details($join_code);
				$class_id  = $arr_code['class_id'];
				redirect('classes/stream/'.$class_id);
			}
			elseif ($arr_code['code_type'] == "2") {
				//Clone COde
			}

		}



	}

	public function insert_institute()
	{
		$user_details = $this->session->userdata('user_details');

		$form_data['create_institute_form']['creator_user_id'] = $this->Mdl_users->get_current_user_id();

		$form_data['create_institute_form']['institute_name'] =$this->input->post('institute_name');
		$form_data['create_institute_form']['institute_type_id'] =$this->input->post('institute_type_id');		
		$form_data['create_institute_form']['state_id'] =$this->input->post('dd_states');		
		$form_data['create_institute_form']['city_id'] =$this->input->post('dd_cities');	
		$form_data['create_institute_form']['address'] =$this->input->post('address');	
		$form_data['create_institute_form']['address'] =$this->input->post('address');			
		$form_data['create_institute_form']['postal_code'] =$this->input->post('postal_code');			
		$form_data['create_institute_form']['institute_code'] =$this->input->post('institute_code');		
		$form_data['user_role_inst']['user_id'] = $this->Mdl_users->get_current_user_id();
		$form_data['user_role_inst']['role_id'] = 3; // role_id of Head
		$form_data['user_role_inst']['institute_code'] =$this->input->post('institute_code');



		$insert_institute = $this->Mdl_institute->db_insert_institute($form_data['create_institute_form']);	
		$this->Mdl_institute->db_insert_institute_user($form_data['user_role_inst']);


		if ($insert_institute) {
			redirect('dashboard');
		}

	}

	public function join()
	{
		if (!($this->session->userdata('is_logged_in'))) {
			$continue_url = urlencode("institute/join");
			redirect('users/signin?continue_url='.$continue_url);
		}


		$ssn_inst_join	= $this->session->userdata('ssn_inst_join');

		$this->load->view('common/header');		
		$this->load->view('institute/inst_join_new');		
		$this->load->view('common/footer');		
	}

	public function create()
	{

		if ($this->session->userdata('is_logged_in')) {
			$form_data['institutes']	= $this->Mdl_institute->get_institute_type_list();
			$form_data['states']  = $this->Mdl_users->get_states();

			$this->load->view('common/header');
			$this->load->view('institute/inst_create',$form_data);
			$this->load->view('common/footer');	
		}
		else{
			$continue_url = urlencode("institute/create");
			redirect('users/signin?continue_url='.$continue_url);
		}

	}

	public function join_code_exists()
	{
		$code = $this->input->post('join_code');
		$rs = $this->Mdl_institute->join_code_exists($code);

		if (!empty($rs)) {
			echo json_encode(TRUE);
			return true;
		}
		else{
			echo json_encode(FALSE);
			return false;
		}
	}
	public function institute_code_exists()
	{
		$code = $this->input->post('institute_code');
		$rs = $this->Mdl_institute->institute_code_exists($code);
		if (!empty($rs)) {
			echo json_encode(TRUE);
			return true;
		}
		else{

			echo json_encode(FALSE);
			return false;
		}
	}

	public function institute_on_join()
	{
		if ($_POST) {
			$user_id = $this->Mdl_users->get_current_user_id();
			$institute_code = $this->input->post('institute_code');
			$role_id = $this->input->post('join_as');

			$arr_inst_join = array(
				'institute_code' => $institute_code,
				'joined_as' =>  $role_id
			);

			$arr_inst_details = $this->Mdl_institute->get_institute_details($institute_code);	

			if (!empty($arr_inst_join) && !empty($arr_inst_details)) {
				if ($this->Mdl_institute->is_valid($role_id, $this->Mdl_users->get_current_user_id(), $institute_code)) {
					$this->session->set_userdata("ssn_inst_join", $arr_inst_join);
					$this->session->set_userdata("ssn_inst_details", $arr_inst_details);
					redirect('institute/join');	
				}
				else{

					if ($this->Mdl_users->get_request_status($user_id, $role_id, $institute_code) === false) {
						$req_sent = $this->Mdl_institute->send_request($institute_code,$role_id);
						if ($req_sent) {
							redirect('institute/join');	
						}
					}

				}	
			}

		}
	}



	public function join_old($value='')
	{
		$data['roles']	= $this->Mdl_users->list_roles();

		$ssn_inst_join	= $this->session->userdata('ssn_inst_join');
		$ssn_request = $this->session->userdata('ssn_request');

		if (!empty($this->session->userdata('ssn_request'))) {
			$data['has_requested'] = true;
		}

		if ($this->session->userdata('is_logged_in')) {

			$this->load->view('common/header');

			if (!empty($ssn_inst_join)) {
				$role_id = $ssn_inst_join['joined_as'];
				$user_id = $this->Mdl_users->get_current_user_id();
				$institute_code = $ssn_inst_join['institute_code'];

				if ($this->Mdl_institute->is_valid($role_id,$user_id,$institute_code)) {
					if ($role_id == 1) {
						$this->load->view('institute/inst_student',$data);			
					}
					elseif ($role_id == 2) {
						$this->load->view('institute/inst_teacher',$data);			
					}
					elseif ($role_id == 3) {
						$this->load->view('institute/inst_head',$data);	
					}
					elseif ($role_id == 4) {
						$this->load->view('institute/inst_asst',$data);		
					}
				}
				else{
					$this->load->view('institute/inst_join',$data);		
				}
			}
			else {
				$this->load->view('institute/inst_join',$data);		
			}
				// if ($ssn_inst_join['joined_as'] == 1) {
				// 	// if ($this->Mdl_users->get_request_status($this->Mdl_users->get_current_user_id(), $ssn_inst_join['joined_as'], $ssn_inst_join['institute_code']) == 1) {
				// 	// 	$this->load->view('institute/inst_student',$data);	
				// 	// }
				// 	// $this->load->view('institute/inst_join',$data);	
				// 	$this->load->view('institute/inst_student',$data);	

				// }
				// else if ($ssn_inst_join['joined_as']  == 2) {
				// 	// if ($this->Mdl_users->get_request_status($this->Mdl_users->get_current_user_id(), $ssn_inst_join['joined_as'], $ssn_inst_join['institute_code']) == 1) {
				// 	// 	$this->load->view('institute/inst_teacher',$data);		
				// 	// }
				// 	// $this->load->view('institute/inst_join',$data);	
				// 	$this->load->view('institute/inst_teacher',$data);		


				// }
				// else if ($ssn_inst_join['joined_as']  == 3) {

				// 	// $is_head = $this->Mdl_institute->is_valid($ssn_inst_join['joined_as'],$this->Mdl_users->get_current_user_id(),$ssn_inst_join['institute_code']);

				// 	// if ($is_head) {
				// 	// 	$this->load->view('institute/inst_head',$data);	
				// 	// }
				// 	// $data['error'] = "Not Head of this institute";

				// 	// $this->load->view('institute/inst_join',$data);		
				// 	$this->load->view('institute/inst_head',$data);		 

				// }
				// else if ($ssn_inst_join['joined_as']  == 4) {
				// 	// if ($this->Mdl_users->get_request_status($this->Mdl_users->get_current_user_id(), $ssn_inst_join['joined_as'], $ssn_inst_join['institute_code']) == 1) {
				// 	// 	$this->load->view('institute/inst_asst',$data);		
				// 	// }
				// 	// $this->load->view('institute/inst_join',$data);	

				// 	$this->load->view('institute/inst_asst',$data);		
				// }


			$this->load->view('common/footer');		
		}
		else{
			$continue_url = urlencode("institute/join");
			redirect('users/signin?continue_url='.$continue_url);
		}


	}



	public function change_pos($value='')
	{
		$this->session->set_userdata('ssn_inst_join',"");
		redirect('institute/select_institute');
	}



	public function is_valid()
	{
		// echo "<script> console.log('".json_encode("hahah")."');</script>";
		$institute_code = $this->input->post('institute_code');
		$role_id = $this->input->post('role_id');
		$user_id =  $this->Mdl_users->get_current_user_id();

		if ($this->Mdl_institute->is_valid($role_id, $user_id, $institute_code)) {
			echo json_encode(true);
		}
		else{
			echo json_encode(false);
		}
	}

	public function block_teacher()
	{
		$teacher_id = $this->input->post("teacher_id", TRUE);
		$institute_code = $this->input->post("institute_code", TRUE);
		$is_blocked = $this->input->post("is_blocked", TRUE);

		$this->db->where("user_id", $teacher_id);
		$this->db->where("role_id", 2);
		$this->db->where("institute_code", $institute_code);
		$this->db->update("tbl_user_inst", array("is_blocked" => $is_blocked));

		$response['btn_add_class'] = ($is_blocked == 1)?"btn-success":"btn-danger";
		$response['btn_remove_class'] = ($is_blocked == 1)?"btn-danger":"btn-success";
		$response['btn_text'] = ($is_blocked == 1)?"Unblock":"Block";
		$response['is_blocked'] = ($is_blocked == 1)?0:1;		

		$this->output->set_content_type('application/json')->set_output(json_encode($response));
	}


}

/* End of file Institute.php */
/* Location: ./application/controllers/Institute.php */
