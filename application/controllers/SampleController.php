<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class SampleController extends CI_Controller {
    
        
        public function __construct()
        {
            parent::__construct();
            $this->load->model('SampleModel');
    
        }
        
        public function index(){
        
            // print_r($this->db->conn_id);

            $mdl_obj    = new SampleModel;
            $data['data']   = $mdl_obj->get_data();
            
            $this->load->view('common/header');
            
            $this->load->view('wrapper',$data);

            $this->load->view('common/footer');

        }


    }
    
    
    
?>