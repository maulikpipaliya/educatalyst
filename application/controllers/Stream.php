<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stream extends Common_Controller {
	
	public $class_id;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Mdl_class');
		$this->load->model('Mdl_post');
	}
	
	public function index(){}

	public function load_posts($class_id)
	{
		// $class_id = $this->Mdl_class->getallheaders()et_current_class_id();
		$data['user_details'] = $this->session->userdata('user_details');
		$data['all_posts'] = $this->Mdl_post->get_all_posts($class_id);
		$this->load->view('stream/posts_teacher', $data, FALSE);
	}

	public function load_poll($class_id)
	{
		$data['polls'] = $this->Mdl_post->get_poll($class_id);
		// echo "<pre>";
		// print_r($data['polls']);
		$this->load->view('stream/teacher/poll_teacher', $data, FALSE);
	}

	public function load_poll_student($class_id)
	{
		$data['polls'] = $this->Mdl_post->get_poll($class_id);
		// echo "<pre>";
		// print_r($data['polls']);
		$this->load->view('stream/student/poll_student', $data, FALSE);
	}

	public function load_notices($class_id)
	{
		$data['notices'] = $this->Mdl_post->get_notices($class_id);
		// echo "<pre>";
		// print_r($data['notices']);
		$this->load->view('stream/notices_teacher', $data, FALSE);
		
	}
	public function load_notices_student($class_id)
	{
		$data['notices'] = $this->Mdl_post->get_notices($class_id);
		// echo "<pre>";
		// print_r($data['notices']);
		$this->load->view('stream/notices_student', $data, FALSE);
		
	}
	public function post_update()
	{

		$response = array();
		
		$data['post_text'] = $this->input->post('post_text')."<br>";

		$array_post['user_id'] = $this->get_current_user_id();
		$array_post['post_content'] = $this->input->post('post_text');
		
		$class_id = $this->Mdl_class->get_current_class_id();
		$array_post['class_id'] = $class_id ;

		$rs = $this->db->where($array_post)->get('tbl_post')->row_array();
		$rs = (!empty($rs)) ? $rs : null ;
		
		if (empty($rs)) {
			$array_post['created_at'] = date("Y-m-d H:i:s");

			$this->db->insert('tbl_post', $array_post);	
		}
		
		
		$data['all_posts'] = $this->Mdl_post->get_all_posts($class_id);
		// $this->load->view('stream/posts', $data, FALSE);
		redirect('stream/load_posts/'.$class_id);

	}


	public function edit_post()
	{
		$post_id = $this->input->post('post_id');
		$post_content = $this->input->post('post_content');

		$this->db->set('post_content', $post_content);
		$this->db->where('post_id', $post_id);
		$this->db->update('tbl_post');
		if($this->db->affected_rows()>0){
			echo json_encode(true);
		}
		else{
			echo json_encode(false);	
		}
		
	}

	public function delete_post()
	{
		$post_id = $this->input->post('post_id');


		$this->db->set('is_deleted','1');
		$this->db->where('post_id', $post_id);
		$this->db->update('tbl_post');
		if($this->db->affected_rows()>0){
			echo json_encode(true);
		}
		else{
			echo json_encode(false);	
		}
	}

	public function do_upvote()
	{
		$response = null;	
		$user_id = $this->input->post('user_id');
		$post_id = $this->input->post('post_id');

		$arr_upvote = array(
			'user_id' => $user_id ,
			'post_id' => $post_id ,
			'has_upvoted' => '1');
		$row = $this->db->where($arr_upvote)->get('tbl_upvote')->row_array();
		// $this->console_log_table($row);
		$already_upvoted = (!empty($row)) ? true : false ;

		$response['already_upvoted'] = $already_upvoted;
		if (!($already_upvoted)) {
			$response['user_id'] = $user_id;
			$response['post_id'] = $post_id;

			if($this->Mdl_post->upvote_post($user_id,$post_id)){
				$response['just_upvoted'] = true;
			}
			else{
				$response['just_upvoted'] = false;			
			}
		}
		else{
			if ($this->Mdl_post->undo_upvote_post($user_id, $post_id)) {
				$response['undone_upvote'] = true;
			}
			else{
				$response['undone_upvote'] = false;	
			}
		}

		
		

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($response));

	}


	public function push_poll()
	{
		$post = $_POST;
		
		// $poll_id = $this->db->where('class_id', $post['class_id'])->get('tbl_poll')->row()->poll_id;
		$p['class_id'] = $post['class_id'];
		$p['text'] = $post['poll_question'];
		
		$p['is_active'] = '1';
		// unset($p);
		// print_r($p);
		$this->db->insert('tbl_poll', $p);

		$last_insert_id = $this->db->insert_id();
		
		$p['options'] = $post['arr_poll_options'];


		foreach ($p['options'] as $option) {
			$array = array(
				'poll_id' => $last_insert_id,
				'option_text'=> $option);
			$this->db->insert('tbl_poll_options', $array);
			// echo $option;
		}

		$this->db->set('is_active', '0');
		$this->db->where('poll_id !=', $last_insert_id);
		$this->db->update('tbl_poll');

		if ($this->db->affected_rows()>0) {
			$response['poll_added'] = true;
		}

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($response));
	}

	public function add_poll_choice()
	{
		print_r($_POST);
	}

	public function add_notice()
	{
		$class_id = $this->input->post('class_id');
		$notice = $this->input->post('notice');
		
		$arr_notice = array(
			'class_id' => $class_id,
			'notice' => $notice,
			'created_at' => date("Y-m-d H:i:s"));
		
		// print_r($arr_notice);
		$this->db->insert('tbl_notice', $arr_notice);
		if (empty($this->db->insert_id())) {
			$response['notice_added'] = false;
		}
		else{
			$response['notice_added'] = true;
		}

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($response));

	}

	public function delete_notice($notice_id)
	{
		$this->db->set('is_deleted', '1');
		$this->db->where('notice_id', $notice_id);
		$this->db->update('tbl_notice');

		if ($this->db->affected_rows() > 0) {
			$response['deleted'] = true;
		}
		else{
			$response['deleted'] = false;	
		}

		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($response));
	}
	

}

/* End of file stream.php */
/* Location: ./application/controllers/stream.php */

?>