<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends Common_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Mdl_users');
		$this->load->model('Mdl_institute');
		$this->load->model('Mdl_users');
	}

	public function index(){
		$this->session->set_userdata("is_logged_in", FALSE);
	}

	public function signup()
	{
		$this->load->model('Mdl_users');
		$form_data['states']  = $this->Mdl_users->get_states();

		$this->load->view('common/header');
		$this->load->view('registration',$form_data);
		$this->load->view('common/footer');
	}




	public function signin()
	{

		if ($_POST) {
			$form_data['signin']['username'] = $this->input->post('username');
			$form_data['signin']['password'] = $this->input->post('password');

			$this->load->model('Mdl_users');

			if($this->Mdl_users->is_login_valid($form_data['signin'])){

				$user_data   =   $this->Mdl_users->is_login_valid($form_data['signin']);
				$this->session->set_userdata("user_details", $user_data);
				$this->session->set_userdata("is_logged_in", TRUE);
				
				if(!empty($this->input->get('continue_url'))) {
					redirect($this->input->get('continue_url'));
				}
				else{
					redirect('dashboard');
				}
				
			}
			else{
				$this->session->set_userdata("is_logged_in", FALSE);
				redirect('users/signin');

			}
		}
		else
		{
			if (empty($this->session->userdata('user_details'))) {
				$this->load->view('common/header');
				$this->load->view('signin');
				$this->load->view('common/footer');
			}
			else{
				redirect('dashboard');
			}


		}

	}

	public function logout()
	{
		unset($_POST);
		
		$this->session->sess_destroy();
		redirect('','refresh'); 
    // $this->load->view('common/header');
    // $this->load->view('signin');
    // $this->load->view('common/footer');
	}


	public function is_registration_data_valid($form_data){

		$this->load->helper('form');
		$this->load->library('form_validation');

		$config = array(
			array(
				'field' => 'firstname',
				'label' => 'First Name',
				'rules' => 'required|regex_match[/^[a-zA-Z ]*$/]',
			),
			array(
				'field' => 'lastname',
				'label' => 'Last Name',
				'rules' => 'required|regex_match[/^[a-zA-Z ]*$/]',
			),
			array(
				'field' => 'email',
				'label' => 'Email',
				'rules' => 'required|valid_email',
			),

			array(
				'field' => 'birthdate',
				'label' => 'Birthdate',
				'rules' => 'required',
			),
			array(
				'field' => 'username',
				'label' => 'Username',
				'rules' => 'required|callback_form_validation_user_exists',
			),
			array(
				'field' => 'password',
				'label' => 'Password',
				'rules' => 'min_length[8]',
			)
		);

		$this->form_validation->set_rules($config);

		if ($this->form_validation->run() == FALSE) {
			echo $this->form_validation->error_string();
			return false;
		} else {
			return true;
		}

	}

	public function register_user(){
		$this->load->helper('form');
		$this->load->library('form_validation');


		$form_data['signup']['firstname']  = $this->input->post('firstname');
		$form_data['signup']['lastname']  = $this->input->post('lastname');

		$form_data['signup']['email']  = $this->input->post('email');
		$form_data['signup']['username']  = $this->input->post('username');
		$form_data['signup']['password']  = md5($this->input->post('password'));

		$form_data['signup']['gender']  = $this->input->post('gender');

		$dateparts = explode('-', $this->input->post('birthdate') );
		$form_data['signup']['birthdate'] = $dateparts[2]."-".$dateparts[1]."-".$dateparts[0];
		$form_data['signup']['created_at'] = date("Y-m-d H:i:s");


		if ($this->is_registration_data_valid($form_data)) {

			if($this->Mdl_users->db_register_user($form_data)){
				$user_details = $form_data['signup'];

				$this->session->set_userdata("user_details",$user_details);
				$this->session->set_userdata("is_logged_in", TRUE);
				redirect('dashboard');	
			}

		}




	}


	public function user_exists(){

		$username= $this->input->post('username');

		$this->load->model('Mdl_users');
		$data['user_exists'] = $this->Mdl_users->user_exists($username);

		if ($data['user_exists'] == TRUE) {
			echo json_encode(FALSE);
			return false;
		}
		else{
			echo json_encode(TRUE);
			return true;
		}
	}

	public function form_validation_user_exists(){

		$username= $this->input->post('username');

		$this->load->model('Mdl_users');
		$data['user_exists'] = $this->Mdl_users->user_exists($username);

		if ($data['user_exists'] == TRUE) {
			$this->form_validation->set_message('form_validation_user_exists','Username already exists');

			return false;
		}
		else{

			return true;
		}
	}

	public function email_exists(){

		$email= $this->input->post('email');

		$this->load->model('Mdl_users');
		$data['email_exists'] = $this->Mdl_users->email_exists($email);

		if ($data['email_exists'] == TRUE) {
			$this->form_validation->set_message('email_exists','Email already exists');

			echo json_encode(FALSE);
			return false;
		}
		else{
			echo json_encode(TRUE);
			return true;
		}
	}



	public function get_cities($state_id){
		$this->load->model('Mdl_users');
		$data['cities'] = $this->Mdl_users->get_cities($state_id);
		$this->load->view('extras/ajax_get_cities', $data);
	}

	public function request_status()
	{
		$user_id = $this->Mdl_users->get_current_user_id();
		$role_id = $this->input->post('role_id');
		$institute_code = $this->input->post('institute_code');

		$status = $this->Mdl_users->get_request_status($user_id, $role_id, $institute_code);

		if ($status !== false) {
			if ($status == 0) {
				echo "pending";
			}
			elseif ($status == 1) {
				echo "accepted";
			}
			elseif ($status == 2) {
				echo "declined";
			}
			elseif ($status == 3) {
				echo "blocked";
			}	
			else{}
		}
	else{
		echo json_encode(false);
	}

}

	public function requests()
	{
	$data['requests'] = $this->Mdl_users->list_requests($this->Mdl_users->get_current_user_id());

	

	}

public function accept_request()
{
		$user_id = $this->input->post('user_id'); // requestor
		$rows_affected = $this->Mdl_users->accept_request($user_id);

		$user_details = $this->Mdl_users->get_user_by_id($user_id);
		
		$ssn_inst_join = $this->session->userdata('ssn_inst_join');
		$institute_code = $ssn_inst_join['institute_code'];
		
		// $this->db->select();
		$this->db->from('tbl_relation');
		$this->db->where('institute_code', $institute_code);
		$this->db->where('request_from_id', $user_id);
		$req_role_id = $this->db->get()->row()->as_role_id;
		// echo $this->db->last_query();;

		$object = array(
			'user_id' => $user_id,
			'role_id' => $req_role_id,
			'institute_code' => $institute_code);
		// echo "<script>console.log('ss". json_encode($object) ."');</script>";

		$this->db->from('tbl_user_inst');
		$this->db->where('user_id', $user_id);
		$this->db->where('role_id', $req_role_id);
		$this->db->where('institute_code', $institute_code);
		$rs  = $this->db->get()->row();
		
		if (empty($rs)) {
			$this->db->insert('tbl_user_inst', $object);     
		}
		
		echo json_encode(($rows_affected > 0) ? true : false);
	}




}

?>
