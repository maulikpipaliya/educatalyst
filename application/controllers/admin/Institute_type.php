<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Institute_type extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/mdl_institute');
		if(!$this->session->userdata("admin_details"))
		{
			redirect('admin/signin');
		}
	}
	
	public function index()
	{
		$header_data['menu'] = "institute";
		$header_data['submenu_level1'] = "institute_type";
		
		$data['institute_types'] = $this->mdl_institute->get_institute_types();
		$this->load->view('admin/common/header', $header_data, FALSE);
		$this->load->view('admin/institute_types', $data, FALSE);
		$this->load->view('admin/common/footer');
	}

	public function add()
	{
		//print_r($_POST);die;
		if(!$this->input->post())
		{
			$header_data['menu'] = "institute";
			$header_data['submenu_level1'] = "institute_type";
			$this->load->view('admin/common/header', $header_data, FALSE);
			$this->load->view('admin/add_edit_institute_type');	
			$this->load->view('admin/common/footer');		
		}
		else
		{
			$input_data['institute_type'] = $this->input->post("institute_type", TRUE);
			
			//Check name already exist
			$arr = $input_data;
			$arr['is_del'] = "0";
			$exist = $this->db->get_where("tbl_institute_types", $arr)->row();
			if(empty($exist))
			{
				$this->db->insert("tbl_institute_types", $input_data);
				$this->session->set_flashdata('resonse_msg', 'Institute type added successfully.');
				$this->session->set_flashdata('response', "success");
				redirect('admin/institute_type');
			}
			else
			{
				$this->session->set_flashdata('resonse_msg', 'Institute type with same name already exist.');
				$this->session->set_flashdata('response', "danger");
				redirect('admin/institute_type');
			}
		}
	}

	public function edit($institute_type_id)
	{
		//print_r($_POST);die;
		if(!$this->input->post())
		{
			$header_data['menu'] = "institute";
			$header_data['submenu_level1'] = "institute_type";

			$data['institute_type'] = $this->db->get_where("tbl_institute_types", array("institute_type_id" => $institute_type_id))->row();
			$this->load->view('admin/common/header', $header_data, FALSE);
			$this->load->view('admin/add_edit_institute_type', $data);			
			$this->load->view('admin/common/footer');
		}
		else
		{
			$input_data['institute_type'] = $this->input->post("institute_type", TRUE);

			//Check name already exist
			$arr = $input_data;
			$arr['is_del'] = "0";
			$arr['institute_type_id != '] = $institute_type_id;

			$exist = $this->db->get_where("tbl_institute_types", $arr)->row();
			if(empty($exist))
			{
				$this->db->where("institute_type_id", $institute_type_id);
				$this->db->update("tbl_institute_types", $input_data);
				$this->session->set_flashdata('resonse_msg', 'Institute type updated successfully.');
				$this->session->set_flashdata('response', "success");
				redirect('admin/institute_type');
			}
			else
			{
				$this->session->set_flashdata('resonse_msg', 'Institute type with same name already exist.');
				$this->session->set_flashdata('response', "danger");
				redirect('admin/institute_type');
			}
		}
	}

	public function delete($institute_type_id)
	{
		$this->db->where("institute_type_id", $institute_type_id);
		$this->db->update("tbl_institute_types", array("is_del" => "1"));
		if($this->db->affected_rows() > 0)
		{
			$this->session->set_flashdata('resonse_msg', 'Institute type deleted successfully.');
			$this->session->set_flashdata('response', "success");	
		}
		else
		{
			$this->session->set_flashdata('resonse_msg', 'Something went wrong! Plesae try again later.');
			$this->session->set_flashdata('response', "danger");	
		}
		redirect('admin/institute_type');
	}
}

/* End of file institute_type.php */
/* Location: ./application/controllers/admin/institute_type.php */