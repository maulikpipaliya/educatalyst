<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/mdl_user');
		if(!$this->session->userdata("admin_details"))
		{
			redirect('admin/signin');
		}
	}
	
	public function index()
	{
		$header_data['menu'] = "user";
		
		$data['users'] = $this->mdl_user->get_all();				
		$this->load->view('admin/common/header', $header_data, FALSE);
		$this->load->view('admin/user', $data, FALSE);
		$this->load->view('admin/common/footer', $data, FALSE);
	}

	public function details($user_id)
	{
		$header_data['menu'] = "user";
		$header_data['submenu_level1'] = "all_users";
		
		$data['user'] = $this->mdl_user->get($user_id);
		$data['user_joinings'] = $this->mdl_user->get_user_joinings($user_id, true);
		
		$this->load->view('admin/common/header', $header_data, FALSE);
		$this->load->view('admin/user_details', $data, FALSE);
		$this->load->view('admin/common/footer', $data, FALSE);
	}

	public function block()
	{
		$user_id = $this->input->post('id');
		$action = $this->input->post('action');

		$this->mdl_user->block($user_id, $action);
		
		$response['action'] = abs($action - 1);
		$response['btn_text'] = ($action == 1)?"Unblock":"Block";
		$response['add_class'] = ($action == 1)?"btn-success":"btn-danger";
		$response['remove_class'] = ($action == 0)?"btn-success":"btn-danger";

		$this->output->set_content_type('application/json')->set_output(json_encode($response));
	}

}

/* End of file User.php */
/* Location: ./application/controllers/admin/User.php */