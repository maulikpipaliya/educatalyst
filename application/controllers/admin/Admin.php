<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/Mdl_admin','admin_model');
	}
	public function index()
	{
		$data=null;
		redirect('admin/signin');

	}

	public function signin()
	{
		$data = null;

		if ($_POST) 
		{
			$form_data['signin']['username'] = $this->input->post('username');
			$form_data['signin']['password'] = $this->input->post('password');

			$admin_details = $this->admin_model->validate_login($form_data['signin']);
						
			if(!empty($admin_details)){
	
				$this->session->set_userdata("admin_details", $admin_details);
				$this->session->set_userdata("is_logged_in", TRUE);
				
				if(!empty($this->input->get('continue_url'))) {
					redirect($this->input->get('continue_url'));
				}
				else{
					redirect('admin/dashboard');
				}
				
			}
			else{
				$this->session->set_userdata("is_logged_in", FALSE);
				redirect('admin/signin');
			}
		}
		else
		{
			if (empty($this->session->userdata('admin_details'))) {
				$this->load->view('admin/signin');
			}
			else{
				redirect('admin/dashboard');
			}
		}
		
	}

	public function signout()
	{
		$this->session->sess_destroy();
		redirect('admin/signin');
	}

	public function dashboard()
	{
		if(!$this->session->userdata("admin_details"))
		{
			redirect('admin/signin');
		}
		$data = null;
		$data['total_institutes'] = $this->db->get_where("tbl_institute", array("is_deleted" => NULL))->num_rows();
		$data['total_classes'] = $this->db->get_where("tbl_class", array("is_folder" => "0"))->num_rows();
		
		$this->load->view('admin/common/header', $data, FALSE);
		$this->load->view('admin/dashboard', $data, FALSE);
		$this->load->view('admin/common/footer', $data, FALSE);
		
	}

}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */
