<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class State extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/mdl_state_city');
		if(!$this->session->userdata("admin_details"))
		{
			redirect('admin/signin');
		}
	}
	
	public function index()
	{
		$header_data['menu'] = "state_city";
		$header_data['submenu_level1'] = "state";
		
		$data['states'] = $this->mdl_state_city->get_all_states();
		$this->load->view('admin/common/header', $header_data, FALSE);
		$this->load->view('admin/states', $data, FALSE);
		$this->load->view('admin/common/footer');
	}

	public function add()
	{
		//print_r($_POST);die;
		if(!$this->input->post())
		{
			$header_data['menu'] = "state_city";
			$header_data['submenu_level1'] = "state";
			$this->load->view('admin/common/header', $header_data, FALSE);
			$this->load->view('admin/add_edit_state');	
			$this->load->view('admin/common/footer');		
		}
		else
		{
			$input_data['name'] = $this->input->post("name", TRUE);
			$input_data['country_id'] = $this->input->post("country_id", TRUE);
			$input_data['code'] = $this->input->post("code", TRUE);
			//Check name already exist
			$arr = $input_data;
			$arr['is_del'] = "0";
			$exist = $this->db->get_where("states", $arr)->row();
			if(empty($exist))
			{
				$this->db->insert("states", $input_data);
				$this->session->set_flashdata('resonse_msg', 'State added successfully.');
				$this->session->set_flashdata('response', "success");
				redirect('admin/state');
			}
			else
			{
				$this->session->set_flashdata('resonse_msg', 'State with same name & code already exist.');
				$this->session->set_flashdata('response', "danger");
				redirect('admin/state');
			}
		}
	}

	public function edit($state_id)
	{
		//print_r($_POST);die;
		if(!$this->input->post())
		{
			$header_data['menu'] = "state_city";
			$header_data['submenu_level1'] = "state";

			$data['state'] = $this->db->get_where("states", array("id" => $state_id))->row();
			$this->load->view('admin/common/header', $header_data, FALSE);
			$this->load->view('admin/add_edit_state', $data);			
			$this->load->view('admin/common/footer');
		}
		else
		{
			$input_data['name'] = $this->input->post("name", TRUE);
			$input_data['country_id'] = $this->input->post("country_id", TRUE);
			$input_data['code'] = $this->input->post("code", TRUE);
			//Check name already exist
			$arr = $input_data;
			$arr['is_del'] = "0";
			$arr['id != '] = $state_id;

			$exist = $this->db->get_where("states", $arr)->row();
			if(empty($exist))
			{
				$this->db->where("id", $state_id);
				$this->db->update("states", $input_data);
				$this->session->set_flashdata('resonse_msg', 'State updated successfully.');
				$this->session->set_flashdata('response', "success");
				redirect('admin/state');
			}
			else
			{
				$this->session->set_flashdata('resonse_msg', 'State with same name & code already exist.');
				$this->session->set_flashdata('response', "danger");
				redirect('admin/state');
			}
		}
	}

	public function delete($state_id)
	{
		$this->db->where("id", $state_id);
		$this->db->update("states", array("is_del" => "1"));
		if($this->db->affected_rows() > 0)
		{
			$this->session->set_flashdata('resonse_msg', 'State deleted successfully.');
			$this->session->set_flashdata('response', "success");	
		}
		else
		{
			$this->session->set_flashdata('resonse_msg', 'Something went wrong! Plesae try again later.');
			$this->session->set_flashdata('response', "danger");	
		}
		redirect('admin/state');
	}
}

/* End of file State.php */
/* Location: ./application/controllers/admin/State.php */