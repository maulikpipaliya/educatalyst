<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Institute extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/mdl_institute');
		if(!$this->session->userdata("admin_details"))
		{
			redirect('admin/signin');
		}
	}
	
	public function index()
	{
		$header_data['menu'] = "institute";
		$header_data['submenu_level1'] = "all_institutes";
		
		$data['institutes'] = $this->mdl_institute->get_all();		
		$this->load->view('admin/common/header', $header_data, FALSE);
		$this->load->view('admin/institute', $data, FALSE);
		$this->load->view('admin/common/footer', $data, FALSE);
	}

	public function details($institute_id)
	{
		$header_data['menu'] = "institute";
		$header_data['submenu_level1'] = "all_institutes";
		
		$data['institute'] = $this->mdl_institute->get($institute_id);
		$data['institute_users'] = $this->mdl_institute->get_institute_users($data['institute']->institute_code, true);
		$data['institute_classes'] = $this->mdl_institute->get_institute_classes($data['institute']->institute_code, true);

		$this->load->view('admin/common/header', $header_data, FALSE);
		$this->load->view('admin/institute_details', $data, FALSE);
		$this->load->view('admin/common/footer', $data, FALSE);
	}

	public function block()
	{
		$institute_id = $this->input->post('id');
		$action = $this->input->post('action');

		$this->mdl_institute->block($institute_id, $action);
		
		$response['action'] = abs($action - 1);
		$response['btn_text'] = ($action == 1)?"Unblock":"Block";
		$response['add_class'] = ($action == 1)?"btn-success":"btn-danger";
		$response['remove_class'] = ($action == 0)?"btn-success":"btn-danger";

		$this->output->set_content_type('application/json')->set_output(json_encode($response));
	}

}

/* End of file Institute.php */
/* Location: ./application/controllers/admin/Institute.php */