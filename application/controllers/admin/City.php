<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class City extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/mdl_state_city');
		if(!$this->session->userdata("admin_details"))
		{
			redirect('admin/signin');
		}
	}
	
	public function index()
	{
		$header_data['menu'] = "state_city";
		$header_data['submenu_level1'] = "city";
		
		$data['cities'] = $this->mdl_state_city->get_all_cities();
		$this->load->view('admin/common/header', $header_data, FALSE);
		$this->load->view('admin/cities', $data, FALSE);
		$this->load->view('admin/common/footer');
	}

	public function add()
	{
		//print_r($_POST);die;
		if(!$this->input->post())
		{
			$header_data['menu'] = "state_city";
			$header_data['submenu_level1'] = "city";

			$data['states'] = $this->mdl_state_city->get_all_states();

			$this->load->view('admin/common/header', $header_data, FALSE);
			$this->load->view('admin/add_edit_city', $data);	
			$this->load->view('admin/common/footer');		
		}
		else
		{
			$input_data['name'] = $this->input->post("name", TRUE);
			$input_data['state_id'] = $this->input->post("state_id", TRUE);

			//Check name already exist
			$arr = $input_data;
			$arr['is_del'] = "0";
			$exist = $this->db->get_where("cities", $arr)->row();
			if(empty($exist))
			{
				$this->db->insert("cities", $input_data);
				$this->session->set_flashdata('resonse_msg', 'City added successfully.');
				$this->session->set_flashdata('response', "success");
				redirect('admin/city');
			}
			else
			{
				$this->session->set_flashdata('resonse_msg', 'City with same name already exist.');
				$this->session->set_flashdata('response', "danger");
				redirect('admin/city');
			}
		}
	}

	public function edit($city_id)
	{
		//print_r($_POST);die;
		if(!$this->input->post())
		{
			$header_data['menu'] = "state_city";
			$header_data['submenu_level1'] = "city";

			$data['states'] = $this->mdl_state_city->get_all_states();
			$data['city'] = $this->db->get_where("cities", array("id" => $city_id))->row();
			$this->load->view('admin/common/header', $header_data, FALSE);
			$this->load->view('admin/add_edit_city', $data);			
			$this->load->view('admin/common/footer');
		}
		else
		{
			$input_data['name'] = $this->input->post("name", TRUE);
			$input_data['state_id'] = $this->input->post("state_id", TRUE);

			//Check name already exist
			$arr = $input_data;
			$arr['is_del'] = "0";
			$arr['id != '] = $city_id;

			$exist = $this->db->get_where("cities", $arr)->row();
			if(empty($exist))
			{
				$this->db->where("id", $city_id);
				$this->db->update("cities", $input_data);
				$this->session->set_flashdata('resonse_msg', 'City updated successfully.');
				$this->session->set_flashdata('response', "success");
				redirect('admin/city');
			}
			else
			{
				$this->session->set_flashdata('resonse_msg', 'City with same name already exist.');
				$this->session->set_flashdata('response', "danger");
				redirect('admin/city');
			}
		}
	}

	public function delete($city_id)
	{
		$this->db->where("id", $city_id);
		$this->db->update("cities", array("is_del" => "1"));
		if($this->db->affected_rows() > 0)
		{
			$this->session->set_flashdata('resonse_msg', 'City deleted successfully.');
			$this->session->set_flashdata('response', "success");	
		}
		else
		{
			$this->session->set_flashdata('resonse_msg', 'Something went wrong! Plesae try again later.');
			$this->session->set_flashdata('response', "danger");	
		}
		redirect('admin/city');
	}
}

/* End of file City.php */
/* Location: ./application/controllers/admin/City.php */