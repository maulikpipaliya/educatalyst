<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Help extends CI_Controller {

	public function index()
	{
		$data = null;
		$this->load->view('common/header', $data, FALSE);
		$this->load->view('help', $data, FALSE);
		$this->load->view('common/footer', $data, FALSE);
	}

}

/* End of file Help.php */
/* Location: ./application/controllers/Help.php */