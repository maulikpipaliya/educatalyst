<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
	}

	public function index()
	{
		$user_details	= $this->session->userdata('user_details');
		$data['user_details'] = $this->session->userdata('user_details');
		$data['is_logged_in'] = $this->session->userdata('is_logged_in');
		
		if ($user_details['username']=="admin") {
			$this->load->view('admin/common/header');
			$this->load->view('admin/dashboard');
			$this->load->view('admin/common/footer');
				
		}
		else{
			// redirect('institute/select_institute');
			$this->load->view('common/header');
			$this->load->view('dashboard',$data);
			$this->load->view('common/footer');
		}
		
		
	}
}

/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */
