<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Requests extends Common_controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Mdl_users');
		$this->load->model('Mdl_request');
	}
	public function index()
	{
		$user_id = $this->Mdl_users->get_current_user_id();
		$data['requests'] = $this->Mdl_request->list_requests_of($user_id);

		$this->console_log_table($data['requests'],"List of Requests");
		
		$this->load->view('common/header');
		$this->load->view('requests',$data);
		$this->load->view('common/footer');
	}

	public function get_request_status()
	{
		$user_id = "66";
		$code = "GJ783_0010422";
		
	}

	public function accept_request()
	{
		$request_id = $this->input->post('request_id');	
		
		if (!empty($request_id)) {
			$rows_affected = $this->Mdl_request->accept_request($request_id);	
		}
		
		echo json_encode(($rows_affected > 0) ? true : false);
			// $ssn_inst_join = $this->session->userdata('ssn_inst_join');
		
		/* ------------------------------------------------------------------------
	
			institute_code management remains	
			------------------------------------------------------------------------*/


		// $user_id = $this->input->post('user_id'); // requestor
		// $as_role_id = $this->input->post('as_role_id');
		// $institute_code = $this->input->post('institute_code');


		// $this->db->select('request_id');
		// $this->db->from('tbl_request');
		// $this->db->where('request_from_id', $user_id);
		// $this->db->where('as_role_id', $as_role_id);
		// $this->db->where('institute_code', $institute_code);
		// $row = $this->db->get()->row();
		// echo "<script>console.log('". $user_id ."');</script>";
		// $rows_affected = $this->Mdl_users->accept_request($user_id);

		// $user_details = $this->Mdl_users->get_user_by_id($user_id);

		// $ssn_inst_join = $this->session->userdata('ssn_inst_join');
		// $institute_code = $ssn_inst_join['institute_code'];

		// // $this->db->select();
		// $this->db->from('tbl_relation');
		// $this->db->where('institute_code', $institute_code);
		// $this->db->where('request_from_id', $user_id);
		// $req_role_id = $this->db->get()->row()->as_role_id;
		// // echo $this->db->last_query();;

		// $object = array(
		// 	'user_id' => $user_id,
		// 	'role_id' => $req_role_id,
		// 	'institute_code' => $institute_code);
		// // echo "<script>console.log('ss". json_encode($object) ."');</script>";

		// $this->db->from('tbl_user_inst');
		// $this->db->where('user_id', $user_id);
		// $this->db->where('role_id', $req_role_id);
		// $this->db->where('institute_code', $institute_code);
		// $rs  = $this->db->get()->row();

		// if (empty($rs)) {
		// 	$this->db->insert('tbl_user_inst', $object);     
		// }




		}



	}

	/* End of file Requests.php */
/* Location: ./application/controllers/Requests.php */