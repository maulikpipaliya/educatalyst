<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Classes extends Common_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Mdl_class');
		$this->load->model('Mdl_users');
		$this->load->helper('download');
	}

	public function home()
	{
		$data=null;
		$user_id = $this->get_current_user_id();
		$data['classes_joined'] = $this->Mdl_class->list_classes_joined($user_id);
		$ssn_inst_join = $this->session->userdata('ssn_inst_join');
		if ($ssn_inst_join['role_id'] == 1) {
			// echo "<pre>";
			// print_r($data['classes_joined']);	
			$this->load->view('stream/common/header', $data, FALSE);	
			$this->load->view('classes/student_classes', $data, FALSE);	
			$this->load->view('stream/common/footer', $data, FALSE);		
		}
		else if ($ssn_inst_join['role_id'] == 2) { // Teacher

			if ($this->Mdl_institute->is_valid($ssn_inst_join['role_id'], $this->Mdl_users->get_current_user_id(), $ssn_inst_join['institute_code'])) {
				redirect('classes/me');
			}
			
		}
		else if ($ssn_inst_join['role_id'] == 3) {
			redirect('institute/head');
		}
		
		
	}

	public function students()
	{
		$ssn_class_details = $this->session->userdata('ssn_class_details');
		$data['students'] = $this->Mdl_class->list_students($ssn_class_details['class_code']);
		
		$students = $data['students'];
		$student_details = array();
		if (!empty($students)) {
			foreach ($students as $student) {
				$student_details[] = $this->Mdl_users->get_user_by_id($student['user_id']);
			}
		}
		

		$data['student_details'] = $student_details;

		$this->load->view('stream/common/header', $data, FALSE);	
		$this->load->view('stream/students', $data, FALSE);	
		$this->load->view('stream/common/footer', $data, FALSE);		
	}

	public function stream($class_id)
	{
		$data['this_class'] = $this->get_class_row($class_id);

		$array_class_details = $data['this_class'];
		$this->session->set_userdata("ssn_class_details" , $array_class_details);


		$ssn_inst_join = $this->session->userdata('ssn_inst_join');

		if ($ssn_inst_join['role_id'] == 1) {
			$this->load->view('stream/common/header', $data, FALSE);	
			$this->load->view('stream/home_student', $data, FALSE);	
			$this->load->view('stream/common/footer', $data, FALSE);	
			
		}
		else if ($ssn_inst_join['role_id'] == 2) {
			$this->load->view('stream/common/header', $data, FALSE);	
			$this->load->view('stream/home_teacher', $data, FALSE);	
			$this->load->view('stream/common/footer', $data, FALSE);	
		}
	}


	public function index()
	{
		$continue_url = urlencode("classes/	my_classes");
		if (!($this->session->userdata('is_logged_in'))) {
			redirect('users/signin?continue_url='.$continue_url);
		}

		if (!($this->session->userdata('ssn_inst_join'))) {
			redirect('institute/select_institute');
		}

		$parent_id = ($_GET) ? $this->input->get('parent_id') : null ;
		redirect('classes/me/'.$parent_id);
	}

	public function create_session()
	{
		$response  = array();
		$session_name = $this->input->post('session_name');
		$session_desc = $this->input->post('session_desc');
		$class_id   = $this->input->post('class_id');
		

		$array_add_session = array(
			'session_name' => $session_name,
			'session_desc' => $session_desc,
			'class_id' => $class_id,
			'created_at' => date("Y-m-d H:i:s")
		);


		$this->db->insert('tbl_session', $array_add_session);
		$affected_rows = $this->db->affected_rows();

			// echo $this->db->last_query();
		
		$response['session_created'] = ($affected_rows > 0) ? 1 : 0 ;

		if ($response['session_created'] == 1) {
			redirect('classes/session/'. $this->db->insert_id());
		}		
		// $this->output
		// 	 ->set_content_type('application/json')
		// 	 ->set_output(json_encode($response));
	}

	public function post_updater()
	{
		$response = array();
		
		$data['post_text'] = $this->input->post('post_text');

		$array_post['user_id'] = $this->get_current_user_id();
		$array_post['post_content'] = $this->input->post('post_text');
		
		
		$rs = $this->db->where($array_post)->get('tbl_post')->row_array();
		$rs = (!empty($rs)) ? $rs : null ;
		
		if (!($rs)) {
			$array_post['created_at'] = date("Y-m-d H:i:s");
			$this->db->insert('tbl_post', $array_post);	
		}


		
		// $this->load->view('stream/posts', $data, FALSE);
		
	}

	public function session($session_id)
	{
		$data['this_session'] = $this->get_session_row($session_id);
		$array_session_details = $data['this_session'];


		$this->session->set_userdata("ssn_session_details" , $array_session_details);
		

		$data['this_class'] = $this->get_class_row($data['this_session']['class_id']);
		$array_class_details = $data['this_class'];
		$this->session->set_userdata("ssn_class_details" , $array_class_details);

		$this->load->view('stream/common/header', $data, FALSE);	
		$this->load->view('stream/home', $data, FALSE);	
		$this->load->view('stream/common/footer', $data, FALSE);	

	}

	

	public function me($parent_id = null)
	{
		$continue_url = urlencode("classes/my_classes");
		if (!($this->session->userdata('is_logged_in'))) {
			redirect('users/signin?continue_url='.$continue_url);
		}
		
		$user_id = $this->get_current_user_id();
		
		$data['parent_id'] = $parent_id;
		
		$this->load->view('common/header', $data, FALSE);
		$this->load->view('classes/my_classes', $data, FALSE);
		$this->load->view('common/footer', $data, FALSE);
	}

	public function change_class()
	{
		$this->session->set_userdata("ssn_class_details", null);
		redirect('classes/me');
	}
	public function my_classes($parent_id = null)
	{
		$continue_url = urlencode("classes/my_classes");
		if (!($this->session->userdata('is_logged_in'))) {
			redirect('users/signin?continue_url='.$continue_url);
		}

		
		$user_id = $this->get_current_user_id();
		// $parent_id = $this->input->post('parent_id');	
		
		
		
		$data['classes'] = $this->Mdl_class->list_classes($user_id,$parent_id);
		
		// $this->console_log_table($data['classes'],"Classes");

		$this->load->view('common/header', $data, FALSE);
		$this->load->view('classes/my_classes', $data, FALSE);
		$this->load->view('common/footer', $data, FALSE);
	}




	public function load_classes($parent_id = null)
	{
		$user_id = $this->get_current_user_id();
		$data['classes'] = $this->Mdl_class->list_classes($user_id,$parent_id);
		$this->load->view('institute/extra/load_class_names', $data, FALSE);
	}

	public function new_class()
	{
		$response  = array();
		$class_name = $this->input->post('class_name');
		$class_desc = $this->input->post('class_desc');
		$is_folder = ($this->input->post('type_choice') === "folder") ? "1" : "0" ;
		$parent_id = $this->input->post('parent_id');
		$creator_id = $this->get_current_user_id();
		
		$ssn_inst_join = $this->session->userdata('ssn_inst_join');
		$institute_code = $ssn_inst_join['institute_code'];

		$class_code = $this->generate_class_code();
		$level = ($parent_id) ? "1" : "0" ;
		
		$array_add_class = array(
			'class_name' => $class_name,
			'class_desc' => $class_desc,
			'creator_id' => $creator_id,
			'class_code' => $class_code,
			'parent_id' => $parent_id,
			'level' => $level,
			'institute_code' => $institute_code,
			'is_folder' => $is_folder);
		 // $this->console_log_table($array_add_class,"Array");
		 // $this->console_log_table($_POST,"POST");

		// $this->console_log_table($this->session->userdata('ssn_inst_join'),"Session");
		
		$this->db->insert('tbl_class', $array_add_class);
		
		$affected_rows = $this->db->affected_rows();
		
		
		if ($affected_rows > 0) {

			$last_id = $this->db->insert_id();	
			$class_id = $last_id;

			$array_add_code = $this->get_class_row($class_id);

			$arr = array(
				'user_id' => $array_add_code['creator_id'],
				'class_id' => $class_id,
				'institute_code' => $institute_code,
				'code' => $class_code,
				'code_type' => "1" ); // 1: code for students

			$this->db->insert('tbl_code', $arr); 
			
			if ($is_folder == "0") { // is_class = true
				//echo $last_id;
				$response['is_folder'] = 0;
				$response['data'] = $last_id;
				// redirect('classes/stream/15');	
			}
			else{
				$response['is_folder'] = 1;
				$response['data'] = "";
				//echo "is_class";
			}
			// echo $class_id = $this->db->where($array_add_class)->get('tbl_class')->row()->class_id;
		}
		else{
			$response['is_folder'] = -1;
			$response['data'] = "";
		}
		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($response));
	}


	public function materials($class_id)
	{
		$data['class_id'] = $class_id;
		$this->load->view('stream/common/header', $data, FALSE);	
		$this->load->view('stream/materials', $data, FALSE);
		$this->load->view('stream/common/footer', $data, FALSE);	
	}


	public function add_material()
	{
		$this->load->helper(array('form', 'url'));

		$config['upload_path']          = 'uploads/';
		$config['allowed_types']  = 'gif|jpeg|jpg|png|pdf|txt|ppt|pptx|xls|xlsx|doc|docx|odt|ods';
		$config['file_name'] = $this->Mdl_class->get_current_class_id()."_".substr(str_shuffle(MD5(microtime())), 0, 15);

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('file'))
		{
			$error = array('error' => $this->upload->display_errors());
			$response['error'] = $error['error'];
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());	
			$arr_add_file_db = array(
				'class_id' => $this->Mdl_class->get_current_class_id(),
				'url' => "uploads/".$data['upload_data']['file_name'],
				'alias' => $data['upload_data']['client_name'],
				'date_added' => date("Y-m-d H:i:s")
			);


			$this->db->insert('tbl_upload', $arr_add_file_db);
			$response['message'] = "uploaded";

		}

		print_r($data);print_r(@$error);
		echo "<script>alert('".hahah."');</script>";

		
	}
	

	public function load_files()
	{
		$response = array();
		$class_id = $this->input->post('class_id');

		$this->db->where('class_id', $class_id);
		$data['files'] = $this->db->get('tbl_upload')->result_array();

		$this->load->view('stream/load_uploads', $data, FALSE);

	}

	public function do_download($file_id,$file_name = "ajaj")
	{
		$this->db->where('id', $file_id);
		$file = $this->db->get('tbl_upload', 1)->row();

		$filename = $file->alias;
		$data = file_get_contents($file->url);
		$set_mime = TRUE;

		// echo mime_content_type($file->url);
		$this->output
		->set_content_type(mime_content_type($file->url))
		->set_output($data);
		// force_download($filename, $data, $set_mime);
		
		

	}
}

/* End of file Classes.php */
/* Location: ./application/controllers/Classes.php */
