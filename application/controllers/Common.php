<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Mdl_users');
	}
	public function index()
	{

		$form_data['states']  = $this->Mdl_users->get_states();
	}

	public function get_states(){
		$this->db->select('*');
		$query  = $this->db->get('states');
		return $query->result_arr_teacher_code();   
	}


	public function get_cities($state_id){
		$this->load->model('Mdl_users');
		$data['cities'] = $this->Mdl_users->get_cities($state_id);
		$this->load->view('extras/ajax_get_cities', $data);
	}

	public function get_state_code($state_id){
		$this->load->model('Mdl_common');
		$code = $this->Mdl_common->get_state_code($state_id);
		// $this->load->view('extras/ajax_get_state_code', $data);
		echo $code['code'];
	}

	public function get_city_code($city_id){
		$this->load->model('Mdl_common');
		$code = $this->Mdl_common->get_city_code($city_id);
		// $this->load->view('extras/ajax_get_state_code', $data);
		echo $code['code'];
	}
	
	function doStuff() {
		$has_run = false;
		
		if (!($this->session->userdata('has_run'))) {
			echo "has_run";
			$this->session->set_userdata("has_run" , true );
		}
		else {
			echo "run already";
		}

  // code using $cache
	}


	public function generate_student_code()
	{
		$executed_already = false;
		//uncomment this line when code doesnt generate
		// $this->session->set_userdata("executed_already",false);

		$ssn_inst_join = $this->session->userdata("ssn_inst_join");
		
		$institute_code = $ssn_inst_join['institute_code'];
		$user_id = $this->Mdl_users->get_current_user_id();

		$array_to_insert = array(
			'user_id' => $user_id,
			'institute_code' => $institute_code,
			// 'code' => $ssn_teacher_code,
			'code_type' => '1' ); //1:Student Code

		$this->db->from('tbl_code');
		$this->db->where($array_to_insert);
		$row = $this->db->get()->row();

		if (empty($row)) {

			if (!($this->session->userdata('executed_already'))) {
				$random = rand(10000,99999);
				$student_code = $institute_code.$random;	
				
				$this->session->set_userdata("ssn_student_code",$student_code );
				$ssn_student_code = $this->session->userdata('ssn_student_code');	
				$this->session->set_userdata( "executed_already", true );
			}
			
			$array_to_insert['code'] = $ssn_student_code;
			$this->db->insert('tbl_code', $array_to_insert);
			echo $ssn_student_code = $this->session->userdata('ssn_student_code');
		}
		else{
			echo $row->code;	
		}
	}	
	
	public function generate_teacher_code()
	{
		$executed_already = false;
		$ssn_inst_join = $this->session->userdata("ssn_inst_join");
		
		$institute_code = $ssn_inst_join['institute_code'];
		$user_id = $this->Mdl_users->get_current_user_id();

		$array_to_insert = array(
			'user_id' => $user_id,
			'institute_code' => $institute_code,
			// 'code' => $ssn_teacher_code,
			'code_type' => '0' );

		$this->db->from('tbl_code');
		$this->db->where($array_to_insert);
		$row = $this->db->get()->row();

		if (empty($row)) {

			if (!($this->session->userdata('executed_already'))) {
				$random = rand(100,999);
				$teacher_code = $institute_code.$random;	
				
				$this->session->set_userdata("ssn_teacher_code",$teacher_code );
				$ssn_teacher_code = $this->session->userdata('ssn_teacher_code');	
				$this->session->set_userdata( "executed_already", true );
			}
			
			$array_to_insert['code'] = $ssn_teacher_code;
			$this->db->insert('tbl_code', $array_to_insert);
			echo $ssn_teacher_code = $this->session->userdata('ssn_teacher_code');
		}
		else{
			echo $row->code;	
		}
	}

	
}

/* End of file Common.php */
/* Location: ./application/controllers/Common.php */