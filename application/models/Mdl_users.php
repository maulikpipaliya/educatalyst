<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdl_users extends CI_Model {


    public function __construct()
    {
        parent::__construct();
        
    }

    public function get_user_by_id($user_id)
    {
        $this->db->select('*,CONCAT(firstname, " " ,lastname) as FullName');
        $this->db->from('tbl_user');
        $this->db->where('user_id', $user_id);
        return $this->db->get()->row_array();
    }

    public function is_login_valid($form_data)
    {
        $this->db->from('tbl_user');
        $this->db->group_start();
        $this->db->where('username', $form_data['username']);
        $this->db->or_where('email', $form_data['username']);
        $this->db->group_end();
        $this->db->where('password', md5($form_data['password']));
        $row = $this->db->get()->row_array();
        if (!empty($row)) {
            return $row;
        }
        return false;

    }

    public function db_register_user($form_data)
    {
        return $this->db->insert('tbl_user', $form_data['signup']);       
    }
    public function get_current_user_id()
    {
        $user_details = $this->session->userdata('user_details');

        $this->db->select("*");
        $this->db->from('tbl_user');
        $this->db->where('username', $user_details['username']);
        // return $this->db->get()->row()->user_id;
        $row = $this->db->get()->row();
        if (!empty($row)) {
            return $row->user_id;
        }
    }
    

    public function get_states(){
        $this->db->select('*');
        $query  = $this->db->get('states');
        return $query->result_array();   
    }

    public function get_cities($state_id = ''){
        $this->db->select('cities.*');
        $this->db->where('state_id', $state_id);
        $query  = $this->db->get('cities');
        
        return $query->result();
        
    }

    

    public function user_exists($username)
    {
        $this->db->from('tbl_user');
        $this->db->where('username', $username);
        $resultset  = $this->db->get();
        // return ($resultset->num_rows()>=1 ? true : false);

        if ($resultset->num_rows()>=1) {
            return true;
        }
        else{
            return false;
        }   
    }


    public function email_exists($email)
    {
        $this->db->from('tbl_user');
        $this->db->where('email', $email);
        $resultset  = $this->db->get();
        // return ($resultset->num_rows()>=1 ? true : false);

        if ($resultset->num_rows()>=1) {
            return true;
        }
        else{
            return false;
        }   
        
    }

    public function list_roles()
    {
        $this->db->from('tbl_roles');
        return $this->db->get()->result_array();
    }

    public function list_requests_of($user_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_relation');
        $this->db->where('request_to_id', $user_id);
        $this->db->where('status', '0'); //0:pending
        $rs = $this->db->get()->result_array();
        return (!empty($rs)) ? $rs : false ;

    }

    public function list_requests()
    {
        $arr = $this->list_requests_of($this->get_current_user_id());
        if (!empty($arr)) {
            foreach ($arr as $key => $value) {
                $users[] = $value['request_from_id'];
            }

            $this->db->select('*,CONCAT(firstname, " " ,lastname) as FullName');
            $this->db->from('tbl_user');
            $this->db->where_in('user_id', $users);
            $users_who_requested = $this->db->get()->result_array();
            
            return $users_who_requested;
        }
        
    }

    public function get_request_status($user_id, $role_id, $institute_code)
    {
        $this->db->from('tbl_relation');
        $this->db->where('request_from_id', $user_id);
        $this->db->where('as_role_id', $role_id);
        $this->db->where('institute_code', $institute_code);
        // $this->db->where('status', '0'); // 0 : pending
        $row = $this->db->get()->row();

        if (!empty($row)) {
            return $row->status;
        }
        else{
            return false;
        }
    }

    public function accept_request($user_id)
    {
        $this->db->set('status', '1'); // accepted
        $this->db->where('request_from_id', $user_id);
        $this->db->update('tbl_relation');
        $affected_rows =  $this->db->affected_rows();

        
        return $affected_rows;
    }
    
}



/* End of file Mdl_users.php */


?>
