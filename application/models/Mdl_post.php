<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdl_post extends CI_Model {

	public function get_all_posts($class_id)
	{
		$current_user_details = $this->session->userdata('user_details');

		$this->db->select('P.*,U.user_id,U.firstname,U.lastname,UV.has_upvoted');
		$this->db->from('tbl_post P');
		$this->db->join('tbl_upvote UV', "UV.post_id = P.post_id AND UV.user_id = ". $current_user_details['user_id'], 'left');
		// $this->db->join('tbl_upvote UVC', "UVC.post_id = P.post_id", 'left'\);
		$this->db->join('tbl_user U', 'P.user_id = U.user_id', 'inner');
		
		$this->db->where('P.class_id', $class_id);
		$this->db->where('P.is_deleted', '0');
		$this->db->order_by('P.created_at', 'desc');
		// $this->db->group_by('P post_id');
		$new_posts = array();
		$posts =  $this->db->get()->result_array();
		// $new_posts =$posts;
		foreach ($posts as $post) {
			$this->db->select('COUNT(*) as upvotes');
			$this->db->where('post_id', $post['post_id']);
			$post['upvotes'] = $this->db->get('tbl_upvote', 1)->row()->upvotes;
			$new_posts[] = $post;
		}

		return $new_posts;
		// return $this->db->last_query();
	}

	public function get_notices($class_id)
	{
		$this->db->where('class_id', $class_id);
		$this->db->where('is_deleted', '0');
		$this->db->order_by('created_at', 'desc');
		return $this->db->get('tbl_notice')->result_array();
	}

	public function get_poll($class_id)
	{
		$poll_id = $this->db->where('class_id', $class_id)->get('tbl_poll')->row()->poll_id;
		
		$this->db->select('P.*, OPT.*');
		// $this->db->where('P.poll_id', $poll_id);
		$this->db->where('P.is_active', '1');
		$this->db->join('tbl_poll_options OPT', 'P.poll_id = OPT.poll_id', 'inner');

		return $this->db->get('tbl_poll P')->result_array();
	}

	public function has_upvoted($user_id, $post_id)
	{
		$this->db->select('has_upvoted');
		$this->db->where('user_id', $user_id);
		$this->db->where('post_id', $post_id);
		return $this->db->get('tbl_upvote', 1)->row()->has_upvoted;
	}

	public function upvote_post($user_id,$post_id)
	{
		$arr_insert_upvote = array(
			'user_id' => $user_id ,
			'post_id' => $post_id );

		$this->db->insert('tbl_upvote', $arr_insert_upvote);
		// echo "string";
		$this->db->set('has_upvoted','1');
		$this->db->where($arr_insert_upvote);
		$this->db->update('tbl_upvote');
		return ($this->db->affected_rows() == 1) ? true : false ;
	}

	public function undo_upvote_post($user_id, $post_id)
	{
		$arr_insert_upvote = array(
			'user_id' => $user_id ,
			'post_id' => $post_id );

		$this->db->where($arr_insert_upvote);
		$this->db->limit(1);
		$this->db->delete('tbl_upvote');
		
		
		// $this->db->update('tbl_upvote');
		return ($this->db->affected_rows() == 1) ? true : false ;
		
	}

}

/* End of file mdl_post.php */
/* Location: ./application/models/mdl_post.php */