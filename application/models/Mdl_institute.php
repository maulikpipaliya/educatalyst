<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdl_institute extends CI_Model {
	public function __construct(){
		parent::__construct();
	}


	public function index()
	{
		
	}

	public function get_connected_institutes($user_id)
	{
		
		$this->db->from('tbl_user_inst');
		$this->db->where('user_id', $user_id);
		return $this->db->get()->result_array();
	}

	public function is_valid($role_id,$user_id,$institute_code)
	{
		$this->db->select('*');
		$this->db->from('tbl_user_inst');
		$this->db->where('role_id', $role_id);
		$this->db->where('user_id', $user_id);
		$this->db->where('institute_code', $institute_code);
		$row = $this->db->get()->row_array();
		if (!empty($row)) {
			// echo json_encode(TRUE);
			return true;
		}
		else{
			// echo json_encode(FALSE);
			return false;	
		}

	}

	// public function is_teacher($role_id,$user_id,$inst)
	// {
	// 	$this->db->select('*');
	// 	$this->db->from('tbl_user_inst');
	// 	$this->db->where('user_id', $user_id);
	// 	$this->db->where('role_id', $role_id);
	// 	$this->db->where('institute_code', $inst);
	// 	$row = $this->db->get()->row_array();
	// 	if (!empty($row)) {
	// 		// echo json_encode(TRUE);
	// 		return true;
	// 	}
	// 	else{
	// 		// echo json_encode(FALSE);
	// 		return false;	
	// 	}

	// }
	
	// public function is_head($role_id,$user_id,$inst)
	// {
	// 	$this->db->select('*');
	// 	$this->db->from('tbl_user_inst');
	// 	$this->db->where('role_id', $role_id);
	// 	$this->db->where('user_id', $user_id);
	// 	$this->db->where('institute_code', $inst);
	// 	$row = $this->db->get()->row_array();
	// 	if (!empty($row)) {
	// 		// echo json_encode(TRUE);
	// 		return true;
	// 	}
	// 	else{
	// 		// echo json_encode(FALSE);
	// 		return false;	
	// 	}

	// }

	public function get_role_id_in_institute($user_id, $institute_code)
	{
		$this->db->select('role_id');
		$this->db->where('user_id', $user_id);
		$this->db->where('institute_code', $institute_code);
		return $this->db->get('tbl_user_inst')->result_array();
	}


	public function get_role_priority_by_id($role_id)
	{
		$user_details = $this->session->userdata('user_details');
		// echo "<script>console.log( '" . json_encode($ssn_inst_join) . "' );</script>";
		$this->db->select('priority');
		$this->db->from('tbl_roles');
		$this->db->where('role_id', $role_id);
		$row = $this->db->get()->row();
		if (!empty($row)) {
			return $row->priority;
		}
		
	}

	public function get_role_name_by_id($role_id)
	{
		$user_details = $this->session->userdata('user_details');
		// echo "<script>console.log( '" . json_encode($ssn_inst_join) . "' );</script>";
		$this->db->select('role_name');
		$this->db->from('tbl_roles');
		$this->db->where('role_id', $role_id);
		$row = $this->db->get()->row();
		return (!empty($row)) ? $row->role_name : false ;
		
	}
	public function get_role_id_by_name($role_name)
	{
		$user_details = $this->session->userdata('user_details');
		// echo "<script>console.log( '" . json_encode($ssn_inst_join) . "' );</script>";
		$this->db->select('role_id');
		$this->db->from('tbl_roles');
		$this->db->where('role_name', $role_name);
		return $this->db->get()->row()->role_id;
		
	}


	public function get_role_id_by_priority($role_priority)
	{
		$user_details = $this->session->userdata('user_details');
		// echo "<script>console.log( '" . json_encode($ssn_inst_join) . "' );</script>";
		$this->db->select('role_id');
		$this->db->from('tbl_roles');
		$this->db->where('priority', $role_priority);

		$role_id =  $this->db->get()->row();
		if (!empty($role_id)) {
			return $role_id->role_id;
		}
		return false;

		
	}



	public function send_request_old($institute_code,$joined_as)
	{

		$user_details = $this->session->userdata('user_details');
		$user_id = $request_from_id = $this->Mdl_users->get_current_user_id();
		
		// $ssn_inst_join = $this->session->userdata('ssn_inst_join');
		
		// $is_head = $this->Mdl_institute->is_valid($ssn_inst_join['joined_as'],$this->Mdl_users->get_current_user_id(),$ssn_inst_join['institute_code']);

		// echo "<script> console.log('is_head". json_encode($is_head) . "');</script>";
		if (!empty($institute_code) && !empty($joined_as)) {
			$priority = $this->get_role_priority_by_id($joined_as);
			// echo "<script> console.log('". json_encode($priority) . "');</script>";	
			$request_to_role_id = $this->get_role_id_by_priority($priority - 1);
			// echo "<script> console.log('". json_encode($request_to_role_id) . "');</script>";
		}

		if ($joined_as == $this->get_role_id_by_name("Head")) { //Head
			echo "<script> console.log('". json_encode("Head") . "');</script>";	
			$request_to_id = "";
		}
		elseif ($joined_as == $this->get_role_id_by_name("Teacher")) { // Teacher
			
			echo "<script> console.log('". json_encode("Teacher") . "');</script>";	
			
			$this->db->select('user_id');
			$this->db->from('tbl_user_inst');
			$this->db->where('role_id', $request_to_role_id);
			$this->db->where('institute_code', $institute_code);
			
			$row = $this->db->get()->row();
			if (!empty($row)) {
				$request_to_id = $row->user_id;
			}
			
			echo "<script> console.log('request_to_id:". json_encode($request_to_id) . "');</script>";
		}
		elseif ($joined_as == $this->get_role_id_by_name("Assistant")) { // assistant
			echo "<script> console.log('". json_encode("Assistant") . "');</script>";		
			$p1 = $this->get_role_id_by_priority($priority - 2);
			// $p2 = $this->get_role_id_by_priority($priority - 2);
			$this->db->select('user_id');
			$this->db->from('tbl_user_inst');
			$this->db->where('role_id', $p1);
			// $this->db->or_where('role_id', $p2);
			$this->db->where('institute_code', $institute_code);
			
			$row = $this->db->get()->row();
			// echo $this->db->last_query();;
			if (!empty($row)) {
				$request_to_id = $row->user_id;
			}
			
		}
		elseif ($joined_as == $this->get_role_id_by_name("Student")) {   //student
			echo "<script> console.log('". json_encode("Student") . "');</script>";					
			$p1 = $this->get_role_id_by_priority($priority - 1);
			$p2 = $this->get_role_id_by_priority($priority - 2);
			
			$this->db->select('user_id');
			$this->db->from('tbl_user_inst');
			$this->db->group_start();
			$this->db->where('role_id', $p1);
			$this->db->or_where('role_id', $p2);
			$this->db->group_end();
			$this->db->where('institute_code', $institute_code);
			
			$row = $this->db->get()->row();
			echo $this->db->last_query();
			// echo "<script> console.log('request_to_id:". json_encode($q) . "');</script>";	
			if (!empty($row)) {
				$request_to_id = $row->user_id;
			}
			
			echo "<script> console.log('request_to_id:". json_encode($request_to_id) . "');</script>";	
		}
		
		$arrayRequest = array(
			'request_from_id' => $user_id,
			'request_to_id' => $request_to_id,
			'institute_code' => $institute_code,
			'as_role_id'  => $joined_as,
			'status'  	  => '0',
			'init_user_id'=> $user_id);
		
		echo "<script> console.log('". json_encode($arrayRequest) . "');</script>";

		if (!empty($request_to_id)) { // Head Case Excluded
			$inserted = $this->db->insert('tbl_relation', $arrayRequest);		
			if ($inserted) {
				$this->session->set_userdata("ssn_request", $arrayRequest );
				return true;
			}
		}

		
		
		

		// echo "<script>console.log( '" .$this->get_role_priority(),json_encode($user_details) . "' );</script>";

	}
	public function get_institute_type_list()
	{
		$this->db->select('*');	
		$this->db->from('tbl_institute_types');
		return $this->db->get()->result_array();
	}

	public function get_institute_details($code)
	{
		$this->db->from('tbl_institute');
		$this->db->where('institute_code', $code);
		return $this->db->get()->row_array();
	}

	public function get_inst_code_trailer()
	{
		$this->db->select('SUBSTRING_INDEX(institute_code, "_",-1) as Trail', FALSE);	
		$this->db->order_by('institute_id', 'desc');
		$this->db->limit(1);
		$this->db->from('tbl_institute');
		return $this->db->get()->result_array();
	}

	public function db_insert_institute($form_data)
	{
		return $this->db->insert('tbl_institute', $form_data);
	}

	public function db_insert_institute_user($form_data)
	{
		return $this->db->insert('tbl_user_inst', $form_data);
	}

	public function get_code_details($code)
	{
		$this->db->from('tbl_code');
		$this->db->where('code', $code);
		$row = $this->db->get()->row_array();
		return (!empty($row)) ? $row : false ;

	}

	public function join_code_exists($code)
	{
		// $this->db->from('tbl_code');
		// $this->db->where('code', $code);
		// return $this->db->get()->result_array();
		$query = "SELECT * FROM (SELECT class_code c FROM `tbl_class` UNION SELECT code FROM tbl_code) as Codemix WHERE BINARY Codemix.c='".$code."'";
		
		return $this->db->query($query)->result_array();

	}


	public function institute_code_exists($code)
	{
		$this->db->from('tbl_institute');
		$this->db->where('institute_code', $code);
		return $this->db->get()->result_array();
	}

	public function teachers_list($institute_code)
	{
		$this->db->select("U.firstname, U.lastname, U.email, U.username, U.mobile, U.gender, U.birthdate, UI.*");
		$this->db->from("tbl_user_inst UI");
		$this->db->join("tbl_user U", "U.user_id = UI.user_id", "left");
		$this->db->where("role_id", 2);
		$this->db->where("institute_code", $institute_code);
		return $this->db->get()->result();
	}
}

/* End of file Mdl_institute.php */
/* Location: ./application/models/Mdl_institute.php */