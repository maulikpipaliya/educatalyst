<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdl_essential extends CI_Model {
	public function __construct()
	{
		parent::__construct();
		
	}

	public function console_log_table( $data, $comment = 'Output :' ) {
    	ob_start();
		// $output  = 'console.info( \'' . $comment . ':\' );';

		$output  = 'console.group("'. $comment .'");';
		$output .= 'console.table(' . json_encode( $data ) . ');';
		$output .= 'console.groupEnd();';
		$output  = sprintf( '<script>%s</script>', $output );
		echo $output;
	}

	public function console_log( $data, $comment = 'Error :' ) {
    	ob_start();
		// $output  = 'console.info( \'' . $comment . ':\' );';

		$output  = 'console.group("'. $comment .'");';
		$output .= 'console.log(' . json_encode( $data ) . ');';
		$output .= 'console.groupEnd();';
		$output  = sprintf( '<script>%s</script>', $output );
		echo $output;
	}
	

}

/* End of file Mdl_essential.php */
/* Location: ./application/models/Mdl_essential.php */