<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdl_common extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		
	}
	public function index()
	{
		
	}

	public function is_class_code($code)
	{
		$this->db->where('class_code', $code);
	}

	public function get_code_details($code)
	{
		$this->db->from('tbl_code');
		$this->db->where('code', $code);
		$row = $this->db->get()->row_array();
		return (!empty($row)) ? $row : false ;

	}

	public function make_date_standard($date)
	{
		$this->db->query("");
		
	}
	public function get_state_code($state_id)
    {
        $this->db->select('code');
        $this->db->where('id', $state_id);
        $query  = $this->db->get('states');
        return $query->row_array();   
    }

    public function get_city_code($city_id)
    {
    	$this->db->select('id');
        $this->db->where('id', $city_name);
        $query  = $this->db->get('states');
        return $query->row_array();   
    }
}

/* End of file Mdl_common.php */
/* Location: ./application/models/Mdl_common.php */

 ?>