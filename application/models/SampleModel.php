<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class SampleModel extends CI_Model {
        function __construct(){
            parent::__construct();
        }

        function get_data(){
            $query  = $this->db->query("SELECT * FROM `tableSample1`");
            return $query->result_array();
        }
    }
    
    /* End of file SampleModel.php */
    
?>