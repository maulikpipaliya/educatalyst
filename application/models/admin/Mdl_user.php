<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdl_user extends CI_Model {

	public function get_all()
	{
		$this->db->select("*");
		$this->db->from("tbl_user");		
		$this->db->where("is_admin", '0');
		return $this->db->get()->result();
	}

	public function get($user_id)
	{
		$this->db->select("*");
		$this->db->from("tbl_user");		
		$this->db->where("is_admin", '0');
		$this->db->where("user_id", $user_id);
		return $this->db->get()->row();
	}

	public function get_user_joinings($user_id, $group_by_role = false)
	{
		$this->db->select("I.*, R.*, IT.*");
		$this->db->from("tbl_user_inst UI");
		$this->db->join("tbl_institute I", "I.institute_code = UI.institute_code", "left");
		$this->db->join("tbl_institute_types IT", "IT.institute_type_id = I.institute_type_id", "left");
		$this->db->join("tbl_roles R", "R.role_id = UI.role_id", "left");
		$this->db->where("UI.user_id", $user_id);
		$this->db->order_by("R.priority");
		$user_joinings = $this->db->get()->result();
		
		if(!$group_by_role)
		{
			return $user_joinings;
		}
		$new_user_joinings = array();
		foreach($user_joinings as $institute)
		{
			$new_user_joinings[$institute->role_name][] = $institute;
		}
		return $new_user_joinings;
	}

	public function block($user_id, $action)
	{
		$this->db->where("user_id", $user_id);
		$this->db->update("tbl_user", array("is_blocked" => $action));
	}
}
