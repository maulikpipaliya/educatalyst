<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdl_state_city extends CI_Model {

	public function get_all_states()
	{
		$this->db->select("S.*, C.name as country_name");
		$this->db->from("states S");
		$this->db->join("countries C", "C.id = S.country_id", "left");
		$this->db->where("S.is_del", "0");
		return $this->db->get()->result();
	}
	
	public function get_all_cities()
	{
		$this->db->select("C.*, S.name as state_name");
		$this->db->from("cities C");
		$this->db->join("states S", "S.id = C.state_id", "left");
		$this->db->where("C.is_del", "0");
		return $this->db->get()->result();
	}
}
