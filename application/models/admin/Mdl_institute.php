<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdl_institute extends CI_Model {

	public function get_all()
	{
		$this->db->select("I.*, IT.institute_type, U.firstname, U.lastname, U.user_id");
		$this->db->from("tbl_institute I");
		$this->db->join("tbl_institute_types IT", "IT.institute_type_id = I.institute_type_id", "left");
		$this->db->join("tbl_user U", "U.user_id = I.creator_user_id", "left");
		$this->db->where("I.is_deleted", NULL);
		return $this->db->get()->result();
	}

	public function get($institute_id)
	{
		$this->db->select("I.*, IT.institute_type, U.firstname, U.lastname, U.user_id");
		$this->db->from("tbl_institute I");
		$this->db->join("tbl_institute_types IT", "IT.institute_type_id = I.institute_type_id", "left");
		$this->db->join("tbl_user U", "U.user_id = I.creator_user_id", "left");
		$this->db->where("I.is_deleted", NULL);
		$this->db->where("I.institute_id", $institute_id);
		return $this->db->get()->row();
	}

	public function get_institute_types()
	{
		$this->db->select("*");
		$this->db->from("tbl_institute_types");		
		$this->db->where("is_del", "0");
		return $this->db->get()->result();
	}	

	public function get_institute_users($institute_code, $group_by_role = false)
	{
		$this->db->select("U.*, R.*");
		$this->db->from("tbl_user_inst UI");
		$this->db->join("tbl_user U", "U.user_id = UI.user_id", "left");
		$this->db->join("tbl_roles R", "R.role_id = UI.role_id", "left");
		$this->db->where("UI.institute_code", $institute_code);
		$this->db->order_by("R.priority");
		$institute_users = $this->db->get()->result();
		
		if(!$group_by_role)
		{
			return $institute_users;
		}
		$new_institute_users = array();
		foreach($institute_users as $institute)
		{
			$new_institute_users[$institute->role_name][] = $institute;
		}
		return $new_institute_users;
	}

	public function get_institute_classes($institute_code, $html_tree = false)
	{
		$this->db->select("C.*, U.*");
		$this->db->from("tbl_class C");
		$this->db->join("tbl_user U", "U.user_id = C.creator_id", "left");
		$this->db->where("C.institute_code", $institute_code);
		$classes = $this->db->get()->result();		
		if(!$html_tree)
		{
			return $classes;
		}
		require_once APPPATH.'libraries/HtmlTreeBuilder.php';
		$objHtmlTree = new HtmlTreeBuilder($classes);
		return $objHtmlTree->htmlList();
	}

	public function block($institute_id, $action)
	{
		$this->db->where("institute_id", $institute_id);
		$this->db->update("tbl_institute", array("blocked" => $action));
	}
}
