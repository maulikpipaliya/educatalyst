<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdl_admin extends CI_Model {

	public function validate_login($form_data)
	{
		$this->db->from('tbl_user');
		$this->db->group_start();
		$this->db->where('username', $form_data['username']);
		$this->db->or_where('email', $form_data['username']);
		$this->db->group_end();
		$this->db->where('password', md5($form_data['password']));
		$this->db->where('is_admin', '1');
		$row = $this->db->get()->row_array();	
		return (!empty($row)) ? $row : null ;
	}

}

/* End of file Mdl_admin.php */
/* Location: ./application/models/admin/Mdl_admin.php */