<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdl_request extends CI_Model {
	public function __construct()
	{
		parent::__construct();
		
	}

	public function index()
	{
		
	}

	public function request_already_exists($value='')
	{
		
	}

	public function list_requests_of($user_id)
	{
		$this->db->where('request_to_id', $user_id);
		$this->db->where('status', "2"); //2:Pending
		$rs = $this->db->get('tbl_request')->result_array();
		$arr_request_from_id = array();
		foreach ($rs as $key => $value) {
			$arr_request_from_id[] = $value['request_from_id'];
			// $arr_request_as[] = $value['as_role_id'];
		}

		// print_r($arr_request_from_id);
		
		if (!empty($arr_request_from_id)) {
			$this->db->select('U.*,R.*,CONCAT(firstname, " " ,lastname) as FullName');
			$this->db->from('tbl_user U');
			$this->db->join('tbl_request R', 'R.request_from_id = U.user_id', 'inner');
			$this->db->where('request_to_id', $user_id);
			$this->db->where('status', "2");

			/*
				SELECT `U`.*, `R`.*, CONCAT(firstname, " ", lastname) as FullName
				FROM `tbl_user` `U`
				INNER JOIN `tbl_request` `R` ON `R`.`request_from_id` = `U`.`user_id`
				WHERE `request_to_id` = '63'
				AND `status` = '2'
			*/
				$users_who_requested = $this->db->get()->result_array();
			}
			
			// print_r($users_who_requested);
		// // $a = array_merge_recursive($users_who_requested, $arr_request_as);
		// foreach ($users_who_requested as $key => $value) {
		// 	foreach ($arr_request_as as $k => $v) {
		// 		$users_who_requested[$key][] = $arr_request_as[$k];
		// 		$users_who_requested[$key]['as_role_id'] = "hah";
		// 	}
		// }

			return (!empty($users_who_requested)) ? $users_who_requested : false ;	

		}

		public function send_request($arr_request)
		{
			$this->db->where($arr_request);
			$row = $this->db->get('tbl_request')->row_array();
			$exists = (!empty($row)) ? $row : false ;

			if (empty($exists)) {
				$this->db->insert('tbl_request', $arr_request);		
			}
		}

		public function accept_request($request_id)
		{
		$this->db->set('status', "1"); // 1 : Accepted
		$this->db->where('request_id', $request_id);		
		$this->db->update('tbl_request');

		$affected_rows =  $this->db->affected_rows();

		$this->db->from('tbl_request');
		$this->db->where('request_id', $request_id);
		$row_array = $this->db->get()->row_array();
		
		// print_r($result_array);
		if ($affected_rows > 0) {
			$object = array(
				'user_id' => $row_array['request_from_id'],
				'role_id' => $row_array['as_role_id'],
				'institute_code' => $row_array['institute_code']);

			$this->db->where($object);
			$row = $this->db->get('tbl_user_inst')->row();
			$already_exists = (!empty($row)) ? true : false ;

			if (!($already_exists)) {
				$this->db->insert('tbl_user_inst', $object);     
			}

		}

		return $affected_rows;
	}
	

}

/* End of file Mdl_request.php */
/* Location: ./application/models/Mdl_request.php */	