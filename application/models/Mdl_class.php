<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdl_class extends CI_Model {
	public function list_classes($user_id,$parent_id)
	{
		
		$this->db->where('creator_id', $user_id);
		$this->db->where('parent_id', $parent_id);
		$result_array = $this->db->get('tbl_class')->result_array();
		return (!empty($result_array)) ? $result_array : null ;
	}
	
	public function list_students($class_code)
	{
		$this->db->where('class_code', $class_code);
		$result_array = $this->db->get('tbl_user_inst')->result_array();
		return (!empty($result_array)) ? $result_array : null ;
	}

	public function get_current_class_id()
	{
		$ssn_class_details = $this->session->userdata('ssn_class_details');
		if (!empty($ssn_class_details)) {
			return $ssn_class_details['class_id'];
		}
		else{
			return null;
		}
	}

	public function list_classes_joined($user_id)
	{
		$ssn_inst_join = $this->session->userdata('ssn_inst_join');

		$this->db->where('UI.user_id', $user_id);
		$this->db->where('UI.is_blocked', '0');
		$this->db->where('UI.class_code!=', "");
		$this->db->where('UI.institute_code', $ssn_inst_join['institute_code']);

		$this->db->join('tbl_class C', 'C.class_code = UI.class_code', 'left');
		$result_array = $this->db->get('tbl_user_inst UI')->result_array();
		return (!empty($result_array)) ? $result_array : null ;
	}

}

/* End of file Mdl_class.php */
/* Location: ./application/models/Mdl_class.php */